package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.MesPackageOrderDetail;

/**
 * mes-包装单明细Mapper接口
 *
 * @author swingli
 * @date 2023-12-25
 */
public interface MesPackageOrderDetailMapper
{
    /**
     * 查询mes-包装单明细
     *
     * @param id mes-包装单明细主键
     * @return mes-包装单明细
     */
    public MesPackageOrderDetail selectMesPackageOrderDetailById(Long id);

    /**
     * 查询mes-包装单明细列表
     *
     * @param mesPackageOrderDetail mes-包装单明细
     * @return mes-包装单明细集合
     */
    public List<MesPackageOrderDetail> selectMesPackageOrderDetailList(MesPackageOrderDetail mesPackageOrderDetail);

    /**
     * 新增mes-包装单明细
     *
     * @param mesPackageOrderDetail mes-包装单明细
     * @return 结果
     */
    public int insertMesPackageOrderDetail(MesPackageOrderDetail mesPackageOrderDetail);

    /**
     * 修改mes-包装单明细
     *
     * @param mesPackageOrderDetail mes-包装单明细
     * @return 结果
     */
    public int updateMesPackageOrderDetail(MesPackageOrderDetail mesPackageOrderDetail);

    /**
     * 删除mes-包装单明细
     *
     * @param id mes-包装单明细主键
     * @return 结果
     */
    public int deleteMesPackageOrderDetailById(Long id);

    /**
     * 批量删除mes-包装单明细
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMesPackageOrderDetailByIds(Long[] ids);
}