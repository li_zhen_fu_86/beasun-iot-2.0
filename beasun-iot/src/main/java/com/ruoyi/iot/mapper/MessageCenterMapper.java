package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.MessageCenter;

/**
 * iot-消息中心Mapper接口
 *
 * @author swingli
 * @date 2023-12-23
 */
public interface MessageCenterMapper
{
    /**
     * 查询iot-消息中心
     *
     * @param id iot-消息中心主键
     * @return iot-消息中心
     */
    public MessageCenter selectMessageCenterById(Long id);

    /**
     * 查询iot-消息中心列表
     *
     * @param messageCenter iot-消息中心
     * @return iot-消息中心集合
     */
    public List<MessageCenter> selectMessageCenterList(MessageCenter messageCenter);

    /**
     * 新增iot-消息中心
     *
     * @param messageCenter iot-消息中心
     * @return 结果
     */
    public int insertMessageCenter(MessageCenter messageCenter);

    /**
     * 修改iot-消息中心
     *
     * @param messageCenter iot-消息中心
     * @return 结果
     */
    public int updateMessageCenter(MessageCenter messageCenter);

    /**
     * 删除iot-消息中心
     *
     * @param id iot-消息中心主键
     * @return 结果
     */
    public int deleteMessageCenterById(Long id);

    /**
     * 批量删除iot-消息中心
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMessageCenterByIds(Long[] ids);
}