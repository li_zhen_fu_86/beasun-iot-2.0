package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.OperProtocol;

/**
 * 运维管理-协议管理Mapper接口
 * 
 * @author swingli
 * @date 2023-12-22
 */
public interface OperProtocolMapper 
{
    /**
     * 查询运维管理-协议管理
     * 
     * @param id 运维管理-协议管理主键
     * @return 运维管理-协议管理
     */
    public OperProtocol selectOperProtocolById(Long id);

    /**
     * 查询运维管理-协议管理列表
     * 
     * @param operProtocol 运维管理-协议管理
     * @return 运维管理-协议管理集合
     */
    public List<OperProtocol> selectOperProtocolList(OperProtocol operProtocol);

    /**
     * 新增运维管理-协议管理
     * 
     * @param operProtocol 运维管理-协议管理
     * @return 结果
     */
    public int insertOperProtocol(OperProtocol operProtocol);

    /**
     * 修改运维管理-协议管理
     * 
     * @param operProtocol 运维管理-协议管理
     * @return 结果
     */
    public int updateOperProtocol(OperProtocol operProtocol);

    /**
     * 删除运维管理-协议管理
     * 
     * @param id 运维管理-协议管理主键
     * @return 结果
     */
    public int deleteOperProtocolById(Long id);

    /**
     * 批量删除运维管理-协议管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOperProtocolByIds(Long[] ids);
}