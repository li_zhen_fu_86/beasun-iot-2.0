package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.MesPackageOrder;

/**
 * mes-包装单Mapper接口
 *
 * @author swingli
 * @date 2023-12-25
 */
public interface MesPackageOrderMapper
{
    /**
     * 查询mes-包装单
     *
     * @param id mes-包装单主键
     * @return mes-包装单
     */
    public MesPackageOrder selectMesPackageOrderById(Long id);

    /**
     * 查询mes-包装单列表
     *
     * @param mesPackageOrder mes-包装单
     * @return mes-包装单集合
     */
    public List<MesPackageOrder> selectMesPackageOrderList(MesPackageOrder mesPackageOrder);

    /**
     * 新增mes-包装单
     *
     * @param mesPackageOrder mes-包装单
     * @return 结果
     */
    public int insertMesPackageOrder(MesPackageOrder mesPackageOrder);

    /**
     * 修改mes-包装单
     *
     * @param mesPackageOrder mes-包装单
     * @return 结果
     */
    public int updateMesPackageOrder(MesPackageOrder mesPackageOrder);

    /**
     * 删除mes-包装单
     *
     * @param id mes-包装单主键
     * @return 结果
     */
    public int deleteMesPackageOrderById(Long id);

    /**
     * 批量删除mes-包装单
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMesPackageOrderByIds(Long[] ids);
}