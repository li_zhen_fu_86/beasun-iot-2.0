package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.MesInWarehouseRecord;

/**
 * mesh-入库记录单Mapper接口
 *
 * @author swingli
 * @date 2023-12-25
 */
public interface MesInWarehouseRecordMapper
{
    /**
     * 查询mesh-入库记录单
     *
     * @param id mesh-入库记录单主键
     * @return mesh-入库记录单
     */
    public MesInWarehouseRecord selectMesInWarehouseRecordById(Long id);

    /**
     * 查询mesh-入库记录单列表
     *
     * @param mesInWarehouseRecord mesh-入库记录单
     * @return mesh-入库记录单集合
     */
    public List<MesInWarehouseRecord> selectMesInWarehouseRecordList(MesInWarehouseRecord mesInWarehouseRecord);

    /**
     * 新增mesh-入库记录单
     *
     * @param mesInWarehouseRecord mesh-入库记录单
     * @return 结果
     */
    public int insertMesInWarehouseRecord(MesInWarehouseRecord mesInWarehouseRecord);

    /**
     * 修改mesh-入库记录单
     *
     * @param mesInWarehouseRecord mesh-入库记录单
     * @return 结果
     */
    public int updateMesInWarehouseRecord(MesInWarehouseRecord mesInWarehouseRecord);

    /**
     * 删除mesh-入库记录单
     *
     * @param id mesh-入库记录单主键
     * @return 结果
     */
    public int deleteMesInWarehouseRecordById(Long id);

    /**
     * 批量删除mesh-入库记录单
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMesInWarehouseRecordByIds(Long[] ids);
}