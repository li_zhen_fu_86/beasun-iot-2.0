package com.ruoyi.iot.mapper;

import com.ruoyi.iot.domain.OperModel;

import java.util.List;

/**
 * 运维管理-设备模拟Mapper接口
 *
 * @author swingli
 * @date 2023-12-22
 */
public interface OperModelMapper
{
    /**
     * 查询运维管理-设备模拟
     *
     * @param id 运维管理-设备模拟主键
     * @return 运维管理-设备模拟
     */
    public OperModel selectOperModelById(Long id);

    /**
     * 查询运维管理-设备模拟列表
     *
     * @param operModel 运维管理-设备模拟
     * @return 运维管理-设备模拟集合
     */
    public List<OperModel> selectOperModelList(OperModel operModel);

    /**
     * 新增运维管理-设备模拟
     *
     * @param operModel 运维管理-设备模拟
     * @return 结果
     */
    public int insertOperModel(OperModel operModel);

    /**
     * 修改运维管理-设备模拟
     *
     * @param operModel 运维管理-设备模拟
     * @return 结果
     */
    public int updateOperModel(OperModel operModel);

    /**
     * 删除运维管理-设备模拟
     *
     * @param id 运维管理-设备模拟主键
     * @return 结果
     */
    public int deleteOperModelById(Long id);

    /**
     * 批量删除运维管理-设备模拟
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOperModelByIds(Long[] ids);
}