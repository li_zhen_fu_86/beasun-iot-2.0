package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.MesCompleteOrder;

/**
 * mes-完工汇报单Mapper接口
 *
 * @author swingli
 * @date 2023-12-23
 */
public interface MesCompleteOrderMapper
{
    /**
     * 查询mes-完工汇报单
     *
     * @param id mes-完工汇报单主键
     * @return mes-完工汇报单
     */
    public MesCompleteOrder selectMesCompleteOrderById(Long id);

    /**
     * 查询mes-完工汇报单列表
     *
     * @param mesCompleteOrder mes-完工汇报单
     * @return mes-完工汇报单集合
     */
    public List<MesCompleteOrder> selectMesCompleteOrderList(MesCompleteOrder mesCompleteOrder);

    /**
     * 新增mes-完工汇报单
     *
     * @param mesCompleteOrder mes-完工汇报单
     * @return 结果
     */
    public int insertMesCompleteOrder(MesCompleteOrder mesCompleteOrder);

    /**
     * 修改mes-完工汇报单
     *
     * @param mesCompleteOrder mes-完工汇报单
     * @return 结果
     */
    public int updateMesCompleteOrder(MesCompleteOrder mesCompleteOrder);

    /**
     * 删除mes-完工汇报单
     *
     * @param id mes-完工汇报单主键
     * @return 结果
     */
    public int deleteMesCompleteOrderById(Long id);

    /**
     * 批量删除mes-完工汇报单
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMesCompleteOrderByIds(Long[] ids);
}