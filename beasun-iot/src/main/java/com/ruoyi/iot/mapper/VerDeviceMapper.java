package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.VerDevice;

/**
 * 版本管理-硬件版本定义Mapper接口
 *
 * @author swingli
 * @date 2023-12-23
 */
public interface VerDeviceMapper
{
    /**
     * 查询版本管理-硬件版本定义
     *
     * @param id 版本管理-硬件版本定义主键
     * @return 版本管理-硬件版本定义
     */
    public VerDevice selectVerDeviceById(Long id);

    /**
     * 查询版本管理-硬件版本定义列表
     *
     * @param verDevice 版本管理-硬件版本定义
     * @return 版本管理-硬件版本定义集合
     */
    public List<VerDevice> selectVerDeviceList(VerDevice verDevice);

    /**
     * 新增版本管理-硬件版本定义
     *
     * @param verDevice 版本管理-硬件版本定义
     * @return 结果
     */
    public int insertVerDevice(VerDevice verDevice);

    /**
     * 修改版本管理-硬件版本定义
     *
     * @param verDevice 版本管理-硬件版本定义
     * @return 结果
     */
    public int updateVerDevice(VerDevice verDevice);

    /**
     * 删除版本管理-硬件版本定义
     *
     * @param id 版本管理-硬件版本定义主键
     * @return 结果
     */
    public int deleteVerDeviceById(Long id);

    /**
     * 批量删除版本管理-硬件版本定义
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteVerDeviceByIds(Long[] ids);
}