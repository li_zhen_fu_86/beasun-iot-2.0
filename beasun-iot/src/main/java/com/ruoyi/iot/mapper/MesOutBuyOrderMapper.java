package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.MesOutBuyOrder;

/**
 * mes-出库订单Mapper接口
 *
 * @author swingli
 * @date 2023-12-25
 */
public interface MesOutBuyOrderMapper
{
    /**
     * 查询mes-出库订单
     *
     * @param id mes-出库订单主键
     * @return mes-出库订单
     */
    public MesOutBuyOrder selectMesOutBuyOrderById(Long id);

    /**
     * 查询mes-出库订单列表
     *
     * @param mesOutBuyOrder mes-出库订单
     * @return mes-出库订单集合
     */
    public List<MesOutBuyOrder> selectMesOutBuyOrderList(MesOutBuyOrder mesOutBuyOrder);

    /**
     * 新增mes-出库订单
     *
     * @param mesOutBuyOrder mes-出库订单
     * @return 结果
     */
    public int insertMesOutBuyOrder(MesOutBuyOrder mesOutBuyOrder);

    /**
     * 修改mes-出库订单
     *
     * @param mesOutBuyOrder mes-出库订单
     * @return 结果
     */
    public int updateMesOutBuyOrder(MesOutBuyOrder mesOutBuyOrder);

    /**
     * 删除mes-出库订单
     *
     * @param id mes-出库订单主键
     * @return 结果
     */
    public int deleteMesOutBuyOrderById(Long id);

    /**
     * 批量删除mes-出库订单
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMesOutBuyOrderByIds(Long[] ids);
}