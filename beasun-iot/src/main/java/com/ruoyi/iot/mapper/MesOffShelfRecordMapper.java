package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.MesOffShelfRecord;

/**
 * mes-下架记录单Mapper接口
 *
 * @author swingli
 * @date 2023-12-25
 */
public interface MesOffShelfRecordMapper
{
    /**
     * 查询mes-下架记录单
     *
     * @param id mes-下架记录单主键
     * @return mes-下架记录单
     */
    public MesOffShelfRecord selectMesOffShelfRecordById(Long id);

    /**
     * 查询mes-下架记录单列表
     *
     * @param mesOffShelfRecord mes-下架记录单
     * @return mes-下架记录单集合
     */
    public List<MesOffShelfRecord> selectMesOffShelfRecordList(MesOffShelfRecord mesOffShelfRecord);

    /**
     * 新增mes-下架记录单
     *
     * @param mesOffShelfRecord mes-下架记录单
     * @return 结果
     */
    public int insertMesOffShelfRecord(MesOffShelfRecord mesOffShelfRecord);

    /**
     * 修改mes-下架记录单
     *
     * @param mesOffShelfRecord mes-下架记录单
     * @return 结果
     */
    public int updateMesOffShelfRecord(MesOffShelfRecord mesOffShelfRecord);

    /**
     * 删除mes-下架记录单
     *
     * @param id mes-下架记录单主键
     * @return 结果
     */
    public int deleteMesOffShelfRecordById(Long id);

    /**
     * 批量删除mes-下架记录单
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMesOffShelfRecordByIds(Long[] ids);
}