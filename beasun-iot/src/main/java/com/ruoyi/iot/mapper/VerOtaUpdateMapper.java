package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.VerOtaUpdate;

/**
 * 版本管理-OTA升级广利Mapper接口
 *
 * @author swingli
 * @date 2023-12-23
 */
public interface VerOtaUpdateMapper
{
    /**
     * 查询版本管理-OTA升级广利
     *
     * @param id 版本管理-OTA升级广利主键
     * @return 版本管理-OTA升级广利
     */
    public VerOtaUpdate selectVerOtaUpdateById(Long id);

    /**
     * 查询版本管理-OTA升级广利列表
     *
     * @param verOtaUpdate 版本管理-OTA升级广利
     * @return 版本管理-OTA升级广利集合
     */
    public List<VerOtaUpdate> selectVerOtaUpdateList(VerOtaUpdate verOtaUpdate);

    /**
     * 新增版本管理-OTA升级广利
     *
     * @param verOtaUpdate 版本管理-OTA升级广利
     * @return 结果
     */
    public int insertVerOtaUpdate(VerOtaUpdate verOtaUpdate);

    /**
     * 修改版本管理-OTA升级广利
     *
     * @param verOtaUpdate 版本管理-OTA升级广利
     * @return 结果
     */
    public int updateVerOtaUpdate(VerOtaUpdate verOtaUpdate);

    /**
     * 删除版本管理-OTA升级广利
     *
     * @param id 版本管理-OTA升级广利主键
     * @return 结果
     */
    public int deleteVerOtaUpdateById(Long id);

    /**
     * 批量删除版本管理-OTA升级广利
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteVerOtaUpdateByIds(Long[] ids);
}