package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.MesSerialWarehouse;

/**
 * mes-序列号库存Mapper接口
 *
 * @author swingli
 * @date 2023-12-25
 */
public interface MesSerialWarehouseMapper
{
    /**
     * 查询mes-序列号库存
     *
     * @param id mes-序列号库存主键
     * @return mes-序列号库存
     */
    public MesSerialWarehouse selectMesSerialWarehouseById(Long id);

    /**
     * 查询mes-序列号库存列表
     *
     * @param mesSerialWarehouse mes-序列号库存
     * @return mes-序列号库存集合
     */
    public List<MesSerialWarehouse> selectMesSerialWarehouseList(MesSerialWarehouse mesSerialWarehouse);

    /**
     * 新增mes-序列号库存
     *
     * @param mesSerialWarehouse mes-序列号库存
     * @return 结果
     */
    public int insertMesSerialWarehouse(MesSerialWarehouse mesSerialWarehouse);

    /**
     * 修改mes-序列号库存
     *
     * @param mesSerialWarehouse mes-序列号库存
     * @return 结果
     */
    public int updateMesSerialWarehouse(MesSerialWarehouse mesSerialWarehouse);

    /**
     * 删除mes-序列号库存
     *
     * @param id mes-序列号库存主键
     * @return 结果
     */
    public int deleteMesSerialWarehouseById(Long id);

    /**
     * 批量删除mes-序列号库存
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMesSerialWarehouseByIds(Long[] ids);
}