package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.MesWarehouseChange;

/**
 * mes-库存流水-制单日期Mapper接口
 *
 * @author swingli
 * @date 2023-12-25
 */
public interface MesWarehouseChangeMapper
{
    /**
     * 查询mes-库存流水-制单日期
     *
     * @param id mes-库存流水-制单日期主键
     * @return mes-库存流水-制单日期
     */
    public MesWarehouseChange selectMesWarehouseChangeById(Long id);

    /**
     * 查询mes-库存流水-制单日期列表
     *
     * @param mesWarehouseChange mes-库存流水-制单日期
     * @return mes-库存流水-制单日期集合
     */
    public List<MesWarehouseChange> selectMesWarehouseChangeList(MesWarehouseChange mesWarehouseChange);

    /**
     * 新增mes-库存流水-制单日期
     *
     * @param mesWarehouseChange mes-库存流水-制单日期
     * @return 结果
     */
    public int insertMesWarehouseChange(MesWarehouseChange mesWarehouseChange);

    /**
     * 修改mes-库存流水-制单日期
     *
     * @param mesWarehouseChange mes-库存流水-制单日期
     * @return 结果
     */
    public int updateMesWarehouseChange(MesWarehouseChange mesWarehouseChange);

    /**
     * 删除mes-库存流水-制单日期
     *
     * @param id mes-库存流水-制单日期主键
     * @return 结果
     */
    public int deleteMesWarehouseChangeById(Long id);

    /**
     * 批量删除mes-库存流水-制单日期
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMesWarehouseChangeByIds(Long[] ids);
}