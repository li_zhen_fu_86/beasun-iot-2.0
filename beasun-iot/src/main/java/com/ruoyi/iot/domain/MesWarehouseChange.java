package com.ruoyi.iot.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * mes-库存流水-制单日期对象 mes_warehouse_change
 *
 * @author swingli
 * @date 2023-12-25
 */
public class MesWarehouseChange extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标识 */
    private Long id;

    /** 货品名称 */
    @Excel(name = "货品名称")
    private String goodsName;

    /** 库存数量 */
    @Excel(name = "库存数量")
    private Long warehouseCount;

    /** 变更数量 */
    @Excel(name = "变更数量")
    private Long changeCount;

    /** 变更类型 */
    @Excel(name = "变更类型")
    private Long changeType;

    /** 变更后库存 */
    @Excel(name = "变更后库存")
    private Long changeWarehouseCount;

    /** 货位名称 */
    @Excel(name = "货位名称")
    private String goodsAreaName;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String warehouseName;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setGoodsName(String goodsName)
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName()
    {
        return goodsName;
    }
    public void setWarehouseCount(Long warehouseCount)
    {
        this.warehouseCount = warehouseCount;
    }

    public Long getWarehouseCount()
    {
        return warehouseCount;
    }
    public void setChangeCount(Long changeCount)
    {
        this.changeCount = changeCount;
    }

    public Long getChangeCount()
    {
        return changeCount;
    }
    public void setChangeType(Long changeType)
    {
        this.changeType = changeType;
    }

    public Long getChangeType()
    {
        return changeType;
    }
    public void setChangeWarehouseCount(Long changeWarehouseCount)
    {
        this.changeWarehouseCount = changeWarehouseCount;
    }

    public Long getChangeWarehouseCount()
    {
        return changeWarehouseCount;
    }
    public void setGoodsAreaName(String goodsAreaName)
    {
        this.goodsAreaName = goodsAreaName;
    }

    public String getGoodsAreaName()
    {
        return goodsAreaName;
    }
    public void setWarehouseName(String warehouseName)
    {
        this.warehouseName = warehouseName;
    }

    public String getWarehouseName()
    {
        return warehouseName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("goodsName", getGoodsName())
                .append("warehouseCount", getWarehouseCount())
                .append("changeCount", getChangeCount())
                .append("changeType", getChangeType())
                .append("changeWarehouseCount", getChangeWarehouseCount())
                .append("goodsAreaName", getGoodsAreaName())
                .append("warehouseName", getWarehouseName())
                .toString();
    }
}