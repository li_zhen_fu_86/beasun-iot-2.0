package com.ruoyi.iot.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 版本管理-固件版管理对象 ver_phisical
 *
 * @author swingli
 * @date 2023-12-23
 */
public class VerPhisical extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 产品代码 */
    @Excel(name = "产品代码")
    private String produceCode;

    /** 固件名称 */
    @Excel(name = "固件名称")
    private String phisicName;

    /** 固件版本 */
    @Excel(name = "固件版本")
    private String phisicVersion;

    /** 固件类型 */
    @Excel(name = "固件类型")
    private String phisicType;

    /** 厂商 */
    @Excel(name = "厂商")
    private String manufacturer;

    /** 升级方式 */
    @Excel(name = "升级方式")
    private String updateType;

    /** 升级时长（秒） */
    @Excel(name = "升级时长", readConverterExp = "秒=")
    private Long updateSeconds;

    /** 说明 */
    @Excel(name = "说明")
    private String comment;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setProduceCode(String produceCode)
    {
        this.produceCode = produceCode;
    }

    public String getProduceCode()
    {
        return produceCode;
    }
    public void setPhisicName(String phisicName)
    {
        this.phisicName = phisicName;
    }

    public String getPhisicName()
    {
        return phisicName;
    }
    public void setPhisicVersion(String phisicVersion)
    {
        this.phisicVersion = phisicVersion;
    }

    public String getPhisicVersion()
    {
        return phisicVersion;
    }
    public void setPhisicType(String phisicType)
    {
        this.phisicType = phisicType;
    }

    public String getPhisicType()
    {
        return phisicType;
    }
    public void setManufacturer(String manufacturer)
    {
        this.manufacturer = manufacturer;
    }

    public String getManufacturer()
    {
        return manufacturer;
    }
    public void setUpdateType(String updateType)
    {
        this.updateType = updateType;
    }

    public String getUpdateType()
    {
        return updateType;
    }
    public void setUpdateSeconds(Long updateSeconds)
    {
        this.updateSeconds = updateSeconds;
    }

    public Long getUpdateSeconds()
    {
        return updateSeconds;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getComment()
    {
        return comment;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("produceCode", getProduceCode())
                .append("phisicName", getPhisicName())
                .append("phisicVersion", getPhisicVersion())
                .append("phisicType", getPhisicType())
                .append("manufacturer", getManufacturer())
                .append("updateType", getUpdateType())
                .append("updateSeconds", getUpdateSeconds())
                .append("updateTime", getUpdateTime())
                .append("updateBy", getUpdateBy())
                .append("comment", getComment())
                .toString();
    }
}