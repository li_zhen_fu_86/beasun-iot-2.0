package com.ruoyi.iot.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * mes-包装单明细对象 mes_package_order_detail
 *
 * @author swingli
 * @date 2023-12-25
 */
public class MesPackageOrderDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标识 */
    private Long id;

    /** 品号 */
    @Excel(name = "品号")
    private String produceNo;

    /** 品名 */
    @Excel(name = "品名")
    private String proudceName;

    /** 批号 */
    @Excel(name = "批号")
    private String batchNo;

    /** 已入库总数量 */
    @Excel(name = "已入库总数量")
    private Long inWarehouseTotalCounts;

    /** 未入库总数量 */
    @Excel(name = "未入库总数量")
    private Long noWarehouseTotalCounts;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setProduceNo(String produceNo)
    {
        this.produceNo = produceNo;
    }

    public String getProduceNo()
    {
        return produceNo;
    }
    public void setProudceName(String proudceName)
    {
        this.proudceName = proudceName;
    }

    public String getProudceName()
    {
        return proudceName;
    }
    public void setBatchNo(String batchNo)
    {
        this.batchNo = batchNo;
    }

    public String getBatchNo()
    {
        return batchNo;
    }
    public void setInWarehouseTotalCounts(Long inWarehouseTotalCounts)
    {
        this.inWarehouseTotalCounts = inWarehouseTotalCounts;
    }

    public Long getInWarehouseTotalCounts()
    {
        return inWarehouseTotalCounts;
    }
    public void setNoWarehouseTotalCounts(Long noWarehouseTotalCounts)
    {
        this.noWarehouseTotalCounts = noWarehouseTotalCounts;
    }

    public Long getNoWarehouseTotalCounts()
    {
        return noWarehouseTotalCounts;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("produceNo", getProduceNo())
                .append("proudceName", getProudceName())
                .append("batchNo", getBatchNo())
                .append("inWarehouseTotalCounts", getInWarehouseTotalCounts())
                .append("noWarehouseTotalCounts", getNoWarehouseTotalCounts())
                .append("createTime", getCreateTime())
                .toString();
    }
}