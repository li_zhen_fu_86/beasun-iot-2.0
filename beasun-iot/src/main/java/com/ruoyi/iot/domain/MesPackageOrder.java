package com.ruoyi.iot.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * mes-包装单对象 mes_package_order
 *
 * @author swingli
 * @date 2023-12-25
 */
public class MesPackageOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标识 */
    private Long id;

    /** 包装单号 */
    @Excel(name = "包装单号")
    private String orderNo;

    /** Mes对应单据 */
    @Excel(name = "Mes对应单据")
    private String meshNo;

    /** 包装名称 */
    @Excel(name = "包装名称")
    private String name;

    /** 货品数量 */
    @Excel(name = "货品数量")
    private String goodsCounts;

    /** 货品名称 */
    @Excel(name = "货品名称")
    private String goodsName;

    /** 是否已入库 */
    @Excel(name = "是否已入库")
    private Long isInWarehouse;

    /** 容器编号 */
    @Excel(name = "容器编号")
    private String containerNo;

    /** 容器数量 */
    @Excel(name = "容器数量")
    private Long containerCounts;

    /** 包装日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "包装日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date time;

    /** ERP对应单据 */
    @Excel(name = "ERP对应单据")
    private String erpNo;

    /** 单据日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "单据日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date orderTime;

    /** 预到货单 */
    @Excel(name = "预到货单")
    private String produceNo;

    /** 批次 */
    @Excel(name = "批次")
    private String batchNo;

    /** 入库单号 */
    @Excel(name = "入库单号")
    private String inOrderNo;

    /** 出库单号 */
    @Excel(name = "出库单号")
    private String outOrderNo;

    /** 出库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出库时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date outWarehouseTime;

    /** 部门编号 */
    @Excel(name = "部门编号")
    private String deptNo;

    /** 部门名称 */
    @Excel(name = "部门名称")
    private String deptName;

    /** 说明 */
    @Excel(name = "说明")
    private String comment;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setOrderNo(String orderNo)
    {
        this.orderNo = orderNo;
    }

    public String getOrderNo()
    {
        return orderNo;
    }
    public void setMeshNo(String meshNo)
    {
        this.meshNo = meshNo;
    }

    public String getMeshNo()
    {
        return meshNo;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setGoodsCounts(String goodsCounts)
    {
        this.goodsCounts = goodsCounts;
    }

    public String getGoodsCounts()
    {
        return goodsCounts;
    }
    public void setGoodsName(String goodsName)
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName()
    {
        return goodsName;
    }
    public void setIsInWarehouse(Long isInWarehouse)
    {
        this.isInWarehouse = isInWarehouse;
    }

    public Long getIsInWarehouse()
    {
        return isInWarehouse;
    }
    public void setContainerNo(String containerNo)
    {
        this.containerNo = containerNo;
    }

    public String getContainerNo()
    {
        return containerNo;
    }
    public void setContainerCounts(Long containerCounts)
    {
        this.containerCounts = containerCounts;
    }

    public Long getContainerCounts()
    {
        return containerCounts;
    }
    public void setTime(Date time)
    {
        this.time = time;
    }

    public Date getTime()
    {
        return time;
    }
    public void setErpNo(String erpNo)
    {
        this.erpNo = erpNo;
    }

    public String getErpNo()
    {
        return erpNo;
    }
    public void setOrderTime(Date orderTime)
    {
        this.orderTime = orderTime;
    }

    public Date getOrderTime()
    {
        return orderTime;
    }
    public void setProduceNo(String produceNo)
    {
        this.produceNo = produceNo;
    }

    public String getProduceNo()
    {
        return produceNo;
    }
    public void setBatchNo(String batchNo)
    {
        this.batchNo = batchNo;
    }

    public String getBatchNo()
    {
        return batchNo;
    }
    public void setInOrderNo(String inOrderNo)
    {
        this.inOrderNo = inOrderNo;
    }

    public String getInOrderNo()
    {
        return inOrderNo;
    }
    public void setOutOrderNo(String outOrderNo)
    {
        this.outOrderNo = outOrderNo;
    }

    public String getOutOrderNo()
    {
        return outOrderNo;
    }
    public void setOutWarehouseTime(Date outWarehouseTime)
    {
        this.outWarehouseTime = outWarehouseTime;
    }

    public Date getOutWarehouseTime()
    {
        return outWarehouseTime;
    }
    public void setDeptNo(String deptNo)
    {
        this.deptNo = deptNo;
    }

    public String getDeptNo()
    {
        return deptNo;
    }
    public void setDeptName(String deptName)
    {
        this.deptName = deptName;
    }

    public String getDeptName()
    {
        return deptName;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getComment()
    {
        return comment;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("orderNo", getOrderNo())
                .append("meshNo", getMeshNo())
                .append("name", getName())
                .append("goodsCounts", getGoodsCounts())
                .append("goodsName", getGoodsName())
                .append("isInWarehouse", getIsInWarehouse())
                .append("containerNo", getContainerNo())
                .append("containerCounts", getContainerCounts())
                .append("time", getTime())
                .append("erpNo", getErpNo())
                .append("orderTime", getOrderTime())
                .append("produceNo", getProduceNo())
                .append("batchNo", getBatchNo())
                .append("inOrderNo", getInOrderNo())
                .append("outOrderNo", getOutOrderNo())
                .append("outWarehouseTime", getOutWarehouseTime())
                .append("deptNo", getDeptNo())
                .append("deptName", getDeptName())
                .append("createTime", getCreateTime())
                .append("comment", getComment())
                .toString();
    }
}