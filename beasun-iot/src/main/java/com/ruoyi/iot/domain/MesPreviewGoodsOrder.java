package com.ruoyi.iot.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * mes-预到货单对象 mes_preview_goods_order
 *
 * @author swingli
 * @date 2023-12-23
 */
public class MesPreviewGoodsOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标识 */
    private Long id;

    /** 出库单号 */
    @Excel(name = "出库单号")
    private String orderNo;

    /** ERP单号 */
    @Excel(name = "ERP单号")
    private String erpNo;

    /** 预到货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "预到货时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date previewTime;

    /** 客户名称 */
    @Excel(name = "客户名称")
    private String customName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String deptName;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String housewareName;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private String orderStatus;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String companyName;

    /** 经手人 */
    @Excel(name = "经手人")
    private String manageBy;

    /** 备料单号 */
    @Excel(name = "备料单号")
    private Long materialOrderNo;

    /** 制单日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "制单日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date orderDate;

    /** 单据类型 */
    @Excel(name = "单据类型")
    private Long orderType;

    /** 	 */
    @Excel(name = "	")
    private Long auditStatus;

    /** 备注 */
    @Excel(name = "备注")
    private String comment;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setOrderNo(String orderNo)
    {
        this.orderNo = orderNo;
    }

    public String getOrderNo()
    {
        return orderNo;
    }
    public void setErpNo(String erpNo)
    {
        this.erpNo = erpNo;
    }

    public String getErpNo()
    {
        return erpNo;
    }
    public void setPreviewTime(Date previewTime)
    {
        this.previewTime = previewTime;
    }

    public Date getPreviewTime()
    {
        return previewTime;
    }
    public void setCustomName(String customName)
    {
        this.customName = customName;
    }

    public String getCustomName()
    {
        return customName;
    }
    public void setDeptName(String deptName)
    {
        this.deptName = deptName;
    }

    public String getDeptName()
    {
        return deptName;
    }
    public void setHousewareName(String housewareName)
    {
        this.housewareName = housewareName;
    }

    public String getHousewareName()
    {
        return housewareName;
    }
    public void setOrderStatus(String orderStatus)
    {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatus()
    {
        return orderStatus;
    }
    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getCompanyName()
    {
        return companyName;
    }
    public void setManageBy(String manageBy)
    {
        this.manageBy = manageBy;
    }

    public String getManageBy()
    {
        return manageBy;
    }
    public void setMaterialOrderNo(Long materialOrderNo)
    {
        this.materialOrderNo = materialOrderNo;
    }

    public Long getMaterialOrderNo()
    {
        return materialOrderNo;
    }
    public void setOrderDate(Date orderDate)
    {
        this.orderDate = orderDate;
    }

    public Date getOrderDate()
    {
        return orderDate;
    }
    public void setOrderType(Long orderType)
    {
        this.orderType = orderType;
    }

    public Long getOrderType()
    {
        return orderType;
    }
    public void setAuditStatus(Long auditStatus)
    {
        this.auditStatus = auditStatus;
    }

    public Long getAuditStatus()
    {
        return auditStatus;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getComment()
    {
        return comment;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("orderNo", getOrderNo())
                .append("erpNo", getErpNo())
                .append("previewTime", getPreviewTime())
                .append("customName", getCustomName())
                .append("deptName", getDeptName())
                .append("housewareName", getHousewareName())
                .append("orderStatus", getOrderStatus())
                .append("companyName", getCompanyName())
                .append("manageBy", getManageBy())
                .append("materialOrderNo", getMaterialOrderNo())
                .append("orderDate", getOrderDate())
                .append("orderType", getOrderType())
                .append("createBy", getCreateBy())
                .append("auditStatus", getAuditStatus())
                .append("comment", getComment())
                .toString();
    }
}