package com.ruoyi.iot.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 任务中心-任务管理对象 iot_task
 *
 * @author swingli
 * @date 2023-12-23
 */
public class Task extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 任务名称 */
    @Excel(name = "任务名称")
    private String name;

    /** 目标类型（1：设备 2：产品） */
    @Excel(name = "目标类型", readConverterExp = "1=：设备,2=：产品")
    private Long targetType;

    /** 设备标识 */
    @Excel(name = "设备标识")
    private String deviceId;

    /** 任务类型 */
    @Excel(name = "任务类型")
    private Long type;

    /** 自定义数据流 */
    @Excel(name = "自定义数据流")
    private String seflDefineData;

    /** 时钟时间 */
    @Excel(name = "时钟时间")
    private String cronTime;

    /** 说明 */
    @Excel(name = "说明")
    private String comment;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setTargetType(Long targetType)
    {
        this.targetType = targetType;
    }

    public Long getTargetType()
    {
        return targetType;
    }
    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }

    public String getDeviceId()
    {
        return deviceId;
    }
    public void setType(Long type)
    {
        this.type = type;
    }

    public Long getType()
    {
        return type;
    }
    public void setSeflDefineData(String seflDefineData)
    {
        this.seflDefineData = seflDefineData;
    }

    public String getSeflDefineData()
    {
        return seflDefineData;
    }
    public void setCronTime(String cronTime)
    {
        this.cronTime = cronTime;
    }

    public String getCronTime()
    {
        return cronTime;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getComment()
    {
        return comment;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("name", getName())
                .append("targetType", getTargetType())
                .append("deviceId", getDeviceId())
                .append("type", getType())
                .append("seflDefineData", getSeflDefineData())
                .append("cronTime", getCronTime())
                .append("comment", getComment())
                .toString();
    }
}