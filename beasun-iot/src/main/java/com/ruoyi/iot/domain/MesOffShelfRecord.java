package com.ruoyi.iot.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * mes-下架记录单对象 mes_off_shelf_record
 *
 * @author swingli
 * @date 2023-12-25
 */
public class MesOffShelfRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标识 */
    private Long id;

    /** Mes对应单据 */
    @Excel(name = "Mes对应单据")
    private String mesNo;

    /** ERP对应单据 */
    @Excel(name = "ERP对应单据")
    private String erpNo;

    /** 出库单号 */
    @Excel(name = "出库单号")
    private String outWarehouseOrderNo;

    /** 批次单号 */
    @Excel(name = "批次单号")
    private String batchNo;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private Long goodsCounts;

    /** 出库仓 */
    @Excel(name = "出库仓")
    private String outWarehouse;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setMesNo(String mesNo)
    {
        this.mesNo = mesNo;
    }

    public String getMesNo()
    {
        return mesNo;
    }
    public void setErpNo(String erpNo)
    {
        this.erpNo = erpNo;
    }

    public String getErpNo()
    {
        return erpNo;
    }
    public void setOutWarehouseOrderNo(String outWarehouseOrderNo)
    {
        this.outWarehouseOrderNo = outWarehouseOrderNo;
    }

    public String getOutWarehouseOrderNo()
    {
        return outWarehouseOrderNo;
    }
    public void setBatchNo(String batchNo)
    {
        this.batchNo = batchNo;
    }

    public String getBatchNo()
    {
        return batchNo;
    }
    public void setGoodsCounts(Long goodsCounts)
    {
        this.goodsCounts = goodsCounts;
    }

    public Long getGoodsCounts()
    {
        return goodsCounts;
    }
    public void setOutWarehouse(String outWarehouse)
    {
        this.outWarehouse = outWarehouse;
    }

    public String getOutWarehouse()
    {
        return outWarehouse;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("mesNo", getMesNo())
                .append("erpNo", getErpNo())
                .append("outWarehouseOrderNo", getOutWarehouseOrderNo())
                .append("batchNo", getBatchNo())
                .append("goodsCounts", getGoodsCounts())
                .append("outWarehouse", getOutWarehouse())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .toString();
    }
}