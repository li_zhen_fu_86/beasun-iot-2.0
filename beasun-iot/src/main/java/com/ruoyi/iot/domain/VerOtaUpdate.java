package com.ruoyi.iot.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 版本管理-OTA升级广利对象 ver_ota_update
 *
 * @author swingli
 * @date 2023-12-23
 */
public class VerOtaUpdate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 客户名称 */
    @Excel(name = "客户名称")
    private String customName;

    /** 产品类型 */
    @Excel(name = "产品类型")
    private String produceType;

    /** 固件名称 */
    @Excel(name = "固件名称")
    private String phsicsName;

    /** 固件版本 */
    @Excel(name = "固件版本")
    private String phsicsVersion;

    /** 升级状态 */
    @Excel(name = "升级状态")
    private Long updateStatus;

    /** 升级数量 */
    @Excel(name = "升级数量")
    private Long updateCount;

    /** 备注 */
    @Excel(name = "备注")
    private String comment;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCustomName(String customName)
    {
        this.customName = customName;
    }

    public String getCustomName()
    {
        return customName;
    }
    public void setProduceType(String produceType)
    {
        this.produceType = produceType;
    }

    public String getProduceType()
    {
        return produceType;
    }
    public void setPhsicsName(String phsicsName)
    {
        this.phsicsName = phsicsName;
    }

    public String getPhsicsName()
    {
        return phsicsName;
    }
    public void setPhsicsVersion(String phsicsVersion)
    {
        this.phsicsVersion = phsicsVersion;
    }

    public String getPhsicsVersion()
    {
        return phsicsVersion;
    }
    public void setUpdateStatus(Long updateStatus)
    {
        this.updateStatus = updateStatus;
    }

    public Long getUpdateStatus()
    {
        return updateStatus;
    }
    public void setUpdateCount(Long updateCount)
    {
        this.updateCount = updateCount;
    }

    public Long getUpdateCount()
    {
        return updateCount;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getComment()
    {
        return comment;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("customName", getCustomName())
                .append("produceType", getProduceType())
                .append("phsicsName", getPhsicsName())
                .append("phsicsVersion", getPhsicsVersion())
                .append("updateStatus", getUpdateStatus())
                .append("updateCount", getUpdateCount())
                .append("createTime", getCreateTime())
                .append("createBy", getCreateBy())
                .append("comment", getComment())
                .toString();
    }
}