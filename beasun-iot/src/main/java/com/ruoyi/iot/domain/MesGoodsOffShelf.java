package com.ruoyi.iot.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * mes-出库下架作业对象 mes_goods_off_shelf
 *
 * @author swingli
 * @date 2023-12-25
 */
public class MesGoodsOffShelf extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标识 */
    private Long id;

    /** 货品编号 */
    @Excel(name = "货品编号")
    private String goodsNo;

    /** 货品名称 */
    @Excel(name = "货品名称")
    private String goodsName;

    /** 批次号 */
    @Excel(name = "批次号")
    private String batchNo;

    /** 可下架数量 */
    @Excel(name = "可下架数量")
    private Long usefullOffGoodsCount;

    /** 下架数量 */
    @Excel(name = "下架数量")
    private Long offGoodsCount;

    /** 序列号 */
    @Excel(name = "序列号")
    private String serialNo;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String warehouseName;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setGoodsNo(String goodsNo)
    {
        this.goodsNo = goodsNo;
    }

    public String getGoodsNo()
    {
        return goodsNo;
    }
    public void setGoodsName(String goodsName)
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName()
    {
        return goodsName;
    }
    public void setBatchNo(String batchNo)
    {
        this.batchNo = batchNo;
    }

    public String getBatchNo()
    {
        return batchNo;
    }
    public void setUsefullOffGoodsCount(Long usefullOffGoodsCount)
    {
        this.usefullOffGoodsCount = usefullOffGoodsCount;
    }

    public Long getUsefullOffGoodsCount()
    {
        return usefullOffGoodsCount;
    }
    public void setOffGoodsCount(Long offGoodsCount)
    {
        this.offGoodsCount = offGoodsCount;
    }

    public Long getOffGoodsCount()
    {
        return offGoodsCount;
    }
    public void setSerialNo(String serialNo)
    {
        this.serialNo = serialNo;
    }

    public String getSerialNo()
    {
        return serialNo;
    }
    public void setWarehouseName(String warehouseName)
    {
        this.warehouseName = warehouseName;
    }

    public String getWarehouseName()
    {
        return warehouseName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("goodsNo", getGoodsNo())
                .append("goodsName", getGoodsName())
                .append("batchNo", getBatchNo())
                .append("usefullOffGoodsCount", getUsefullOffGoodsCount())
                .append("offGoodsCount", getOffGoodsCount())
                .append("serialNo", getSerialNo())
                .append("warehouseName", getWarehouseName())
                .toString();
    }
}