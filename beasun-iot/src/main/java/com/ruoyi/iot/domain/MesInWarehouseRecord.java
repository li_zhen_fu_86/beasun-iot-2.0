package com.ruoyi.iot.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * mesh-入库记录单对象 mes_in_warehouse_record
 *
 * @author swingli
 * @date 2023-12-25
 */
public class MesInWarehouseRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标识 */
    private Long id;

    /** 单据日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "单据日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date orderTime;

    /** ERP单号 */
    @Excel(name = "ERP单号")
    private String erpNo;

    /** 预到货单 */
    @Excel(name = "预到货单")
    private String previewGoodsOrder;

    /** 单据类型 */
    @Excel(name = "单据类型")
    private Long orderType;

    /** 经手人 */
    @Excel(name = "经手人")
    private String manageBy;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String warehouseName;

    /** 状态 */
    @Excel(name = "状态")
    private Long status;

    /** 数量 */
    @Excel(name = "数量")
    private Long count;

    /** 推送ERP状态 */
    @Excel(name = "推送ERP状态")
    private Long pushStatus;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setOrderTime(Date orderTime)
    {
        this.orderTime = orderTime;
    }

    public Date getOrderTime()
    {
        return orderTime;
    }
    public void setErpNo(String erpNo)
    {
        this.erpNo = erpNo;
    }

    public String getErpNo()
    {
        return erpNo;
    }
    public void setPreviewGoodsOrder(String previewGoodsOrder)
    {
        this.previewGoodsOrder = previewGoodsOrder;
    }

    public String getPreviewGoodsOrder()
    {
        return previewGoodsOrder;
    }
    public void setOrderType(Long orderType)
    {
        this.orderType = orderType;
    }

    public Long getOrderType()
    {
        return orderType;
    }
    public void setManageBy(String manageBy)
    {
        this.manageBy = manageBy;
    }

    public String getManageBy()
    {
        return manageBy;
    }
    public void setWarehouseName(String warehouseName)
    {
        this.warehouseName = warehouseName;
    }

    public String getWarehouseName()
    {
        return warehouseName;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setCount(Long count)
    {
        this.count = count;
    }

    public Long getCount()
    {
        return count;
    }
    public void setPushStatus(Long pushStatus)
    {
        this.pushStatus = pushStatus;
    }

    public Long getPushStatus()
    {
        return pushStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("orderTime", getOrderTime())
                .append("erpNo", getErpNo())
                .append("previewGoodsOrder", getPreviewGoodsOrder())
                .append("orderType", getOrderType())
                .append("manageBy", getManageBy())
                .append("warehouseName", getWarehouseName())
                .append("status", getStatus())
                .append("count", getCount())
                .append("pushStatus", getPushStatus())
                .toString();
    }
}