package com.ruoyi.iot.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * mes-序列号库存对象 mes_serial_warehouse
 *
 * @author swingli
 * @date 2023-12-25
 */
public class MesSerialWarehouse extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标识 */
    private Long id;

    /** 仓库编号 */
    @Excel(name = "仓库编号")
    private String warehouseNo;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String warehouseName;

    /** 货位名称 */
    @Excel(name = "货位名称")
    private String goodsAreaName;

    /** 序列号 */
    @Excel(name = "序列号")
    private String serialNo;

    /** 容器号 */
    @Excel(name = "容器号")
    private String containerNo;

    /** 入库单 */
    @Excel(name = "入库单")
    private String inWarehouseNo;

    /** 任务单 */
    @Excel(name = "任务单")
    private String taskNo;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setWarehouseNo(String warehouseNo)
    {
        this.warehouseNo = warehouseNo;
    }

    public String getWarehouseNo()
    {
        return warehouseNo;
    }
    public void setWarehouseName(String warehouseName)
    {
        this.warehouseName = warehouseName;
    }

    public String getWarehouseName()
    {
        return warehouseName;
    }
    public void setGoodsAreaName(String goodsAreaName)
    {
        this.goodsAreaName = goodsAreaName;
    }

    public String getGoodsAreaName()
    {
        return goodsAreaName;
    }
    public void setSerialNo(String serialNo)
    {
        this.serialNo = serialNo;
    }

    public String getSerialNo()
    {
        return serialNo;
    }
    public void setContainerNo(String containerNo)
    {
        this.containerNo = containerNo;
    }

    public String getContainerNo()
    {
        return containerNo;
    }
    public void setInWarehouseNo(String inWarehouseNo)
    {
        this.inWarehouseNo = inWarehouseNo;
    }

    public String getInWarehouseNo()
    {
        return inWarehouseNo;
    }
    public void setTaskNo(String taskNo)
    {
        this.taskNo = taskNo;
    }

    public String getTaskNo()
    {
        return taskNo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("warehouseNo", getWarehouseNo())
                .append("warehouseName", getWarehouseName())
                .append("goodsAreaName", getGoodsAreaName())
                .append("serialNo", getSerialNo())
                .append("containerNo", getContainerNo())
                .append("inWarehouseNo", getInWarehouseNo())
                .append("taskNo", getTaskNo())
                .toString();
    }
}