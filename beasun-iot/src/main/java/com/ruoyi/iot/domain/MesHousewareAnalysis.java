package com.ruoyi.iot.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * mes-库存分析对象 mes_houseware_analysis
 *
 * @author swingli
 * @date 2023-12-23
 */
public class MesHousewareAnalysis extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标识 */
    private Long id;

    /** 备料单号 */
    @Excel(name = "备料单号")
    private String materialOrderNo;

    /** 调出仓库名称 */
    @Excel(name = "调出仓库名称")
    private String outWarehouseName;

    /** 调入仓库名称 */
    @Excel(name = "调入仓库名称")
    private String inWarehouseName;

    /** 超领单号 */
    @Excel(name = "超领单号")
    private String moreOrderNo;

    /** 是否优先车间仓 */
    @Excel(name = "是否优先车间仓")
    private Long isFirstWarehouse;

    /** 是否同仓占用 */
    @Excel(name = "是否同仓占用")
    private Long isSameWarehouse;

    /** 任务单号 */
    @Excel(name = "任务单号")
    private Long taskNo;

    /** 说明 */
    @Excel(name = "说明")
    private String comment;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setMaterialOrderNo(String materialOrderNo)
    {
        this.materialOrderNo = materialOrderNo;
    }

    public String getMaterialOrderNo()
    {
        return materialOrderNo;
    }
    public void setOutWarehouseName(String outWarehouseName)
    {
        this.outWarehouseName = outWarehouseName;
    }

    public String getOutWarehouseName()
    {
        return outWarehouseName;
    }
    public void setInWarehouseName(String inWarehouseName)
    {
        this.inWarehouseName = inWarehouseName;
    }

    public String getInWarehouseName()
    {
        return inWarehouseName;
    }
    public void setMoreOrderNo(String moreOrderNo)
    {
        this.moreOrderNo = moreOrderNo;
    }

    public String getMoreOrderNo()
    {
        return moreOrderNo;
    }
    public void setIsFirstWarehouse(Long isFirstWarehouse)
    {
        this.isFirstWarehouse = isFirstWarehouse;
    }

    public Long getIsFirstWarehouse()
    {
        return isFirstWarehouse;
    }
    public void setIsSameWarehouse(Long isSameWarehouse)
    {
        this.isSameWarehouse = isSameWarehouse;
    }

    public Long getIsSameWarehouse()
    {
        return isSameWarehouse;
    }
    public void setTaskNo(Long taskNo)
    {
        this.taskNo = taskNo;
    }

    public Long getTaskNo()
    {
        return taskNo;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getComment()
    {
        return comment;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("materialOrderNo", getMaterialOrderNo())
                .append("outWarehouseName", getOutWarehouseName())
                .append("inWarehouseName", getInWarehouseName())
                .append("moreOrderNo", getMoreOrderNo())
                .append("isFirstWarehouse", getIsFirstWarehouse())
                .append("isSameWarehouse", getIsSameWarehouse())
                .append("taskNo", getTaskNo())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("comment", getComment())
                .toString();
    }
}