package com.ruoyi.iot.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 运维管理-采集模板对象 oper_template
 *
 * @author swingli
 * @date 2023-12-22
 */
public class OperTemplate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 模版标识 */
    private Long id;

    /** 模板名称 */
    @Excel(name = "模板名称")
    private String name;

    /** 采集方式 */
    @Excel(name = "采集方式")
    private String type;

    /** 从机/变量 */
    @Excel(name = "从机/变量")
    private String properties;

    /** 模板说明 */
    @Excel(name = "模板说明")
    private String content;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }
    public void setProperties(String properties)
    {
        this.properties = properties;
    }

    public String getProperties()
    {
        return properties;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    public String getContent()
    {
        return content;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("name", getName())
                .append("type", getType())
                .append("properties", getProperties())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("content", getContent())
                .toString();
    }
}