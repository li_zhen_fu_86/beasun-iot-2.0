package com.ruoyi.iot.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * mes-库存信息对象 mes_warehouse_info
 *
 * @author swingli
 * @date 2023-12-25
 */
public class MesWarehouseInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标识 */
    private Long id;

    /** 入库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "入库时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date inTime;

    /** 批号 */
    @Excel(name = "批号")
    private String batchNo;

    /** 品号 */
    @Excel(name = "品号")
    private String produceNo;

    /** 品名 */
    @Excel(name = "品名")
    private String produceName;

    /** 规格 */
    @Excel(name = "规格")
    private String norms;

    /** 往来单位 */
    @Excel(name = "往来单位")
    private String companyName;

    /** 分类名称 */
    @Excel(name = "分类名称")
    private String catagaryName;

    /** 启用序列 */
    @Excel(name = "启用序列")
    private String sortList;

    /** 仓库编号 */
    @Excel(name = "仓库编号")
    private String warehouseNo;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String warehouseName;

    /** 库区名称 */
    @Excel(name = "库区名称")
    private String werehousePart;

    /** 货位名称 */
    @Excel(name = "货位名称")
    private String goodsAreaName;

    /** 库存状态 */
    @Excel(name = "库存状态")
    private Long status;

    /** 库存数 */
    @Excel(name = "库存数")
    private Long count;

    /** 占用数 */
    @Excel(name = "占用数")
    private Long onCount;

    /** 有效库存 */
    @Excel(name = "有效库存")
    private Long reallyCount;

    /** 说明 */
    @Excel(name = "说明")
    private String comment;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setInTime(Date inTime)
    {
        this.inTime = inTime;
    }

    public Date getInTime()
    {
        return inTime;
    }
    public void setBatchNo(String batchNo)
    {
        this.batchNo = batchNo;
    }

    public String getBatchNo()
    {
        return batchNo;
    }
    public void setProduceNo(String produceNo)
    {
        this.produceNo = produceNo;
    }

    public String getProduceNo()
    {
        return produceNo;
    }
    public void setProduceName(String produceName)
    {
        this.produceName = produceName;
    }

    public String getProduceName()
    {
        return produceName;
    }
    public void setNorms(String norms)
    {
        this.norms = norms;
    }

    public String getNorms()
    {
        return norms;
    }
    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getCompanyName()
    {
        return companyName;
    }
    public void setCatagaryName(String catagaryName)
    {
        this.catagaryName = catagaryName;
    }

    public String getCatagaryName()
    {
        return catagaryName;
    }
    public void setSortList(String sortList)
    {
        this.sortList = sortList;
    }

    public String getSortList()
    {
        return sortList;
    }
    public void setWarehouseNo(String warehouseNo)
    {
        this.warehouseNo = warehouseNo;
    }

    public String getWarehouseNo()
    {
        return warehouseNo;
    }
    public void setWarehouseName(String warehouseName)
    {
        this.warehouseName = warehouseName;
    }

    public String getWarehouseName()
    {
        return warehouseName;
    }
    public void setWerehousePart(String werehousePart)
    {
        this.werehousePart = werehousePart;
    }

    public String getWerehousePart()
    {
        return werehousePart;
    }
    public void setGoodsAreaName(String goodsAreaName)
    {
        this.goodsAreaName = goodsAreaName;
    }

    public String getGoodsAreaName()
    {
        return goodsAreaName;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setCount(Long count)
    {
        this.count = count;
    }

    public Long getCount()
    {
        return count;
    }
    public void setOnCount(Long onCount)
    {
        this.onCount = onCount;
    }

    public Long getOnCount()
    {
        return onCount;
    }
    public void setReallyCount(Long reallyCount)
    {
        this.reallyCount = reallyCount;
    }

    public Long getReallyCount()
    {
        return reallyCount;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getComment()
    {
        return comment;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("inTime", getInTime())
                .append("batchNo", getBatchNo())
                .append("produceNo", getProduceNo())
                .append("produceName", getProduceName())
                .append("norms", getNorms())
                .append("companyName", getCompanyName())
                .append("catagaryName", getCatagaryName())
                .append("sortList", getSortList())
                .append("warehouseNo", getWarehouseNo())
                .append("warehouseName", getWarehouseName())
                .append("werehousePart", getWerehousePart())
                .append("goodsAreaName", getGoodsAreaName())
                .append("status", getStatus())
                .append("count", getCount())
                .append("onCount", getOnCount())
                .append("reallyCount", getReallyCount())
                .append("comment", getComment())
                .toString();
    }
}