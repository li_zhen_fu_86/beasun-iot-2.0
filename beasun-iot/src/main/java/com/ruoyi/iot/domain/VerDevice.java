package com.ruoyi.iot.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 版本管理-硬件版本定义对象 ver_device
 *
 * @author swingli
 * @date 2023-12-23
 */
public class VerDevice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 版本号 */
    @Excel(name = "版本号")
    private String verNo;

    /** 通信方式 */
    @Excel(name = "通信方式")
    private String linkType;

    /** 产品类型 */
    @Excel(name = "产品类型")
    private String productType;

    /** 厂商 */
    @Excel(name = "厂商")
    private String manufacturer;

    /** 型号 */
    @Excel(name = "型号")
    private String modelNo;

    /** 备注 */
    @Excel(name = "备注")
    private String comment;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setVerNo(String verNo)
    {
        this.verNo = verNo;
    }

    public String getVerNo()
    {
        return verNo;
    }
    public void setLinkType(String linkType)
    {
        this.linkType = linkType;
    }

    public String getLinkType()
    {
        return linkType;
    }
    public void setProductType(String productType)
    {
        this.productType = productType;
    }

    public String getProductType()
    {
        return productType;
    }
    public void setManufacturer(String manufacturer)
    {
        this.manufacturer = manufacturer;
    }

    public String getManufacturer()
    {
        return manufacturer;
    }
    public void setModelNo(String modelNo)
    {
        this.modelNo = modelNo;
    }

    public String getModelNo()
    {
        return modelNo;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getComment()
    {
        return comment;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("verNo", getVerNo())
                .append("linkType", getLinkType())
                .append("productType", getProductType())
                .append("manufacturer", getManufacturer())
                .append("modelNo", getModelNo())
                .append("updateTime", getUpdateTime())
                .append("updateBy", getUpdateBy())
                .append("comment", getComment())
                .toString();
    }
}