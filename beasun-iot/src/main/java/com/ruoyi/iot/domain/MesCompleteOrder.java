package com.ruoyi.iot.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * mes-完工汇报单对象 mes_complete_order
 *
 * @author swingli
 * @date 2023-12-23
 */
public class MesCompleteOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标识 */
    private Long id;

    /** 单据日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "单据日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date orderTime;

    /** 经手人 */
    @Excel(name = "经手人")
    private String manageBy;

    /** 部门名称 */
    @Excel(name = "部门名称")
    private String deptName;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String warehouseName;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private String orderStatus;

    /** 审核状态 */
    @Excel(name = "审核状态")
    private Long auditStatus;

    /** 备注 */
    @Excel(name = "备注")
    private String comment;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setOrderTime(Date orderTime)
    {
        this.orderTime = orderTime;
    }

    public Date getOrderTime()
    {
        return orderTime;
    }
    public void setManageBy(String manageBy)
    {
        this.manageBy = manageBy;
    }

    public String getManageBy()
    {
        return manageBy;
    }
    public void setDeptName(String deptName)
    {
        this.deptName = deptName;
    }

    public String getDeptName()
    {
        return deptName;
    }
    public void setWarehouseName(String warehouseName)
    {
        this.warehouseName = warehouseName;
    }

    public String getWarehouseName()
    {
        return warehouseName;
    }
    public void setOrderStatus(String orderStatus)
    {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatus()
    {
        return orderStatus;
    }
    public void setAuditStatus(Long auditStatus)
    {
        this.auditStatus = auditStatus;
    }

    public Long getAuditStatus()
    {
        return auditStatus;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getComment()
    {
        return comment;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("orderTime", getOrderTime())
                .append("manageBy", getManageBy())
                .append("deptName", getDeptName())
                .append("warehouseName", getWarehouseName())
                .append("orderStatus", getOrderStatus())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("auditStatus", getAuditStatus())
                .append("comment", getComment())
                .toString();
    }
}