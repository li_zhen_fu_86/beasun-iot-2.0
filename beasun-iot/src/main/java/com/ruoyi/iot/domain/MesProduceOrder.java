package com.ruoyi.iot.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * mes-生产任务单对象 mes_produce_order
 *
 * @author swingli
 * @date 2023-12-23
 */
public class MesProduceOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 任务号 */
    @Excel(name = "任务号")
    private String taskNo;

    /** 订单号 */
    @Excel(name = "订单号")
    private String orderNo;

    /** 项目号 */
    @Excel(name = "项目号")
    private Long projectNo;

    /** 单据日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "单据日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date orderDate;

    /** 生成状态 */
    @Excel(name = "生成状态")
    private String makeStatus;

    /** 紧急程度 */
    @Excel(name = "紧急程度")
    private String orderLevel;

    /** 单据类型 */
    @Excel(name = "单据类型")
    private String orderType;

    /** 品名 */
    @Excel(name = "品名")
    private String produceName;

    /** 品号 */
    @Excel(name = "品号")
    private String produceNo;

    /** 批号 */
    @Excel(name = "批号")
    private Long batchNo;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setTaskNo(String taskNo)
    {
        this.taskNo = taskNo;
    }

    public String getTaskNo()
    {
        return taskNo;
    }
    public void setOrderNo(String orderNo)
    {
        this.orderNo = orderNo;
    }

    public String getOrderNo()
    {
        return orderNo;
    }
    public void setProjectNo(Long projectNo)
    {
        this.projectNo = projectNo;
    }

    public Long getProjectNo()
    {
        return projectNo;
    }
    public void setOrderDate(Date orderDate)
    {
        this.orderDate = orderDate;
    }

    public Date getOrderDate()
    {
        return orderDate;
    }
    public void setMakeStatus(String makeStatus)
    {
        this.makeStatus = makeStatus;
    }

    public String getMakeStatus()
    {
        return makeStatus;
    }
    public void setOrderLevel(String orderLevel)
    {
        this.orderLevel = orderLevel;
    }

    public String getOrderLevel()
    {
        return orderLevel;
    }
    public void setOrderType(String orderType)
    {
        this.orderType = orderType;
    }

    public String getOrderType()
    {
        return orderType;
    }
    public void setProduceName(String produceName)
    {
        this.produceName = produceName;
    }

    public String getProduceName()
    {
        return produceName;
    }
    public void setProduceNo(String produceNo)
    {
        this.produceNo = produceNo;
    }

    public String getProduceNo()
    {
        return produceNo;
    }
    public void setBatchNo(Long batchNo)
    {
        this.batchNo = batchNo;
    }

    public Long getBatchNo()
    {
        return batchNo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("taskNo", getTaskNo())
                .append("orderNo", getOrderNo())
                .append("projectNo", getProjectNo())
                .append("orderDate", getOrderDate())
                .append("makeStatus", getMakeStatus())
                .append("orderLevel", getOrderLevel())
                .append("orderType", getOrderType())
                .append("produceName", getProduceName())
                .append("produceNo", getProduceNo())
                .append("batchNo", getBatchNo())
                .toString();
    }
}