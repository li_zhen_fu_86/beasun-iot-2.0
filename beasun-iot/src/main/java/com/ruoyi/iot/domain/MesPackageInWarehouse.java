package com.ruoyi.iot.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * mes-包装入库对象 mes_package_in_warehouse
 *
 * @author swingli
 * @date 2023-12-25
 */
public class MesPackageInWarehouse extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标识 */
    private Long id;

    /** 序号 */
    @Excel(name = "序号")
    private Long orderNo;

    /** 品号 */
    @Excel(name = "品号")
    private String produceNo;

    /** 品名 */
    @Excel(name = "品名")
    private String produceName;

    /** 批次号 */
    @Excel(name = "批次号")
    private String batchNo;

    /** 入库数量 */
    @Excel(name = "入库数量")
    private Long inWarehouseCount;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String warehouseName;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setOrderNo(Long orderNo)
    {
        this.orderNo = orderNo;
    }

    public Long getOrderNo()
    {
        return orderNo;
    }
    public void setProduceNo(String produceNo)
    {
        this.produceNo = produceNo;
    }

    public String getProduceNo()
    {
        return produceNo;
    }
    public void setProduceName(String produceName)
    {
        this.produceName = produceName;
    }

    public String getProduceName()
    {
        return produceName;
    }
    public void setBatchNo(String batchNo)
    {
        this.batchNo = batchNo;
    }

    public String getBatchNo()
    {
        return batchNo;
    }
    public void setInWarehouseCount(Long inWarehouseCount)
    {
        this.inWarehouseCount = inWarehouseCount;
    }

    public Long getInWarehouseCount()
    {
        return inWarehouseCount;
    }
    public void setWarehouseName(String warehouseName)
    {
        this.warehouseName = warehouseName;
    }

    public String getWarehouseName()
    {
        return warehouseName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("orderNo", getOrderNo())
                .append("produceNo", getProduceNo())
                .append("produceName", getProduceName())
                .append("batchNo", getBatchNo())
                .append("inWarehouseCount", getInWarehouseCount())
                .append("warehouseName", getWarehouseName())
                .toString();
    }
}