package com.ruoyi.iot.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 版本管理-软件版本管理对象 ver_soft_ware
 *
 * @author swingli
 * @date 2023-12-23
 */
public class VerSoftWare extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 软件版本标识 */
    private Long id;

    /** 版本号 */
    @Excel(name = "版本号")
    private String verNo;

    /** 系统名称 */
    @Excel(name = "系统名称")
    private String system;

    /** 更新说明 */
    @Excel(name = "更新说明")
    private String updateInfo;

    /** 状态 */
    @Excel(name = "状态")
    private Long updateStatus;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setVerNo(String verNo)
    {
        this.verNo = verNo;
    }

    public String getVerNo()
    {
        return verNo;
    }
    public void setSystem(String system)
    {
        this.system = system;
    }

    public String getSystem()
    {
        return system;
    }
    public void setUpdateInfo(String updateInfo)
    {
        this.updateInfo = updateInfo;
    }

    public String getUpdateInfo()
    {
        return updateInfo;
    }
    public void setUpdateStatus(Long updateStatus)
    {
        this.updateStatus = updateStatus;
    }

    public Long getUpdateStatus()
    {
        return updateStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("verNo", getVerNo())
                .append("system", getSystem())
                .append("updateInfo", getUpdateInfo())
                .append("updateStatus", getUpdateStatus())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}