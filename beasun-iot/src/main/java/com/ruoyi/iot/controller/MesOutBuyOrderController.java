package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.MesOutBuyOrder;
import com.ruoyi.iot.service.IMesOutBuyOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * mes-出库订单Controller
 *
 * @author swingli
 * @date 2023-12-25
 */
@RestController
@RequestMapping("/iot/outbuy/order")
public class MesOutBuyOrderController extends BaseController
{
    @Autowired
    private IMesOutBuyOrderService mesOutBuyOrderService;

    /**
     * 查询mes-出库订单列表
     */
    @PreAuthorize("@ss.hasPermi('iot:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(MesOutBuyOrder mesOutBuyOrder)
    {
        startPage();
        List<MesOutBuyOrder> list = mesOutBuyOrderService.selectMesOutBuyOrderList(mesOutBuyOrder);
        return getDataTable(list);
    }

    /**
     * 导出mes-出库订单列表
     */
    @PreAuthorize("@ss.hasPermi('iot:order:export')")
    @Log(title = "mes-出库订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MesOutBuyOrder mesOutBuyOrder)
    {
        List<MesOutBuyOrder> list = mesOutBuyOrderService.selectMesOutBuyOrderList(mesOutBuyOrder);
        ExcelUtil<MesOutBuyOrder> util = new ExcelUtil<MesOutBuyOrder>(MesOutBuyOrder.class);
        util.exportExcel(response, list, "mes-出库订单数据");
    }

    /**
     * 获取mes-出库订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mesOutBuyOrderService.selectMesOutBuyOrderById(id));
    }

    /**
     * 新增mes-出库订单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:add')")
    @Log(title = "mes-出库订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MesOutBuyOrder mesOutBuyOrder)
    {
        return toAjax(mesOutBuyOrderService.insertMesOutBuyOrder(mesOutBuyOrder));
    }

    /**
     * 修改mes-出库订单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:edit')")
    @Log(title = "mes-出库订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MesOutBuyOrder mesOutBuyOrder)
    {
        return toAjax(mesOutBuyOrderService.updateMesOutBuyOrder(mesOutBuyOrder));
    }

    /**
     * 删除mes-出库订单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:remove')")
    @Log(title = "mes-出库订单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mesOutBuyOrderService.deleteMesOutBuyOrderByIds(ids));
    }
}