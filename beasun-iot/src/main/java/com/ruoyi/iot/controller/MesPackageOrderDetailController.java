package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.MesPackageOrderDetail;
import com.ruoyi.iot.service.IMesPackageOrderDetailService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * mes-包装单明细Controller
 *
 * @author swingli
 * @date 2023-12-25
 */
@RestController
@RequestMapping("/iot/packageorder/detail")
public class MesPackageOrderDetailController extends BaseController
{
    @Autowired
    private IMesPackageOrderDetailService mesPackageOrderDetailService;

    /**
     * 查询mes-包装单明细列表
     */
    @PreAuthorize("@ss.hasPermi('iot:detail:list')")
    @GetMapping("/list")
    public TableDataInfo list(MesPackageOrderDetail mesPackageOrderDetail)
    {
        startPage();
        List<MesPackageOrderDetail> list = mesPackageOrderDetailService.selectMesPackageOrderDetailList(mesPackageOrderDetail);
        return getDataTable(list);
    }

    /**
     * 导出mes-包装单明细列表
     */
    @PreAuthorize("@ss.hasPermi('iot:detail:export')")
    @Log(title = "mes-包装单明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MesPackageOrderDetail mesPackageOrderDetail)
    {
        List<MesPackageOrderDetail> list = mesPackageOrderDetailService.selectMesPackageOrderDetailList(mesPackageOrderDetail);
        ExcelUtil<MesPackageOrderDetail> util = new ExcelUtil<MesPackageOrderDetail>(MesPackageOrderDetail.class);
        util.exportExcel(response, list, "mes-包装单明细数据");
    }

    /**
     * 获取mes-包装单明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:detail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mesPackageOrderDetailService.selectMesPackageOrderDetailById(id));
    }

    /**
     * 新增mes-包装单明细
     */
    @PreAuthorize("@ss.hasPermi('iot:detail:add')")
    @Log(title = "mes-包装单明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MesPackageOrderDetail mesPackageOrderDetail)
    {
        return toAjax(mesPackageOrderDetailService.insertMesPackageOrderDetail(mesPackageOrderDetail));
    }

    /**
     * 修改mes-包装单明细
     */
    @PreAuthorize("@ss.hasPermi('iot:detail:edit')")
    @Log(title = "mes-包装单明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MesPackageOrderDetail mesPackageOrderDetail)
    {
        return toAjax(mesPackageOrderDetailService.updateMesPackageOrderDetail(mesPackageOrderDetail));
    }

    /**
     * 删除mes-包装单明细
     */
    @PreAuthorize("@ss.hasPermi('iot:detail:remove')")
    @Log(title = "mes-包装单明细", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mesPackageOrderDetailService.deleteMesPackageOrderDetailByIds(ids));
    }
}