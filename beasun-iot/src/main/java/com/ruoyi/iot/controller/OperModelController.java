package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.OperModel;
import com.ruoyi.iot.service.IOperModelService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 运维管理-设备模拟Controller
 *
 * @author swingli
 * @date 2023-12-22
 */
@RestController
@RequestMapping("/iot/oper/model")
public class OperModelController extends BaseController
{
    @Autowired
    private IOperModelService operModelService;

    /**
     * 查询运维管理-设备模拟列表
     */
    @PreAuthorize("@ss.hasPermi('iot:model:list')")
    @GetMapping("/list")
    public TableDataInfo list(OperModel operModel)
    {
        startPage();
        List<OperModel> list = operModelService.selectOperModelList(operModel);
        return getDataTable(list);
    }

    /**
     * 导出运维管理-设备模拟列表
     */
    @PreAuthorize("@ss.hasPermi('iot:model:export')")
    @Log(title = "运维管理-设备模拟", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OperModel operModel)
    {
        List<OperModel> list = operModelService.selectOperModelList(operModel);
        ExcelUtil<OperModel> util = new ExcelUtil<OperModel>(OperModel.class);
        util.exportExcel(response, list, "运维管理-设备模拟数据");
    }

    /**
     * 获取运维管理-设备模拟详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:model:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(operModelService.selectOperModelById(id));
    }

    /**
     * 新增运维管理-设备模拟
     */
    @PreAuthorize("@ss.hasPermi('iot:model:add')")
    @Log(title = "运维管理-设备模拟", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OperModel operModel)
    {
        return toAjax(operModelService.insertOperModel(operModel));
    }

    /**
     * 修改运维管理-设备模拟
     */
    @PreAuthorize("@ss.hasPermi('iot:model:edit')")
    @Log(title = "运维管理-设备模拟", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OperModel operModel)
    {
        return toAjax(operModelService.updateOperModel(operModel));
    }

    /**
     * 删除运维管理-设备模拟
     */
    @PreAuthorize("@ss.hasPermi('iot:model:remove')")
    @Log(title = "运维管理-设备模拟", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(operModelService.deleteOperModelByIds(ids));
    }
}