package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.MesOutOrder;
import com.ruoyi.iot.service.IMesOutOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * mes-出库单Controller
 *
 * @author swingli
 * @date 2023-12-23
 */
@RestController
@RequestMapping("/iot/warehouseout/order")
public class MesOutOrderController extends BaseController
{
    @Autowired
    private IMesOutOrderService mesOutOrderService;

    /**
     * 查询mes-出库单列表
     */
    @PreAuthorize("@ss.hasPermi('iot:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(MesOutOrder mesOutOrder)
    {
        startPage();
        List<MesOutOrder> list = mesOutOrderService.selectMesOutOrderList(mesOutOrder);
        return getDataTable(list);
    }

    /**
     * 导出mes-出库单列表
     */
    @PreAuthorize("@ss.hasPermi('iot:order:export')")
    @Log(title = "mes-出库单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MesOutOrder mesOutOrder)
    {
        List<MesOutOrder> list = mesOutOrderService.selectMesOutOrderList(mesOutOrder);
        ExcelUtil<MesOutOrder> util = new ExcelUtil<MesOutOrder>(MesOutOrder.class);
        util.exportExcel(response, list, "mes-出库单数据");
    }

    /**
     * 获取mes-出库单详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mesOutOrderService.selectMesOutOrderById(id));
    }

    /**
     * 新增mes-出库单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:add')")
    @Log(title = "mes-出库单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MesOutOrder mesOutOrder)
    {
        return toAjax(mesOutOrderService.insertMesOutOrder(mesOutOrder));
    }

    /**
     * 修改mes-出库单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:edit')")
    @Log(title = "mes-出库单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MesOutOrder mesOutOrder)
    {
        return toAjax(mesOutOrderService.updateMesOutOrder(mesOutOrder));
    }

    /**
     * 删除mes-出库单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:remove')")
    @Log(title = "mes-出库单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mesOutOrderService.deleteMesOutOrderByIds(ids));
    }
}