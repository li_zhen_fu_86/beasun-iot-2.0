package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.MesCompleteOrder;
import com.ruoyi.iot.service.IMesCompleteOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * mes-完工汇报单Controller
 *
 * @author swingli
 * @date 2023-12-23
 */
@RestController
@RequestMapping("/iot/complete/order")
public class MesCompleteOrderController extends BaseController
{
    @Autowired
    private IMesCompleteOrderService mesCompleteOrderService;

    /**
     * 查询mes-完工汇报单列表
     */
    @PreAuthorize("@ss.hasPermi('iot:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(MesCompleteOrder mesCompleteOrder)
    {
        startPage();
        List<MesCompleteOrder> list = mesCompleteOrderService.selectMesCompleteOrderList(mesCompleteOrder);
        return getDataTable(list);
    }

    /**
     * 导出mes-完工汇报单列表
     */
    @PreAuthorize("@ss.hasPermi('iot:order:export')")
    @Log(title = "mes-完工汇报单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MesCompleteOrder mesCompleteOrder)
    {
        List<MesCompleteOrder> list = mesCompleteOrderService.selectMesCompleteOrderList(mesCompleteOrder);
        ExcelUtil<MesCompleteOrder> util = new ExcelUtil<MesCompleteOrder>(MesCompleteOrder.class);
        util.exportExcel(response, list, "mes-完工汇报单数据");
    }

    /**
     * 获取mes-完工汇报单详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mesCompleteOrderService.selectMesCompleteOrderById(id));
    }

    /**
     * 新增mes-完工汇报单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:add')")
    @Log(title = "mes-完工汇报单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MesCompleteOrder mesCompleteOrder)
    {
        return toAjax(mesCompleteOrderService.insertMesCompleteOrder(mesCompleteOrder));
    }

    /**
     * 修改mes-完工汇报单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:edit')")
    @Log(title = "mes-完工汇报单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MesCompleteOrder mesCompleteOrder)
    {
        return toAjax(mesCompleteOrderService.updateMesCompleteOrder(mesCompleteOrder));
    }

    /**
     * 删除mes-完工汇报单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:remove')")
    @Log(title = "mes-完工汇报单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mesCompleteOrderService.deleteMesCompleteOrderByIds(ids));
    }
}