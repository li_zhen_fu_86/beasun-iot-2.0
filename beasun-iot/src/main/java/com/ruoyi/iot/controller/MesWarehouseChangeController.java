package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.MesWarehouseChange;
import com.ruoyi.iot.service.IMesWarehouseChangeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * mes-库存流水-制单日期Controller
 *
 * @author swingli
 * @date 2023-12-25
 */
@RestController
@RequestMapping("/iot/warehouse/change")
public class MesWarehouseChangeController extends BaseController
{
    @Autowired
    private IMesWarehouseChangeService mesWarehouseChangeService;

    /**
     * 查询mes-库存流水-制单日期列表
     */
    @PreAuthorize("@ss.hasPermi('iot:change:list')")
    @GetMapping("/list")
    public TableDataInfo list(MesWarehouseChange mesWarehouseChange)
    {
        startPage();
        List<MesWarehouseChange> list = mesWarehouseChangeService.selectMesWarehouseChangeList(mesWarehouseChange);
        return getDataTable(list);
    }

    /**
     * 导出mes-库存流水-制单日期列表
     */
    @PreAuthorize("@ss.hasPermi('iot:change:export')")
    @Log(title = "mes-库存流水-制单日期", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MesWarehouseChange mesWarehouseChange)
    {
        List<MesWarehouseChange> list = mesWarehouseChangeService.selectMesWarehouseChangeList(mesWarehouseChange);
        ExcelUtil<MesWarehouseChange> util = new ExcelUtil<MesWarehouseChange>(MesWarehouseChange.class);
        util.exportExcel(response, list, "mes-库存流水-制单日期数据");
    }

    /**
     * 获取mes-库存流水-制单日期详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:change:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mesWarehouseChangeService.selectMesWarehouseChangeById(id));
    }

    /**
     * 新增mes-库存流水-制单日期
     */
    @PreAuthorize("@ss.hasPermi('iot:change:add')")
    @Log(title = "mes-库存流水-制单日期", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MesWarehouseChange mesWarehouseChange)
    {
        return toAjax(mesWarehouseChangeService.insertMesWarehouseChange(mesWarehouseChange));
    }

    /**
     * 修改mes-库存流水-制单日期
     */
    @PreAuthorize("@ss.hasPermi('iot:change:edit')")
    @Log(title = "mes-库存流水-制单日期", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MesWarehouseChange mesWarehouseChange)
    {
        return toAjax(mesWarehouseChangeService.updateMesWarehouseChange(mesWarehouseChange));
    }

    /**
     * 删除mes-库存流水-制单日期
     */
    @PreAuthorize("@ss.hasPermi('iot:change:remove')")
    @Log(title = "mes-库存流水-制单日期", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mesWarehouseChangeService.deleteMesWarehouseChangeByIds(ids));
    }
}