package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.VerPhisical;
import com.ruoyi.iot.service.IVerPhisicalService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 版本管理-固件版管理Controller
 *
 * @author swingli
 * @date 2023-12-23
 */
@RestController
@RequestMapping("/iot/phisical")
public class VerPhisicalController extends BaseController
{
    @Autowired
    private IVerPhisicalService verPhisicalService;

    /**
     * 查询版本管理-固件版管理列表
     */
    @PreAuthorize("@ss.hasPermi('iot:phisical:list')")
    @GetMapping("/list")
    public TableDataInfo list(VerPhisical verPhisical)
    {
        startPage();
        List<VerPhisical> list = verPhisicalService.selectVerPhisicalList(verPhisical);
        return getDataTable(list);
    }

    /**
     * 导出版本管理-固件版管理列表
     */
    @PreAuthorize("@ss.hasPermi('iot:phisical:export')")
    @Log(title = "版本管理-固件版管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, VerPhisical verPhisical)
    {
        List<VerPhisical> list = verPhisicalService.selectVerPhisicalList(verPhisical);
        ExcelUtil<VerPhisical> util = new ExcelUtil<VerPhisical>(VerPhisical.class);
        util.exportExcel(response, list, "版本管理-固件版管理数据");
    }

    /**
     * 获取版本管理-固件版管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:phisical:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(verPhisicalService.selectVerPhisicalById(id));
    }

    /**
     * 新增版本管理-固件版管理
     */
    @PreAuthorize("@ss.hasPermi('iot:phisical:add')")
    @Log(title = "版本管理-固件版管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody VerPhisical verPhisical)
    {
        return toAjax(verPhisicalService.insertVerPhisical(verPhisical));
    }

    /**
     * 修改版本管理-固件版管理
     */
    @PreAuthorize("@ss.hasPermi('iot:phisical:edit')")
    @Log(title = "版本管理-固件版管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody VerPhisical verPhisical)
    {
        return toAjax(verPhisicalService.updateVerPhisical(verPhisical));
    }

    /**
     * 删除版本管理-固件版管理
     */
    @PreAuthorize("@ss.hasPermi('iot:phisical:remove')")
    @Log(title = "版本管理-固件版管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(verPhisicalService.deleteVerPhisicalByIds(ids));
    }
}