package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.VerDevice;
import com.ruoyi.iot.service.IVerDeviceService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 版本管理-硬件版本定义Controller
 *
 * @author swingli
 * @date 2023-12-23
 */
@RestController
@RequestMapping("/iot/version/device")
public class VerDeviceController extends BaseController
{
    @Autowired
    private IVerDeviceService verDeviceService;

    /**
     * 查询版本管理-硬件版本定义列表
     */
    @PreAuthorize("@ss.hasPermi('iot:device:list')")
    @GetMapping("/list")
    public TableDataInfo list(VerDevice verDevice)
    {
        startPage();
        List<VerDevice> list = verDeviceService.selectVerDeviceList(verDevice);
        return getDataTable(list);
    }

    /**
     * 导出版本管理-硬件版本定义列表
     */
    @PreAuthorize("@ss.hasPermi('iot:device:export')")
    @Log(title = "版本管理-硬件版本定义", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, VerDevice verDevice)
    {
        List<VerDevice> list = verDeviceService.selectVerDeviceList(verDevice);
        ExcelUtil<VerDevice> util = new ExcelUtil<VerDevice>(VerDevice.class);
        util.exportExcel(response, list, "版本管理-硬件版本定义数据");
    }

    /**
     * 获取版本管理-硬件版本定义详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:device:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(verDeviceService.selectVerDeviceById(id));
    }

    /**
     * 新增版本管理-硬件版本定义
     */
    @PreAuthorize("@ss.hasPermi('iot:device:add')")
    @Log(title = "版本管理-硬件版本定义", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody VerDevice verDevice)
    {
        return toAjax(verDeviceService.insertVerDevice(verDevice));
    }

    /**
     * 修改版本管理-硬件版本定义
     */
    @PreAuthorize("@ss.hasPermi('iot:device:edit')")
    @Log(title = "版本管理-硬件版本定义", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody VerDevice verDevice)
    {
        return toAjax(verDeviceService.updateVerDevice(verDevice));
    }

    /**
     * 删除版本管理-硬件版本定义
     */
    @PreAuthorize("@ss.hasPermi('iot:device:remove')")
    @Log(title = "版本管理-硬件版本定义", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(verDeviceService.deleteVerDeviceByIds(ids));
    }
}