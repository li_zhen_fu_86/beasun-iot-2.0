package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.VerOtaUpdate;
import com.ruoyi.iot.service.IVerOtaUpdateService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 版本管理-OTA升级广利Controller
 *
 * @author swingli
 * @date 2023-12-23
 */
@RestController
@RequestMapping("/iot/update")
public class VerOtaUpdateController extends BaseController
{
    @Autowired
    private IVerOtaUpdateService verOtaUpdateService;

    /**
     * 查询版本管理-OTA升级广利列表
     */
    @PreAuthorize("@ss.hasPermi('iot:update:list')")
    @GetMapping("/list")
    public TableDataInfo list(VerOtaUpdate verOtaUpdate)
    {
        startPage();
        List<VerOtaUpdate> list = verOtaUpdateService.selectVerOtaUpdateList(verOtaUpdate);
        return getDataTable(list);
    }

    /**
     * 导出版本管理-OTA升级广利列表
     */
    @PreAuthorize("@ss.hasPermi('iot:update:export')")
    @Log(title = "版本管理-OTA升级广利", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, VerOtaUpdate verOtaUpdate)
    {
        List<VerOtaUpdate> list = verOtaUpdateService.selectVerOtaUpdateList(verOtaUpdate);
        ExcelUtil<VerOtaUpdate> util = new ExcelUtil<VerOtaUpdate>(VerOtaUpdate.class);
        util.exportExcel(response, list, "版本管理-OTA升级广利数据");
    }

    /**
     * 获取版本管理-OTA升级广利详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:update:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(verOtaUpdateService.selectVerOtaUpdateById(id));
    }

    /**
     * 新增版本管理-OTA升级广利
     */
    @PreAuthorize("@ss.hasPermi('iot:update:add')")
    @Log(title = "版本管理-OTA升级广利", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody VerOtaUpdate verOtaUpdate)
    {
        return toAjax(verOtaUpdateService.insertVerOtaUpdate(verOtaUpdate));
    }

    /**
     * 修改版本管理-OTA升级广利
     */
    @PreAuthorize("@ss.hasPermi('iot:update:edit')")
    @Log(title = "版本管理-OTA升级广利", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody VerOtaUpdate verOtaUpdate)
    {
        return toAjax(verOtaUpdateService.updateVerOtaUpdate(verOtaUpdate));
    }

    /**
     * 删除版本管理-OTA升级广利
     */
    @PreAuthorize("@ss.hasPermi('iot:update:remove')")
    @Log(title = "版本管理-OTA升级广利", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(verOtaUpdateService.deleteVerOtaUpdateByIds(ids));
    }
}