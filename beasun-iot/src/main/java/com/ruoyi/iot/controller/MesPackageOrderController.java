package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.MesPackageOrder;
import com.ruoyi.iot.service.IMesPackageOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * mes-包装单Controller
 *
 * @author swingli
 * @date 2023-12-25
 */
@RestController
@RequestMapping("/iot/package/order")
public class MesPackageOrderController extends BaseController
{
    @Autowired
    private IMesPackageOrderService mesPackageOrderService;

    /**
     * 查询mes-包装单列表
     */
    @PreAuthorize("@ss.hasPermi('iot:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(MesPackageOrder mesPackageOrder)
    {
        startPage();
        List<MesPackageOrder> list = mesPackageOrderService.selectMesPackageOrderList(mesPackageOrder);
        return getDataTable(list);
    }

    /**
     * 导出mes-包装单列表
     */
    @PreAuthorize("@ss.hasPermi('iot:order:export')")
    @Log(title = "mes-包装单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MesPackageOrder mesPackageOrder)
    {
        List<MesPackageOrder> list = mesPackageOrderService.selectMesPackageOrderList(mesPackageOrder);
        ExcelUtil<MesPackageOrder> util = new ExcelUtil<MesPackageOrder>(MesPackageOrder.class);
        util.exportExcel(response, list, "mes-包装单数据");
    }

    /**
     * 获取mes-包装单详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mesPackageOrderService.selectMesPackageOrderById(id));
    }

    /**
     * 新增mes-包装单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:add')")
    @Log(title = "mes-包装单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MesPackageOrder mesPackageOrder)
    {
        return toAjax(mesPackageOrderService.insertMesPackageOrder(mesPackageOrder));
    }

    /**
     * 修改mes-包装单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:edit')")
    @Log(title = "mes-包装单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MesPackageOrder mesPackageOrder)
    {
        return toAjax(mesPackageOrderService.updateMesPackageOrder(mesPackageOrder));
    }

    /**
     * 删除mes-包装单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:remove')")
    @Log(title = "mes-包装单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mesPackageOrderService.deleteMesPackageOrderByIds(ids));
    }
}