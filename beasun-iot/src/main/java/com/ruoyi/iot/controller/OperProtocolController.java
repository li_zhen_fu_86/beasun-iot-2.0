package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.iot.domain.OperProtocol;
import com.ruoyi.iot.service.IOperProtocolService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 运维管理-协议管理Controller
 * 
 * @author swingli
 * @date 2023-12-22
 */
@RestController
@RequestMapping("/iot/protocol")
public class OperProtocolController extends BaseController
{
    @Autowired
    private IOperProtocolService operProtocolService;

    /**
     * 查询运维管理-协议管理列表
     */
    @PreAuthorize("@ss.hasPermi('iot:protocol:list')")
    @GetMapping("/list")
    public TableDataInfo list(OperProtocol operProtocol)
    {
        startPage();
        List<OperProtocol> list = operProtocolService.selectOperProtocolList(operProtocol);
        return getDataTable(list);
    }

    /**
     * 导出运维管理-协议管理列表
     */
    @PreAuthorize("@ss.hasPermi('iot:protocol:export')")
    @Log(title = "运维管理-协议管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OperProtocol operProtocol)
    {
        List<OperProtocol> list = operProtocolService.selectOperProtocolList(operProtocol);
        ExcelUtil<OperProtocol> util = new ExcelUtil<OperProtocol>(OperProtocol.class);
        util.exportExcel(response, list, "运维管理-协议管理数据");
    }

    /**
     * 获取运维管理-协议管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:protocol:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(operProtocolService.selectOperProtocolById(id));
    }

    /**
     * 新增运维管理-协议管理
     */
    @PreAuthorize("@ss.hasPermi('iot:protocol:add')")
    @Log(title = "运维管理-协议管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OperProtocol operProtocol)
    {
        return toAjax(operProtocolService.insertOperProtocol(operProtocol));
    }

    /**
     * 修改运维管理-协议管理
     */
    @PreAuthorize("@ss.hasPermi('iot:protocol:edit')")
    @Log(title = "运维管理-协议管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OperProtocol operProtocol)
    {
        return toAjax(operProtocolService.updateOperProtocol(operProtocol));
    }

    /**
     * 删除运维管理-协议管理
     */
    @PreAuthorize("@ss.hasPermi('iot:protocol:remove')")
    @Log(title = "运维管理-协议管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(operProtocolService.deleteOperProtocolByIds(ids));
    }
}