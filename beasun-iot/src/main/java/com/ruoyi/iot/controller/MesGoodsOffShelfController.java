package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.MesGoodsOffShelf;
import com.ruoyi.iot.service.IMesGoodsOffShelfService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * mes-出库下架作业Controller
 *
 * @author swingli
 * @date 2023-12-25
 */
@RestController
@RequestMapping("/iot/goodsoff/shelf")
public class MesGoodsOffShelfController extends BaseController
{
    @Autowired
    private IMesGoodsOffShelfService mesGoodsOffShelfService;

    /**
     * 查询mes-出库下架作业列表
     */
    @PreAuthorize("@ss.hasPermi('iot:shelf:list')")
    @GetMapping("/list")
    public TableDataInfo list(MesGoodsOffShelf mesGoodsOffShelf)
    {
        startPage();
        List<MesGoodsOffShelf> list = mesGoodsOffShelfService.selectMesGoodsOffShelfList(mesGoodsOffShelf);
        return getDataTable(list);
    }

    /**
     * 导出mes-出库下架作业列表
     */
    @PreAuthorize("@ss.hasPermi('iot:shelf:export')")
    @Log(title = "mes-出库下架作业", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MesGoodsOffShelf mesGoodsOffShelf)
    {
        List<MesGoodsOffShelf> list = mesGoodsOffShelfService.selectMesGoodsOffShelfList(mesGoodsOffShelf);
        ExcelUtil<MesGoodsOffShelf> util = new ExcelUtil<MesGoodsOffShelf>(MesGoodsOffShelf.class);
        util.exportExcel(response, list, "mes-出库下架作业数据");
    }

    /**
     * 获取mes-出库下架作业详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:shelf:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mesGoodsOffShelfService.selectMesGoodsOffShelfById(id));
    }

    /**
     * 新增mes-出库下架作业
     */
    @PreAuthorize("@ss.hasPermi('iot:shelf:add')")
    @Log(title = "mes-出库下架作业", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MesGoodsOffShelf mesGoodsOffShelf)
    {
        return toAjax(mesGoodsOffShelfService.insertMesGoodsOffShelf(mesGoodsOffShelf));
    }

    /**
     * 修改mes-出库下架作业
     */
    @PreAuthorize("@ss.hasPermi('iot:shelf:edit')")
    @Log(title = "mes-出库下架作业", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MesGoodsOffShelf mesGoodsOffShelf)
    {
        return toAjax(mesGoodsOffShelfService.updateMesGoodsOffShelf(mesGoodsOffShelf));
    }

    /**
     * 删除mes-出库下架作业
     */
    @PreAuthorize("@ss.hasPermi('iot:shelf:remove')")
    @Log(title = "mes-出库下架作业", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mesGoodsOffShelfService.deleteMesGoodsOffShelfByIds(ids));
    }
}