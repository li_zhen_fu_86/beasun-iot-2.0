package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.MessageCenter;
import com.ruoyi.iot.service.IMessageCenterService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * iot-消息中心Controller
 *
 * @author swingli
 * @date 2023-12-23
 */
@RestController
@RequestMapping("/iot/center")
public class MessageCenterController extends BaseController
{
    @Autowired
    private IMessageCenterService messageCenterService;

    /**
     * 查询iot-消息中心列表
     */
    @PreAuthorize("@ss.hasPermi('iot:center:list')")
    @GetMapping("/list")
    public TableDataInfo list(MessageCenter messageCenter)
    {
        startPage();
        List<MessageCenter> list = messageCenterService.selectMessageCenterList(messageCenter);
        return getDataTable(list);
    }

    /**
     * 导出iot-消息中心列表
     */
    @PreAuthorize("@ss.hasPermi('iot:center:export')")
    @Log(title = "iot-消息中心", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MessageCenter messageCenter)
    {
        List<MessageCenter> list = messageCenterService.selectMessageCenterList(messageCenter);
        ExcelUtil<MessageCenter> util = new ExcelUtil<MessageCenter>(MessageCenter.class);
        util.exportExcel(response, list, "iot-消息中心数据");
    }

    /**
     * 获取iot-消息中心详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:center:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(messageCenterService.selectMessageCenterById(id));
    }

    /**
     * 新增iot-消息中心
     */
    @PreAuthorize("@ss.hasPermi('iot:center:add')")
    @Log(title = "iot-消息中心", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MessageCenter messageCenter)
    {
        return toAjax(messageCenterService.insertMessageCenter(messageCenter));
    }

    /**
     * 修改iot-消息中心
     */
    @PreAuthorize("@ss.hasPermi('iot:center:edit')")
    @Log(title = "iot-消息中心", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MessageCenter messageCenter)
    {
        return toAjax(messageCenterService.updateMessageCenter(messageCenter));
    }

    /**
     * 删除iot-消息中心
     */
    @PreAuthorize("@ss.hasPermi('iot:center:remove')")
    @Log(title = "iot-消息中心", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(messageCenterService.deleteMessageCenterByIds(ids));
    }
}