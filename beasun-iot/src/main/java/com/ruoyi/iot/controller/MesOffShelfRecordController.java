package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.MesOffShelfRecord;
import com.ruoyi.iot.service.IMesOffShelfRecordService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * mes-下架记录单Controller
 *
 * @author swingli
 * @date 2023-12-25
 */
@RestController
@RequestMapping("/iot/offshelf/record")
public class MesOffShelfRecordController extends BaseController
{
    @Autowired
    private IMesOffShelfRecordService mesOffShelfRecordService;

    /**
     * 查询mes-下架记录单列表
     */
    @PreAuthorize("@ss.hasPermi('iot:record:list')")
    @GetMapping("/list")
    public TableDataInfo list(MesOffShelfRecord mesOffShelfRecord)
    {
        startPage();
        List<MesOffShelfRecord> list = mesOffShelfRecordService.selectMesOffShelfRecordList(mesOffShelfRecord);
        return getDataTable(list);
    }

    /**
     * 导出mes-下架记录单列表
     */
    @PreAuthorize("@ss.hasPermi('iot:record:export')")
    @Log(title = "mes-下架记录单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MesOffShelfRecord mesOffShelfRecord)
    {
        List<MesOffShelfRecord> list = mesOffShelfRecordService.selectMesOffShelfRecordList(mesOffShelfRecord);
        ExcelUtil<MesOffShelfRecord> util = new ExcelUtil<MesOffShelfRecord>(MesOffShelfRecord.class);
        util.exportExcel(response, list, "mes-下架记录单数据");
    }

    /**
     * 获取mes-下架记录单详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:record:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mesOffShelfRecordService.selectMesOffShelfRecordById(id));
    }

    /**
     * 新增mes-下架记录单
     */
    @PreAuthorize("@ss.hasPermi('iot:record:add')")
    @Log(title = "mes-下架记录单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MesOffShelfRecord mesOffShelfRecord)
    {
        return toAjax(mesOffShelfRecordService.insertMesOffShelfRecord(mesOffShelfRecord));
    }

    /**
     * 修改mes-下架记录单
     */
    @PreAuthorize("@ss.hasPermi('iot:record:edit')")
    @Log(title = "mes-下架记录单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MesOffShelfRecord mesOffShelfRecord)
    {
        return toAjax(mesOffShelfRecordService.updateMesOffShelfRecord(mesOffShelfRecord));
    }

    /**
     * 删除mes-下架记录单
     */
    @PreAuthorize("@ss.hasPermi('iot:record:remove')")
    @Log(title = "mes-下架记录单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mesOffShelfRecordService.deleteMesOffShelfRecordByIds(ids));
    }
}