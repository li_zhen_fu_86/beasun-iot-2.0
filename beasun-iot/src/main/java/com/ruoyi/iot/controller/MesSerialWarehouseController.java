package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.MesSerialWarehouse;
import com.ruoyi.iot.service.IMesSerialWarehouseService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * mes-序列号库存Controller
 *
 * @author swingli
 * @date 2023-12-25
 */
@RestController
@RequestMapping("/iot/serial/warehouse")
public class MesSerialWarehouseController extends BaseController
{
    @Autowired
    private IMesSerialWarehouseService mesSerialWarehouseService;

    /**
     * 查询mes-序列号库存列表
     */
    @PreAuthorize("@ss.hasPermi('iot:warehouse:list')")
    @GetMapping("/list")
    public TableDataInfo list(MesSerialWarehouse mesSerialWarehouse)
    {
        startPage();
        List<MesSerialWarehouse> list = mesSerialWarehouseService.selectMesSerialWarehouseList(mesSerialWarehouse);
        return getDataTable(list);
    }

    /**
     * 导出mes-序列号库存列表
     */
    @PreAuthorize("@ss.hasPermi('iot:warehouse:export')")
    @Log(title = "mes-序列号库存", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MesSerialWarehouse mesSerialWarehouse)
    {
        List<MesSerialWarehouse> list = mesSerialWarehouseService.selectMesSerialWarehouseList(mesSerialWarehouse);
        ExcelUtil<MesSerialWarehouse> util = new ExcelUtil<MesSerialWarehouse>(MesSerialWarehouse.class);
        util.exportExcel(response, list, "mes-序列号库存数据");
    }

    /**
     * 获取mes-序列号库存详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:warehouse:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mesSerialWarehouseService.selectMesSerialWarehouseById(id));
    }

    /**
     * 新增mes-序列号库存
     */
    @PreAuthorize("@ss.hasPermi('iot:warehouse:add')")
    @Log(title = "mes-序列号库存", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MesSerialWarehouse mesSerialWarehouse)
    {
        return toAjax(mesSerialWarehouseService.insertMesSerialWarehouse(mesSerialWarehouse));
    }

    /**
     * 修改mes-序列号库存
     */
    @PreAuthorize("@ss.hasPermi('iot:warehouse:edit')")
    @Log(title = "mes-序列号库存", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MesSerialWarehouse mesSerialWarehouse)
    {
        return toAjax(mesSerialWarehouseService.updateMesSerialWarehouse(mesSerialWarehouse));
    }

    /**
     * 删除mes-序列号库存
     */
    @PreAuthorize("@ss.hasPermi('iot:warehouse:remove')")
    @Log(title = "mes-序列号库存", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mesSerialWarehouseService.deleteMesSerialWarehouseByIds(ids));
    }
}