package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.MesProduceOrder;
import com.ruoyi.iot.service.IMesProduceOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * mes-生产任务单Controller
 *
 * @author swingli
 * @date 2023-12-23
 */
@RestController
@RequestMapping("/iot/order")
public class MesProduceOrderController extends BaseController
{
    @Autowired
    private IMesProduceOrderService mesProduceOrderService;

    /**
     * 查询mes-生产任务单列表
     */
    @PreAuthorize("@ss.hasPermi('iot:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(MesProduceOrder mesProduceOrder)
    {
        startPage();
        List<MesProduceOrder> list = mesProduceOrderService.selectMesProduceOrderList(mesProduceOrder);
        return getDataTable(list);
    }

    /**
     * 导出mes-生产任务单列表
     */
    @PreAuthorize("@ss.hasPermi('iot:order:export')")
    @Log(title = "mes-生产任务单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MesProduceOrder mesProduceOrder)
    {
        List<MesProduceOrder> list = mesProduceOrderService.selectMesProduceOrderList(mesProduceOrder);
        ExcelUtil<MesProduceOrder> util = new ExcelUtil<MesProduceOrder>(MesProduceOrder.class);
        util.exportExcel(response, list, "mes-生产任务单数据");
    }

    /**
     * 获取mes-生产任务单详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mesProduceOrderService.selectMesProduceOrderById(id));
    }

    /**
     * 新增mes-生产任务单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:add')")
    @Log(title = "mes-生产任务单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MesProduceOrder mesProduceOrder)
    {
        return toAjax(mesProduceOrderService.insertMesProduceOrder(mesProduceOrder));
    }

    /**
     * 修改mes-生产任务单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:edit')")
    @Log(title = "mes-生产任务单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MesProduceOrder mesProduceOrder)
    {
        return toAjax(mesProduceOrderService.updateMesProduceOrder(mesProduceOrder));
    }

    /**
     * 删除mes-生产任务单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:remove')")
    @Log(title = "mes-生产任务单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mesProduceOrderService.deleteMesProduceOrderByIds(ids));
    }
}