package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.MesPackageInWarehouse;
import com.ruoyi.iot.service.IMesPackageInWarehouseService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * mes-包装入库Controller
 *
 * @author swingli
 * @date 2023-12-25
 */
@RestController
@RequestMapping("/iot/packagein/warehouse")
public class MesPackageInWarehouseController extends BaseController
{
    @Autowired
    private IMesPackageInWarehouseService mesPackageInWarehouseService;

    /**
     * 查询mes-包装入库列表
     */
    @PreAuthorize("@ss.hasPermi('iot:warehouse:list')")
    @GetMapping("/list")
    public TableDataInfo list(MesPackageInWarehouse mesPackageInWarehouse)
    {
        startPage();
        List<MesPackageInWarehouse> list = mesPackageInWarehouseService.selectMesPackageInWarehouseList(mesPackageInWarehouse);
        return getDataTable(list);
    }

    /**
     * 导出mes-包装入库列表
     */
    @PreAuthorize("@ss.hasPermi('iot:warehouse:export')")
    @Log(title = "mes-包装入库", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MesPackageInWarehouse mesPackageInWarehouse)
    {
        List<MesPackageInWarehouse> list = mesPackageInWarehouseService.selectMesPackageInWarehouseList(mesPackageInWarehouse);
        ExcelUtil<MesPackageInWarehouse> util = new ExcelUtil<MesPackageInWarehouse>(MesPackageInWarehouse.class);
        util.exportExcel(response, list, "mes-包装入库数据");
    }

    /**
     * 获取mes-包装入库详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:warehouse:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mesPackageInWarehouseService.selectMesPackageInWarehouseById(id));
    }

    /**
     * 新增mes-包装入库
     */
    @PreAuthorize("@ss.hasPermi('iot:warehouse:add')")
    @Log(title = "mes-包装入库", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MesPackageInWarehouse mesPackageInWarehouse)
    {
        return toAjax(mesPackageInWarehouseService.insertMesPackageInWarehouse(mesPackageInWarehouse));
    }

    /**
     * 修改mes-包装入库
     */
    @PreAuthorize("@ss.hasPermi('iot:warehouse:edit')")
    @Log(title = "mes-包装入库", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MesPackageInWarehouse mesPackageInWarehouse)
    {
        return toAjax(mesPackageInWarehouseService.updateMesPackageInWarehouse(mesPackageInWarehouse));
    }

    /**
     * 删除mes-包装入库
     */
    @PreAuthorize("@ss.hasPermi('iot:warehouse:remove')")
    @Log(title = "mes-包装入库", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mesPackageInWarehouseService.deleteMesPackageInWarehouseByIds(ids));
    }
}