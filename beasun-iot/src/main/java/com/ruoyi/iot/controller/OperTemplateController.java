package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.OperTemplate;
import com.ruoyi.iot.service.IOperTemplateService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 运维管理-采集模板Controller
 *
 * @author swingli
 * @date 2023-12-22
 */
@RestController
@RequestMapping("/iot/oper/template")
public class OperTemplateController extends BaseController
{
    @Autowired
    private IOperTemplateService operTemplateService;

    /**
     * 查询运维管理-采集模板列表
     */
    @PreAuthorize("@ss.hasPermi('iot:template:list')")
    @GetMapping("/list")
    public TableDataInfo list(OperTemplate operTemplate)
    {
        startPage();
        List<OperTemplate> list = operTemplateService.selectOperTemplateList(operTemplate);
        return getDataTable(list);
    }

    /**
     * 导出运维管理-采集模板列表
     */
    @PreAuthorize("@ss.hasPermi('iot:template:export')")
    @Log(title = "运维管理-采集模板", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OperTemplate operTemplate)
    {
        List<OperTemplate> list = operTemplateService.selectOperTemplateList(operTemplate);
        ExcelUtil<OperTemplate> util = new ExcelUtil<OperTemplate>(OperTemplate.class);
        util.exportExcel(response, list, "运维管理-采集模板数据");
    }

    /**
     * 获取运维管理-采集模板详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:template:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(operTemplateService.selectOperTemplateById(id));
    }

    /**
     * 新增运维管理-采集模板
     */
    @PreAuthorize("@ss.hasPermi('iot:template:add')")
    @Log(title = "运维管理-采集模板", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OperTemplate operTemplate)
    {
        return toAjax(operTemplateService.insertOperTemplate(operTemplate));
    }

    /**
     * 修改运维管理-采集模板
     */
    @PreAuthorize("@ss.hasPermi('iot:template:edit')")
    @Log(title = "运维管理-采集模板", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OperTemplate operTemplate)
    {
        return toAjax(operTemplateService.updateOperTemplate(operTemplate));
    }

    /**
     * 删除运维管理-采集模板
     */
    @PreAuthorize("@ss.hasPermi('iot:template:remove')")
    @Log(title = "运维管理-采集模板", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(operTemplateService.deleteOperTemplateByIds(ids));
    }
}