package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.MesPreviewGoodsOrder;
import com.ruoyi.iot.service.IMesPreviewGoodsOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * mes-预到货单Controller
 *
 * @author swingli
 * @date 2023-12-23
 */
@RestController
@RequestMapping("/iot/preview/order")
public class MesPreviewGoodsOrderController extends BaseController
{
    @Autowired
    private IMesPreviewGoodsOrderService mesPreviewGoodsOrderService;

    /**
     * 查询mes-预到货单列表
     */
    @PreAuthorize("@ss.hasPermi('iot:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(MesPreviewGoodsOrder mesPreviewGoodsOrder)
    {
        startPage();
        List<MesPreviewGoodsOrder> list = mesPreviewGoodsOrderService.selectMesPreviewGoodsOrderList(mesPreviewGoodsOrder);
        return getDataTable(list);
    }

    /**
     * 导出mes-预到货单列表
     */
    @PreAuthorize("@ss.hasPermi('iot:order:export')")
    @Log(title = "mes-预到货单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MesPreviewGoodsOrder mesPreviewGoodsOrder)
    {
        List<MesPreviewGoodsOrder> list = mesPreviewGoodsOrderService.selectMesPreviewGoodsOrderList(mesPreviewGoodsOrder);
        ExcelUtil<MesPreviewGoodsOrder> util = new ExcelUtil<MesPreviewGoodsOrder>(MesPreviewGoodsOrder.class);
        util.exportExcel(response, list, "mes-预到货单数据");
    }

    /**
     * 获取mes-预到货单详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mesPreviewGoodsOrderService.selectMesPreviewGoodsOrderById(id));
    }

    /**
     * 新增mes-预到货单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:add')")
    @Log(title = "mes-预到货单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MesPreviewGoodsOrder mesPreviewGoodsOrder)
    {
        return toAjax(mesPreviewGoodsOrderService.insertMesPreviewGoodsOrder(mesPreviewGoodsOrder));
    }

    /**
     * 修改mes-预到货单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:edit')")
    @Log(title = "mes-预到货单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MesPreviewGoodsOrder mesPreviewGoodsOrder)
    {
        return toAjax(mesPreviewGoodsOrderService.updateMesPreviewGoodsOrder(mesPreviewGoodsOrder));
    }

    /**
     * 删除mes-预到货单
     */
    @PreAuthorize("@ss.hasPermi('iot:order:remove')")
    @Log(title = "mes-预到货单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mesPreviewGoodsOrderService.deleteMesPreviewGoodsOrderByIds(ids));
    }
}