package com.ruoyi.iot.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.config.ResultModel;
import com.ruoyi.iot.config.ResultStatus;
import com.ruoyi.iot.domain.DeviceLog;
import com.ruoyi.iot.influxdb.bean.DeviceData;
import com.ruoyi.iot.influxdb.query.DeviceLogQuery;
import com.ruoyi.iot.influxdb.service.BlueToothDataCmdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.support.DefaultedPageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * 设备日志Controller
 *
 * @author kerwincui
 * @date 2022-01-13
 */
@RestController
@RequestMapping("/iot/influx/data")
public class InfluxDeviceLogController {
    @Autowired
    private BlueToothDataCmdService blueToothDataCmdService;


    /**
     * 查询设备指标
     * @param deviceId
     * @return
     */
    @RequestMapping(value = "/{deviceId}",method= RequestMethod.GET, produces="application/json" )
    public @ResponseBody
    ResponseEntity<ResultModel> findByDeviceId(@PathVariable String deviceId ) {

        return  ResponseEntity.ok(blueToothDataCmdService.getDeviceDetailFromCache(deviceId));
    }

    /**
     * 查询设备日志列表
     */
//    @PreAuthorize("@ss.hasPermi('iot:data:list')")
    @GetMapping("/list")
    public ResultModel list(DeviceLogQuery query, DefaultedPageable pageable)
    {
        return blueToothDataCmdService.findHistoryData(query,pageable.getPageable());
    }

    /**
     * 新增设备日志
     */
//    @PreAuthorize("@ss.hasPermi('iot:data:add')")
    @Log(title = "设备日志", businessType = BusinessType.INSERT)
    @PostMapping
    public ResultModel add(@RequestBody DeviceData deviceData)
    {
//        System.out.println("新增设备数据"+deviceLog.getSerialNumber());
        try {
            return blueToothDataCmdService.saveData(deviceData);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return  ResultModel.error(ResultStatus.IOT_DATA_ERROR);
    }


}
