package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.MesHousewareAnalysis;
import com.ruoyi.iot.service.IMesHousewareAnalysisService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * mes-库存分析Controller
 *
 * @author swingli
 * @date 2023-12-23
 */
@RestController
@RequestMapping("/iot/analysis")
public class MesHousewareAnalysisController extends BaseController
{
    @Autowired
    private IMesHousewareAnalysisService mesHousewareAnalysisService;

    /**
     * 查询mes-库存分析列表
     */
    @PreAuthorize("@ss.hasPermi('iot:analysis:list')")
    @GetMapping("/list")
    public TableDataInfo list(MesHousewareAnalysis mesHousewareAnalysis)
    {
        startPage();
        List<MesHousewareAnalysis> list = mesHousewareAnalysisService.selectMesHousewareAnalysisList(mesHousewareAnalysis);
        return getDataTable(list);
    }

    /**
     * 导出mes-库存分析列表
     */
    @PreAuthorize("@ss.hasPermi('iot:analysis:export')")
    @Log(title = "mes-库存分析", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MesHousewareAnalysis mesHousewareAnalysis)
    {
        List<MesHousewareAnalysis> list = mesHousewareAnalysisService.selectMesHousewareAnalysisList(mesHousewareAnalysis);
        ExcelUtil<MesHousewareAnalysis> util = new ExcelUtil<MesHousewareAnalysis>(MesHousewareAnalysis.class);
        util.exportExcel(response, list, "mes-库存分析数据");
    }

    /**
     * 获取mes-库存分析详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:analysis:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mesHousewareAnalysisService.selectMesHousewareAnalysisById(id));
    }

    /**
     * 新增mes-库存分析
     */
    @PreAuthorize("@ss.hasPermi('iot:analysis:add')")
    @Log(title = "mes-库存分析", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MesHousewareAnalysis mesHousewareAnalysis)
    {
        return toAjax(mesHousewareAnalysisService.insertMesHousewareAnalysis(mesHousewareAnalysis));
    }

    /**
     * 修改mes-库存分析
     */
    @PreAuthorize("@ss.hasPermi('iot:analysis:edit')")
    @Log(title = "mes-库存分析", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MesHousewareAnalysis mesHousewareAnalysis)
    {
        return toAjax(mesHousewareAnalysisService.updateMesHousewareAnalysis(mesHousewareAnalysis));
    }

    /**
     * 删除mes-库存分析
     */
    @PreAuthorize("@ss.hasPermi('iot:analysis:remove')")
    @Log(title = "mes-库存分析", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mesHousewareAnalysisService.deleteMesHousewareAnalysisByIds(ids));
    }
}