package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.VerSoftWare;
import com.ruoyi.iot.service.IVerSoftWareService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 版本管理-软件版本管理Controller
 *
 * @author swingli
 * @date 2023-12-23
 */
@RestController
@RequestMapping("/iot/ware")
public class VerSoftWareController extends BaseController
{
    @Autowired
    private IVerSoftWareService verSoftWareService;

    /**
     * 查询版本管理-软件版本管理列表
     */
    @PreAuthorize("@ss.hasPermi('iot:ware:list')")
    @GetMapping("/list")
    public TableDataInfo list(VerSoftWare verSoftWare)
    {
        startPage();
        List<VerSoftWare> list = verSoftWareService.selectVerSoftWareList(verSoftWare);
        return getDataTable(list);
    }

    /**
     * 导出版本管理-软件版本管理列表
     */
    @PreAuthorize("@ss.hasPermi('iot:ware:export')")
    @Log(title = "版本管理-软件版本管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, VerSoftWare verSoftWare)
    {
        List<VerSoftWare> list = verSoftWareService.selectVerSoftWareList(verSoftWare);
        ExcelUtil<VerSoftWare> util = new ExcelUtil<VerSoftWare>(VerSoftWare.class);
        util.exportExcel(response, list, "版本管理-软件版本管理数据");
    }

    /**
     * 获取版本管理-软件版本管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:ware:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(verSoftWareService.selectVerSoftWareById(id));
    }

    /**
     * 新增版本管理-软件版本管理
     */
    @PreAuthorize("@ss.hasPermi('iot:ware:add')")
    @Log(title = "版本管理-软件版本管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody VerSoftWare verSoftWare)
    {
        return toAjax(verSoftWareService.insertVerSoftWare(verSoftWare));
    }

    /**
     * 修改版本管理-软件版本管理
     */
    @PreAuthorize("@ss.hasPermi('iot:ware:edit')")
    @Log(title = "版本管理-软件版本管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody VerSoftWare verSoftWare)
    {
        return toAjax(verSoftWareService.updateVerSoftWare(verSoftWare));
    }

    /**
     * 删除版本管理-软件版本管理
     */
    @PreAuthorize("@ss.hasPermi('iot:ware:remove')")
    @Log(title = "版本管理-软件版本管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(verSoftWareService.deleteVerSoftWareByIds(ids));
    }
}