package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.MesWarehouseInfo;
import com.ruoyi.iot.service.IMesWarehouseInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * mes-库存信息Controller
 *
 * @author swingli
 * @date 2023-12-25
 */
@RestController
@RequestMapping("/iot/warehouse/info")
public class MesWarehouseInfoController extends BaseController
{
    @Autowired
    private IMesWarehouseInfoService mesWarehouseInfoService;

    /**
     * 查询mes-库存信息列表
     */
    @PreAuthorize("@ss.hasPermi('iot:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(MesWarehouseInfo mesWarehouseInfo)
    {
        startPage();
        List<MesWarehouseInfo> list = mesWarehouseInfoService.selectMesWarehouseInfoList(mesWarehouseInfo);
        return getDataTable(list);
    }

    /**
     * 导出mes-库存信息列表
     */
    @PreAuthorize("@ss.hasPermi('iot:info:export')")
    @Log(title = "mes-库存信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MesWarehouseInfo mesWarehouseInfo)
    {
        List<MesWarehouseInfo> list = mesWarehouseInfoService.selectMesWarehouseInfoList(mesWarehouseInfo);
        ExcelUtil<MesWarehouseInfo> util = new ExcelUtil<MesWarehouseInfo>(MesWarehouseInfo.class);
        util.exportExcel(response, list, "mes-库存信息数据");
    }

    /**
     * 获取mes-库存信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mesWarehouseInfoService.selectMesWarehouseInfoById(id));
    }

    /**
     * 新增mes-库存信息
     */
    @PreAuthorize("@ss.hasPermi('iot:info:add')")
    @Log(title = "mes-库存信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MesWarehouseInfo mesWarehouseInfo)
    {
        return toAjax(mesWarehouseInfoService.insertMesWarehouseInfo(mesWarehouseInfo));
    }

    /**
     * 修改mes-库存信息
     */
    @PreAuthorize("@ss.hasPermi('iot:info:edit')")
    @Log(title = "mes-库存信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MesWarehouseInfo mesWarehouseInfo)
    {
        return toAjax(mesWarehouseInfoService.updateMesWarehouseInfo(mesWarehouseInfo));
    }

    /**
     * 删除mes-库存信息
     */
    @PreAuthorize("@ss.hasPermi('iot:info:remove')")
    @Log(title = "mes-库存信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mesWarehouseInfoService.deleteMesWarehouseInfoByIds(ids));
    }
}