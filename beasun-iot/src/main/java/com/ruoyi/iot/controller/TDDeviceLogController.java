package com.ruoyi.iot.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.DeviceLog;
import com.ruoyi.iot.tdengine.service.ILogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 设备日志Controller
 *
 * @author kerwincui
 * @date 2022-01-13
 */
@RestController
@RequestMapping("/iot/tdengine/data")
public class TDDeviceLogController extends BaseController {
    @Autowired
    private ILogService iLogService;

    /**
     * 查询设备日志列表
     */
//    @PreAuthorize("@ss.hasPermi('iot:data:list')")
    @GetMapping("/list")
    public TableDataInfo list(DeviceLog deviceLog)
    {
        startPage();
        List<DeviceLog> list = iLogService.selectDeviceLogList(deviceLog);
        return getDataTable(list);
    }


    /**
     * 新增设备日志
     */
//    @PreAuthorize("@ss.hasPermi('iot:data:add')")
    @Log(title = "设备日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DeviceLog deviceLog)
    {
        System.out.println("新增设备数据"+deviceLog.getSerialNumber());
        return toAjax(iLogService.saveDeviceLog(deviceLog));
    }


}
