package com.ruoyi.iot.config;


/**
 * 常量
 * Created by Andy.Yang on 2020-03-01.
 */
public class Constants {

    public static final String DATE_FORMATE = "yyyy-MM-dd HH:mm:ss";

    /**
     * Influxdb database
     */
    public static final String INFLUXDB_DATABASE_PLAT = "device_data";

    /**
     * token有效期（秒）
     */
    public static final int TOKEN_EXPIRES = 3600 * 24;

    /**
     * 存放Authorization的header字段
     */
    public static final String AUTHORIZATION = "authorization";

    /**
     * 当前请求的Token对象
     */
    public static final String CURRENT_TOKEN = "CURRENT_TOKEN";


}
