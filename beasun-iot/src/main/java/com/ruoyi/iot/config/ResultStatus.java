package com.ruoyi.iot.config;


/**
 * Created by andy on 2020-03-01.
 * 自定义请求状态码
 */
public enum ResultStatus {
    SUCCESS(200, "成功"),
    WEB_SERVER_ERROR(1000, "Web服务器错误"),
    USERNAME_OR_PASSWORD_ERROR(1001, "用户名或密码错误"),
    TOKEN_NOT_FOUND(1002, "Token不存在"),
    DEVICE_NOT_FOUND(2001, "设备不存在"),
    DEVICE_ALREADY_EXISTS(2002, "设备已经存在"),
    DEVICE_UNACTIVATED(2003, "设备未激活"),
    DEVICE_OFFLINE(2004, "设备离线"),
    DEVICE_SEND_ERROR(2005, "发送命令失败"),
    ILLEGAL_COMMAND(2006, "非法命令，请检查参数"),
    NOT_FOUND(2007, "不存在"),
    ALREADY_EXISTS(2008, "已经存在"),
    DATA_NOT_REPORT(2009, "数据未上报"),
    DATA_NOT_FOUND(2010, "没有数据"),
    DEVICE_TYPE_NOT_FOUND(2011, "设备类型不存在"),
    CLOUD_API_ERROR(2012, "云端API调用错误"),
    FILE_UPLOAD_ERROR(2013, "上传文件错误"),
    FACE_NOT_FOUND(2013, "没有检测到人脸"),
    ID_NOT_FIT(2013,"身份证不合规"),
    MAC_NOT_FOUND(2014, "MAC地址不存在"),
    IOT_DATA_ERROR(3001,"物联网设备数据保存失败"),
    REQEST_ACCESS_LIMIT_ERROR(2015, "请勿频繁请求接口");

    /**
     * 返回码
     */
    private int code;

    /**
     * 返回结果描述
     */
    private String message;

    ResultStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
