package com.ruoyi.iot.influxdb.repository.metric;

/**
 * Repository interface for aggregated metrics data.
 *
 * @param <T> type of metrics
 * @author Andy.Yang on 2020-03-01.
 */
public interface MetricsRepository<T> {

    /**
     * 从缓存中查询最新的指标数据
     * @param key
     * @return 指标数据
     */
    public T findByDeviceIdFromCache(String key);
    /**
     * 保存记录
     * @param deviceId
     * @param metrics
     * @param persistentData
     */
    void save(String deviceId, T metrics, boolean persistentData);



}
