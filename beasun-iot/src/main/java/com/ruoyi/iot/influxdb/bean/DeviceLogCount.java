package com.ruoyi.iot.influxdb.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;
import org.influxdb.annotation.TimeColumn;

import java.time.Instant;

@Measurement(name = "NewFence")
public class DeviceLogCount {
    // 数据时间
    @TimeColumn
    @Column(name = "time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Instant time;

    @Column(name = "count")
    private int count;

    public Instant getTime() {
        return time;
    }

    public void setTime(Instant time) {
        this.time = time;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
