package com.ruoyi.iot.influxdb.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;
import org.influxdb.annotation.TimeColumn;

import java.time.Instant;

/**
 * 设备日志对象 iot_device_log
 *
 * @author kerwincui
 * @date 2022-01-13
 */
@Measurement(name="device_data")
public class DeviceData extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    // 数据时间
    @TimeColumn
    @Column(name = "time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Instant time;



    /** 类型（1=属性上报，2=事件上报，3=调用功能，4=设备升级，5=设备上线，6=设备离线） */
    @Excel(name = "数据类型", readConverterExp = "1==属性上报，2=事件上报，3=调用功能，4=设备升级，5=设备上线，6=设备离线")
    @Column(name = "dataType")
    private Integer dataType;

    /** 日志值 */
    @Excel(name = "数据值")
    @Column(name = "dataValue")
    private String logValue;

    /** 设备ID */
    @Excel(name = "设备ID")
    @Column(name = "deviceId")
    private Long deviceId;

    /** 设备编号 */
    @Excel(name = "设备编号")
    @Column(name = "serialNumber")
    private String serialNumber;

    /** 标识符 */
    @Excel(name = "标识符")
    @Column(name = "identity")
    private String identity;

    /** 是否监测数据（1=是，0=否） */
    @Excel(name = "是否监测数据", readConverterExp = "1=是，0=否")
    @Column(name = "isMonitor")
    private Integer isMonitor;

    /** 模式 */
    @Excel(name = "模式", readConverterExp = "1=影子模式，2=在线模式,3=其他")
    @Column(name = "mode")
    private Integer mode;

    /** 设备数据 */
    @Excel(name = "监测项")
    @Column(name = "data1")
    private String data1;

    /** 设备数据 */
    @Excel(name = "监测项")
    @Column(name = "data2")
    private String data2;

    /** 设备数据 */
    @Excel(name = "监测项")
    @Column(name = "data3")
    private String data3;

    /** 设备数据 */
    @Excel(name = "监测项")
    @Column(name = "data4")
    private String data4;

    /** 设备数据 */
    @Excel(name = "监测项")
    @Column(name = "data5")
    private String data5;



    /** 查询用的开始时间 */

    private String beginTime;

    /** 查询用的结束时间 */
    private String endTime;

    /** 查询的总数 */
    private int total;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getMode() {
        return mode;
    }

    public void setMode(Integer mode) {
        this.mode = mode;
    }



    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }




    public void setLogValue(String logValue) 
    {
        this.logValue = logValue;
    }

    public String getLogValue() 
    {
        return logValue;
    }
    public void setDeviceId(Long deviceId) 
    {
        this.deviceId = deviceId;
    }

    public Long getDeviceId() 
    {
        return deviceId;
    }

    public void setIdentity(String identity) 
    {
        this.identity = identity;
    }

    public String getIdentity() 
    {
        return identity;
    }
    public void setIsMonitor(Integer isMonitor) 
    {
        this.isMonitor = isMonitor;
    }

    public Integer getIsMonitor() 
    {
        return isMonitor;
    }

    public String getData1() {
        return data1;
    }

    public void setData1(String data1) {
        this.data1 = data1;
    }

    public String getData2() {
        return data2;
    }

    public void setData2(String data2) {
        this.data2 = data2;
    }

    public String getData3() {
        return data3;
    }

    public void setData3(String data3) {
        this.data3 = data3;
    }

    public String getData4() {
        return data4;
    }

    public void setData4(String data4) {
        this.data4 = data4;
    }

    public String getData5() {
        return data5;
    }

    public void setData5(String data5) {
        this.data5 = data5;
    }

    public Instant getTime() {
        return time;
    }

    public void setTime(Instant time) {
        this.time = time;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("logValue", getLogValue())
            .append("deviceId", getDeviceId())
            .append("identity", getIdentity())
            .append("createBy", getCreateBy())
            .append("isMonitor", getIsMonitor())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .toString();
    }
}
