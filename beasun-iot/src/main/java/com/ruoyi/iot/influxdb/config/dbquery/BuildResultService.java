package com.ruoyi.iot.influxdb.config.dbquery;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Build result
 * Created by Andy.Yang on 2019-05-28.
 */
@Service
public class BuildResultService<T> {
    private final Logger log = LoggerFactory.getLogger(getClass());

    /**
     * Build dataEntity with page
     * @param page
     * @return
     */
    public DataEntity buildDataEntity(Page page ){
        DataEntity data = new DataEntity();
        List<T> list = (List<T>) page.getContent();
        data.setDatas( list );
        data.setPage( new PageEntity(page) );
        return data;
    }


    /**
     * Build dataEntity with page
     * @param list
     * @return
     */
    public DataEntity buildDataEntity( List<T> list ){
        DataEntity data = new DataEntity();
        data.setDatas( list );
        data.getPage().setTotalElements(list.size());
        data.getPage().setTotalPages(1);
        return data;
    }

    /**
     * Build dataEntity with one instance
     * @param one
     * @return
     */
    public DataEntity buildDataEntity( T one ){
        DataEntity data = new DataEntity();
        List<Object> list = new ArrayList<>();
        list.add( one );
        data.setDatas( list );
        data.getPage().setTotalElements(1);
        data.getPage().setTotalPages(1);
        return data;
    }


}
