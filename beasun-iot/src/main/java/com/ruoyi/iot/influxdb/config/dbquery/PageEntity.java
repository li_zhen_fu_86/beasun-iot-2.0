package com.ruoyi.iot.influxdb.config.dbquery;

import org.springframework.data.domain.Page;

/**
 * Created by Andy.Yang on 2019-05-28.
 */
public class PageEntity {
    // size of page
    private int size = 20;
    // total of pages
    private int totalPages = 0;
    // total of elements
    private long totalElements = 0;
    // page number
    private int number = 0;

    public PageEntity(){
    }

    public PageEntity(Page page ){
        size = page.getSize();
        totalPages = page.getTotalPages();
        totalElements = page.getTotalElements();
        number = page.getNumber();
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
