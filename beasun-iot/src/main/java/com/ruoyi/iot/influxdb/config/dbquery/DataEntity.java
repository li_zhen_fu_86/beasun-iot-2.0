package com.ruoyi.iot.influxdb.config.dbquery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andy.Yang on 2019-05-28.
 */
public class DataEntity {
    List<?> datas = new ArrayList<>();

    private PageEntity page = new PageEntity();

    public List<?> getDatas() {
        return datas;
    }

    public void setDatas(List<?> datas) {
        this.datas = datas;
    }

    public PageEntity getPage() {
        return page;
    }

    public void setPage(PageEntity page) {
        this.page = page;
    }
}
