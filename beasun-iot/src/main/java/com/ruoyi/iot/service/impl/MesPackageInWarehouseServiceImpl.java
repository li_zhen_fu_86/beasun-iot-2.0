package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.MesPackageInWarehouseMapper;
import com.ruoyi.iot.domain.MesPackageInWarehouse;
import com.ruoyi.iot.service.IMesPackageInWarehouseService;

/**
 * mes-包装入库Service业务层处理
 *
 * @author swingli
 * @date 2023-12-25
 */
@Service
public class MesPackageInWarehouseServiceImpl implements IMesPackageInWarehouseService
{
    @Autowired
    private MesPackageInWarehouseMapper mesPackageInWarehouseMapper;

    /**
     * 查询mes-包装入库
     *
     * @param id mes-包装入库主键
     * @return mes-包装入库
     */
    @Override
    public MesPackageInWarehouse selectMesPackageInWarehouseById(Long id)
    {
        return mesPackageInWarehouseMapper.selectMesPackageInWarehouseById(id);
    }

    /**
     * 查询mes-包装入库列表
     *
     * @param mesPackageInWarehouse mes-包装入库
     * @return mes-包装入库
     */
    @Override
    public List<MesPackageInWarehouse> selectMesPackageInWarehouseList(MesPackageInWarehouse mesPackageInWarehouse)
    {
        return mesPackageInWarehouseMapper.selectMesPackageInWarehouseList(mesPackageInWarehouse);
    }

    /**
     * 新增mes-包装入库
     *
     * @param mesPackageInWarehouse mes-包装入库
     * @return 结果
     */
    @Override
    public int insertMesPackageInWarehouse(MesPackageInWarehouse mesPackageInWarehouse)
    {
        return mesPackageInWarehouseMapper.insertMesPackageInWarehouse(mesPackageInWarehouse);
    }

    /**
     * 修改mes-包装入库
     *
     * @param mesPackageInWarehouse mes-包装入库
     * @return 结果
     */
    @Override
    public int updateMesPackageInWarehouse(MesPackageInWarehouse mesPackageInWarehouse)
    {
        return mesPackageInWarehouseMapper.updateMesPackageInWarehouse(mesPackageInWarehouse);
    }

    /**
     * 批量删除mes-包装入库
     *
     * @param ids 需要删除的mes-包装入库主键
     * @return 结果
     */
    @Override
    public int deleteMesPackageInWarehouseByIds(Long[] ids)
    {
        return mesPackageInWarehouseMapper.deleteMesPackageInWarehouseByIds(ids);
    }

    /**
     * 删除mes-包装入库信息
     *
     * @param id mes-包装入库主键
     * @return 结果
     */
    @Override
    public int deleteMesPackageInWarehouseById(Long id)
    {
        return mesPackageInWarehouseMapper.deleteMesPackageInWarehouseById(id);
    }
}