package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.MesGoodsOffShelfMapper;
import com.ruoyi.iot.domain.MesGoodsOffShelf;
import com.ruoyi.iot.service.IMesGoodsOffShelfService;

/**
 * mes-出库下架作业Service业务层处理
 *
 * @author swingli
 * @date 2023-12-25
 */
@Service
public class MesGoodsOffShelfServiceImpl implements IMesGoodsOffShelfService
{
    @Autowired
    private MesGoodsOffShelfMapper mesGoodsOffShelfMapper;

    /**
     * 查询mes-出库下架作业
     *
     * @param id mes-出库下架作业主键
     * @return mes-出库下架作业
     */
    @Override
    public MesGoodsOffShelf selectMesGoodsOffShelfById(Long id)
    {
        return mesGoodsOffShelfMapper.selectMesGoodsOffShelfById(id);
    }

    /**
     * 查询mes-出库下架作业列表
     *
     * @param mesGoodsOffShelf mes-出库下架作业
     * @return mes-出库下架作业
     */
    @Override
    public List<MesGoodsOffShelf> selectMesGoodsOffShelfList(MesGoodsOffShelf mesGoodsOffShelf)
    {
        return mesGoodsOffShelfMapper.selectMesGoodsOffShelfList(mesGoodsOffShelf);
    }

    /**
     * 新增mes-出库下架作业
     *
     * @param mesGoodsOffShelf mes-出库下架作业
     * @return 结果
     */
    @Override
    public int insertMesGoodsOffShelf(MesGoodsOffShelf mesGoodsOffShelf)
    {
        return mesGoodsOffShelfMapper.insertMesGoodsOffShelf(mesGoodsOffShelf);
    }

    /**
     * 修改mes-出库下架作业
     *
     * @param mesGoodsOffShelf mes-出库下架作业
     * @return 结果
     */
    @Override
    public int updateMesGoodsOffShelf(MesGoodsOffShelf mesGoodsOffShelf)
    {
        return mesGoodsOffShelfMapper.updateMesGoodsOffShelf(mesGoodsOffShelf);
    }

    /**
     * 批量删除mes-出库下架作业
     *
     * @param ids 需要删除的mes-出库下架作业主键
     * @return 结果
     */
    @Override
    public int deleteMesGoodsOffShelfByIds(Long[] ids)
    {
        return mesGoodsOffShelfMapper.deleteMesGoodsOffShelfByIds(ids);
    }

    /**
     * 删除mes-出库下架作业信息
     *
     * @param id mes-出库下架作业主键
     * @return 结果
     */
    @Override
    public int deleteMesGoodsOffShelfById(Long id)
    {
        return mesGoodsOffShelfMapper.deleteMesGoodsOffShelfById(id);
    }
}