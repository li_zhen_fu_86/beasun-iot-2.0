package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.MesPackageInWarehouse;

/**
 * mes-包装入库Service接口
 *
 * @author swingli
 * @date 2023-12-25
 */
public interface IMesPackageInWarehouseService
{
    /**
     * 查询mes-包装入库
     *
     * @param id mes-包装入库主键
     * @return mes-包装入库
     */
    public MesPackageInWarehouse selectMesPackageInWarehouseById(Long id);

    /**
     * 查询mes-包装入库列表
     *
     * @param mesPackageInWarehouse mes-包装入库
     * @return mes-包装入库集合
     */
    public List<MesPackageInWarehouse> selectMesPackageInWarehouseList(MesPackageInWarehouse mesPackageInWarehouse);

    /**
     * 新增mes-包装入库
     *
     * @param mesPackageInWarehouse mes-包装入库
     * @return 结果
     */
    public int insertMesPackageInWarehouse(MesPackageInWarehouse mesPackageInWarehouse);

    /**
     * 修改mes-包装入库
     *
     * @param mesPackageInWarehouse mes-包装入库
     * @return 结果
     */
    public int updateMesPackageInWarehouse(MesPackageInWarehouse mesPackageInWarehouse);

    /**
     * 批量删除mes-包装入库
     *
     * @param ids 需要删除的mes-包装入库主键集合
     * @return 结果
     */
    public int deleteMesPackageInWarehouseByIds(Long[] ids);

    /**
     * 删除mes-包装入库信息
     *
     * @param id mes-包装入库主键
     * @return 结果
     */
    public int deleteMesPackageInWarehouseById(Long id);
}