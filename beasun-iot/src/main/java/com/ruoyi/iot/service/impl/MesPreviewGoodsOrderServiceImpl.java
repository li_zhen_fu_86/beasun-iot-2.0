package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.MesPreviewGoodsOrderMapper;
import com.ruoyi.iot.domain.MesPreviewGoodsOrder;
import com.ruoyi.iot.service.IMesPreviewGoodsOrderService;

/**
 * mes-预到货单Service业务层处理
 *
 * @author swingli
 * @date 2023-12-23
 */
@Service
public class MesPreviewGoodsOrderServiceImpl implements IMesPreviewGoodsOrderService
{
    @Autowired
    private MesPreviewGoodsOrderMapper mesPreviewGoodsOrderMapper;

    /**
     * 查询mes-预到货单
     *
     * @param id mes-预到货单主键
     * @return mes-预到货单
     */
    @Override
    public MesPreviewGoodsOrder selectMesPreviewGoodsOrderById(Long id)
    {
        return mesPreviewGoodsOrderMapper.selectMesPreviewGoodsOrderById(id);
    }

    /**
     * 查询mes-预到货单列表
     *
     * @param mesPreviewGoodsOrder mes-预到货单
     * @return mes-预到货单
     */
    @Override
    public List<MesPreviewGoodsOrder> selectMesPreviewGoodsOrderList(MesPreviewGoodsOrder mesPreviewGoodsOrder)
    {
        return mesPreviewGoodsOrderMapper.selectMesPreviewGoodsOrderList(mesPreviewGoodsOrder);
    }

    /**
     * 新增mes-预到货单
     *
     * @param mesPreviewGoodsOrder mes-预到货单
     * @return 结果
     */
    @Override
    public int insertMesPreviewGoodsOrder(MesPreviewGoodsOrder mesPreviewGoodsOrder)
    {
        return mesPreviewGoodsOrderMapper.insertMesPreviewGoodsOrder(mesPreviewGoodsOrder);
    }

    /**
     * 修改mes-预到货单
     *
     * @param mesPreviewGoodsOrder mes-预到货单
     * @return 结果
     */
    @Override
    public int updateMesPreviewGoodsOrder(MesPreviewGoodsOrder mesPreviewGoodsOrder)
    {
        return mesPreviewGoodsOrderMapper.updateMesPreviewGoodsOrder(mesPreviewGoodsOrder);
    }

    /**
     * 批量删除mes-预到货单
     *
     * @param ids 需要删除的mes-预到货单主键
     * @return 结果
     */
    @Override
    public int deleteMesPreviewGoodsOrderByIds(Long[] ids)
    {
        return mesPreviewGoodsOrderMapper.deleteMesPreviewGoodsOrderByIds(ids);
    }

    /**
     * 删除mes-预到货单信息
     *
     * @param id mes-预到货单主键
     * @return 结果
     */
    @Override
    public int deleteMesPreviewGoodsOrderById(Long id)
    {
        return mesPreviewGoodsOrderMapper.deleteMesPreviewGoodsOrderById(id);
    }
}