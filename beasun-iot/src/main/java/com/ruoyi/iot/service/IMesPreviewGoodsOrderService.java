package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.MesPreviewGoodsOrder;

/**
 * mes-预到货单Service接口
 *
 * @author swingli
 * @date 2023-12-23
 */
public interface IMesPreviewGoodsOrderService
{
    /**
     * 查询mes-预到货单
     *
     * @param id mes-预到货单主键
     * @return mes-预到货单
     */
    public MesPreviewGoodsOrder selectMesPreviewGoodsOrderById(Long id);

    /**
     * 查询mes-预到货单列表
     *
     * @param mesPreviewGoodsOrder mes-预到货单
     * @return mes-预到货单集合
     */
    public List<MesPreviewGoodsOrder> selectMesPreviewGoodsOrderList(MesPreviewGoodsOrder mesPreviewGoodsOrder);

    /**
     * 新增mes-预到货单
     *
     * @param mesPreviewGoodsOrder mes-预到货单
     * @return 结果
     */
    public int insertMesPreviewGoodsOrder(MesPreviewGoodsOrder mesPreviewGoodsOrder);

    /**
     * 修改mes-预到货单
     *
     * @param mesPreviewGoodsOrder mes-预到货单
     * @return 结果
     */
    public int updateMesPreviewGoodsOrder(MesPreviewGoodsOrder mesPreviewGoodsOrder);

    /**
     * 批量删除mes-预到货单
     *
     * @param ids 需要删除的mes-预到货单主键集合
     * @return 结果
     */
    public int deleteMesPreviewGoodsOrderByIds(Long[] ids);

    /**
     * 删除mes-预到货单信息
     *
     * @param id mes-预到货单主键
     * @return 结果
     */
    public int deleteMesPreviewGoodsOrderById(Long id);
}