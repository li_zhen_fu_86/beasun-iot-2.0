package com.ruoyi.iot.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.VerOtaUpdateMapper;
import com.ruoyi.iot.domain.VerOtaUpdate;
import com.ruoyi.iot.service.IVerOtaUpdateService;

/**
 * 版本管理-OTA升级广利Service业务层处理
 *
 * @author swingli
 * @date 2023-12-23
 */
@Service
public class VerOtaUpdateServiceImpl implements IVerOtaUpdateService
{
    @Autowired
    private VerOtaUpdateMapper verOtaUpdateMapper;

    /**
     * 查询版本管理-OTA升级广利
     *
     * @param id 版本管理-OTA升级广利主键
     * @return 版本管理-OTA升级广利
     */
    @Override
    public VerOtaUpdate selectVerOtaUpdateById(Long id)
    {
        return verOtaUpdateMapper.selectVerOtaUpdateById(id);
    }

    /**
     * 查询版本管理-OTA升级广利列表
     *
     * @param verOtaUpdate 版本管理-OTA升级广利
     * @return 版本管理-OTA升级广利
     */
    @Override
    public List<VerOtaUpdate> selectVerOtaUpdateList(VerOtaUpdate verOtaUpdate)
    {
        return verOtaUpdateMapper.selectVerOtaUpdateList(verOtaUpdate);
    }

    /**
     * 新增版本管理-OTA升级广利
     *
     * @param verOtaUpdate 版本管理-OTA升级广利
     * @return 结果
     */
    @Override
    public int insertVerOtaUpdate(VerOtaUpdate verOtaUpdate)
    {
        verOtaUpdate.setCreateTime(DateUtils.getNowDate());
        return verOtaUpdateMapper.insertVerOtaUpdate(verOtaUpdate);
    }

    /**
     * 修改版本管理-OTA升级广利
     *
     * @param verOtaUpdate 版本管理-OTA升级广利
     * @return 结果
     */
    @Override
    public int updateVerOtaUpdate(VerOtaUpdate verOtaUpdate)
    {
        return verOtaUpdateMapper.updateVerOtaUpdate(verOtaUpdate);
    }

    /**
     * 批量删除版本管理-OTA升级广利
     *
     * @param ids 需要删除的版本管理-OTA升级广利主键
     * @return 结果
     */
    @Override
    public int deleteVerOtaUpdateByIds(Long[] ids)
    {
        return verOtaUpdateMapper.deleteVerOtaUpdateByIds(ids);
    }

    /**
     * 删除版本管理-OTA升级广利信息
     *
     * @param id 版本管理-OTA升级广利主键
     * @return 结果
     */
    @Override
    public int deleteVerOtaUpdateById(Long id)
    {
        return verOtaUpdateMapper.deleteVerOtaUpdateById(id);
    }
}