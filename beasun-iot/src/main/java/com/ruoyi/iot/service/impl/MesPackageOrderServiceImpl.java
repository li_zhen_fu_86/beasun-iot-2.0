package com.ruoyi.iot.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.MesPackageOrderMapper;
import com.ruoyi.iot.domain.MesPackageOrder;
import com.ruoyi.iot.service.IMesPackageOrderService;

/**
 * mes-包装单Service业务层处理
 *
 * @author swingli
 * @date 2023-12-25
 */
@Service
public class MesPackageOrderServiceImpl implements IMesPackageOrderService
{
    @Autowired
    private MesPackageOrderMapper mesPackageOrderMapper;

    /**
     * 查询mes-包装单
     *
     * @param id mes-包装单主键
     * @return mes-包装单
     */
    @Override
    public MesPackageOrder selectMesPackageOrderById(Long id)
    {
        return mesPackageOrderMapper.selectMesPackageOrderById(id);
    }

    /**
     * 查询mes-包装单列表
     *
     * @param mesPackageOrder mes-包装单
     * @return mes-包装单
     */
    @Override
    public List<MesPackageOrder> selectMesPackageOrderList(MesPackageOrder mesPackageOrder)
    {
        return mesPackageOrderMapper.selectMesPackageOrderList(mesPackageOrder);
    }

    /**
     * 新增mes-包装单
     *
     * @param mesPackageOrder mes-包装单
     * @return 结果
     */
    @Override
    public int insertMesPackageOrder(MesPackageOrder mesPackageOrder)
    {
        mesPackageOrder.setCreateTime(DateUtils.getNowDate());
        return mesPackageOrderMapper.insertMesPackageOrder(mesPackageOrder);
    }

    /**
     * 修改mes-包装单
     *
     * @param mesPackageOrder mes-包装单
     * @return 结果
     */
    @Override
    public int updateMesPackageOrder(MesPackageOrder mesPackageOrder)
    {
        return mesPackageOrderMapper.updateMesPackageOrder(mesPackageOrder);
    }

    /**
     * 批量删除mes-包装单
     *
     * @param ids 需要删除的mes-包装单主键
     * @return 结果
     */
    @Override
    public int deleteMesPackageOrderByIds(Long[] ids)
    {
        return mesPackageOrderMapper.deleteMesPackageOrderByIds(ids);
    }

    /**
     * 删除mes-包装单信息
     *
     * @param id mes-包装单主键
     * @return 结果
     */
    @Override
    public int deleteMesPackageOrderById(Long id)
    {
        return mesPackageOrderMapper.deleteMesPackageOrderById(id);
    }
}