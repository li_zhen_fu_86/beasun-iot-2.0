package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.OperTemplate;

/**
 * 运维管理-采集模板Service接口
 *
 * @author swingli
 * @date 2023-12-22
 */
public interface IOperTemplateService
{
    /**
     * 查询运维管理-采集模板
     *
     * @param id 运维管理-采集模板主键
     * @return 运维管理-采集模板
     */
    public OperTemplate selectOperTemplateById(Long id);

    /**
     * 查询运维管理-采集模板列表
     *
     * @param operTemplate 运维管理-采集模板
     * @return 运维管理-采集模板集合
     */
    public List<OperTemplate> selectOperTemplateList(OperTemplate operTemplate);

    /**
     * 新增运维管理-采集模板
     *
     * @param operTemplate 运维管理-采集模板
     * @return 结果
     */
    public int insertOperTemplate(OperTemplate operTemplate);

    /**
     * 修改运维管理-采集模板
     *
     * @param operTemplate 运维管理-采集模板
     * @return 结果
     */
    public int updateOperTemplate(OperTemplate operTemplate);

    /**
     * 批量删除运维管理-采集模板
     *
     * @param ids 需要删除的运维管理-采集模板主键集合
     * @return 结果
     */
    public int deleteOperTemplateByIds(Long[] ids);

    /**
     * 删除运维管理-采集模板信息
     *
     * @param id 运维管理-采集模板主键
     * @return 结果
     */
    public int deleteOperTemplateById(Long id);
}