package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.TaskMapper;
import com.ruoyi.iot.domain.Task;
import com.ruoyi.iot.service.ITaskService;

/**
 * 任务中心-任务管理Service业务层处理
 *
 * @author swingli
 * @date 2023-12-23
 */
@Service
public class TaskServiceImpl implements ITaskService
{
    @Autowired
    private TaskMapper taskMapper;

    /**
     * 查询任务中心-任务管理
     *
     * @param id 任务中心-任务管理主键
     * @return 任务中心-任务管理
     */
    @Override
    public Task selectTaskById(Long id)
    {
        return taskMapper.selectTaskById(id);
    }

    /**
     * 查询任务中心-任务管理列表
     *
     * @param task 任务中心-任务管理
     * @return 任务中心-任务管理
     */
    @Override
    public List<Task> selectTaskList(Task task)
    {
        return taskMapper.selectTaskList(task);
    }

    /**
     * 新增任务中心-任务管理
     *
     * @param task 任务中心-任务管理
     * @return 结果
     */
    @Override
    public int insertTask(Task task)
    {
        return taskMapper.insertTask(task);
    }

    /**
     * 修改任务中心-任务管理
     *
     * @param task 任务中心-任务管理
     * @return 结果
     */
    @Override
    public int updateTask(Task task)
    {
        return taskMapper.updateTask(task);
    }

    /**
     * 批量删除任务中心-任务管理
     *
     * @param ids 需要删除的任务中心-任务管理主键
     * @return 结果
     */
    @Override
    public int deleteTaskByIds(Long[] ids)
    {
        return taskMapper.deleteTaskByIds(ids);
    }

    /**
     * 删除任务中心-任务管理信息
     *
     * @param id 任务中心-任务管理主键
     * @return 结果
     */
    @Override
    public int deleteTaskById(Long id)
    {
        return taskMapper.deleteTaskById(id);
    }
}