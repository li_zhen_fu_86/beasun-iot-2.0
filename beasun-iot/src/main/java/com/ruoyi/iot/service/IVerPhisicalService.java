package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.VerPhisical;

/**
 * 版本管理-固件版管理Service接口
 *
 * @author swingli
 * @date 2023-12-23
 */
public interface IVerPhisicalService
{
    /**
     * 查询版本管理-固件版管理
     *
     * @param id 版本管理-固件版管理主键
     * @return 版本管理-固件版管理
     */
    public VerPhisical selectVerPhisicalById(Long id);

    /**
     * 查询版本管理-固件版管理列表
     *
     * @param verPhisical 版本管理-固件版管理
     * @return 版本管理-固件版管理集合
     */
    public List<VerPhisical> selectVerPhisicalList(VerPhisical verPhisical);

    /**
     * 新增版本管理-固件版管理
     *
     * @param verPhisical 版本管理-固件版管理
     * @return 结果
     */
    public int insertVerPhisical(VerPhisical verPhisical);

    /**
     * 修改版本管理-固件版管理
     *
     * @param verPhisical 版本管理-固件版管理
     * @return 结果
     */
    public int updateVerPhisical(VerPhisical verPhisical);

    /**
     * 批量删除版本管理-固件版管理
     *
     * @param ids 需要删除的版本管理-固件版管理主键集合
     * @return 结果
     */
    public int deleteVerPhisicalByIds(Long[] ids);

    /**
     * 删除版本管理-固件版管理信息
     *
     * @param id 版本管理-固件版管理主键
     * @return 结果
     */
    public int deleteVerPhisicalById(Long id);
}