package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.MesWarehouseChangeMapper;
import com.ruoyi.iot.domain.MesWarehouseChange;
import com.ruoyi.iot.service.IMesWarehouseChangeService;

/**
 * mes-库存流水-制单日期Service业务层处理
 *
 * @author swingli
 * @date 2023-12-25
 */
@Service
public class MesWarehouseChangeServiceImpl implements IMesWarehouseChangeService
{
    @Autowired
    private MesWarehouseChangeMapper mesWarehouseChangeMapper;

    /**
     * 查询mes-库存流水-制单日期
     *
     * @param id mes-库存流水-制单日期主键
     * @return mes-库存流水-制单日期
     */
    @Override
    public MesWarehouseChange selectMesWarehouseChangeById(Long id)
    {
        return mesWarehouseChangeMapper.selectMesWarehouseChangeById(id);
    }

    /**
     * 查询mes-库存流水-制单日期列表
     *
     * @param mesWarehouseChange mes-库存流水-制单日期
     * @return mes-库存流水-制单日期
     */
    @Override
    public List<MesWarehouseChange> selectMesWarehouseChangeList(MesWarehouseChange mesWarehouseChange)
    {
        return mesWarehouseChangeMapper.selectMesWarehouseChangeList(mesWarehouseChange);
    }

    /**
     * 新增mes-库存流水-制单日期
     *
     * @param mesWarehouseChange mes-库存流水-制单日期
     * @return 结果
     */
    @Override
    public int insertMesWarehouseChange(MesWarehouseChange mesWarehouseChange)
    {
        return mesWarehouseChangeMapper.insertMesWarehouseChange(mesWarehouseChange);
    }

    /**
     * 修改mes-库存流水-制单日期
     *
     * @param mesWarehouseChange mes-库存流水-制单日期
     * @return 结果
     */
    @Override
    public int updateMesWarehouseChange(MesWarehouseChange mesWarehouseChange)
    {
        return mesWarehouseChangeMapper.updateMesWarehouseChange(mesWarehouseChange);
    }

    /**
     * 批量删除mes-库存流水-制单日期
     *
     * @param ids 需要删除的mes-库存流水-制单日期主键
     * @return 结果
     */
    @Override
    public int deleteMesWarehouseChangeByIds(Long[] ids)
    {
        return mesWarehouseChangeMapper.deleteMesWarehouseChangeByIds(ids);
    }

    /**
     * 删除mes-库存流水-制单日期信息
     *
     * @param id mes-库存流水-制单日期主键
     * @return 结果
     */
    @Override
    public int deleteMesWarehouseChangeById(Long id)
    {
        return mesWarehouseChangeMapper.deleteMesWarehouseChangeById(id);
    }
}