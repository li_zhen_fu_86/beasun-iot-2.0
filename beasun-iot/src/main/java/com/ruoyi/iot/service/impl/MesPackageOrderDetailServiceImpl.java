package com.ruoyi.iot.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.MesPackageOrderDetailMapper;
import com.ruoyi.iot.domain.MesPackageOrderDetail;
import com.ruoyi.iot.service.IMesPackageOrderDetailService;

/**
 * mes-包装单明细Service业务层处理
 *
 * @author swingli
 * @date 2023-12-25
 */
@Service
public class MesPackageOrderDetailServiceImpl implements IMesPackageOrderDetailService
{
    @Autowired
    private MesPackageOrderDetailMapper mesPackageOrderDetailMapper;

    /**
     * 查询mes-包装单明细
     *
     * @param id mes-包装单明细主键
     * @return mes-包装单明细
     */
    @Override
    public MesPackageOrderDetail selectMesPackageOrderDetailById(Long id)
    {
        return mesPackageOrderDetailMapper.selectMesPackageOrderDetailById(id);
    }

    /**
     * 查询mes-包装单明细列表
     *
     * @param mesPackageOrderDetail mes-包装单明细
     * @return mes-包装单明细
     */
    @Override
    public List<MesPackageOrderDetail> selectMesPackageOrderDetailList(MesPackageOrderDetail mesPackageOrderDetail)
    {
        return mesPackageOrderDetailMapper.selectMesPackageOrderDetailList(mesPackageOrderDetail);
    }

    /**
     * 新增mes-包装单明细
     *
     * @param mesPackageOrderDetail mes-包装单明细
     * @return 结果
     */
    @Override
    public int insertMesPackageOrderDetail(MesPackageOrderDetail mesPackageOrderDetail)
    {
        mesPackageOrderDetail.setCreateTime(DateUtils.getNowDate());
        return mesPackageOrderDetailMapper.insertMesPackageOrderDetail(mesPackageOrderDetail);
    }

    /**
     * 修改mes-包装单明细
     *
     * @param mesPackageOrderDetail mes-包装单明细
     * @return 结果
     */
    @Override
    public int updateMesPackageOrderDetail(MesPackageOrderDetail mesPackageOrderDetail)
    {
        return mesPackageOrderDetailMapper.updateMesPackageOrderDetail(mesPackageOrderDetail);
    }

    /**
     * 批量删除mes-包装单明细
     *
     * @param ids 需要删除的mes-包装单明细主键
     * @return 结果
     */
    @Override
    public int deleteMesPackageOrderDetailByIds(Long[] ids)
    {
        return mesPackageOrderDetailMapper.deleteMesPackageOrderDetailByIds(ids);
    }

    /**
     * 删除mes-包装单明细信息
     *
     * @param id mes-包装单明细主键
     * @return 结果
     */
    @Override
    public int deleteMesPackageOrderDetailById(Long id)
    {
        return mesPackageOrderDetailMapper.deleteMesPackageOrderDetailById(id);
    }
}