package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.MesHousewareAnalysis;

/**
 * mes-库存分析Service接口
 *
 * @author swingli
 * @date 2023-12-23
 */
public interface IMesHousewareAnalysisService
{
    /**
     * 查询mes-库存分析
     *
     * @param id mes-库存分析主键
     * @return mes-库存分析
     */
    public MesHousewareAnalysis selectMesHousewareAnalysisById(Long id);

    /**
     * 查询mes-库存分析列表
     *
     * @param mesHousewareAnalysis mes-库存分析
     * @return mes-库存分析集合
     */
    public List<MesHousewareAnalysis> selectMesHousewareAnalysisList(MesHousewareAnalysis mesHousewareAnalysis);

    /**
     * 新增mes-库存分析
     *
     * @param mesHousewareAnalysis mes-库存分析
     * @return 结果
     */
    public int insertMesHousewareAnalysis(MesHousewareAnalysis mesHousewareAnalysis);

    /**
     * 修改mes-库存分析
     *
     * @param mesHousewareAnalysis mes-库存分析
     * @return 结果
     */
    public int updateMesHousewareAnalysis(MesHousewareAnalysis mesHousewareAnalysis);

    /**
     * 批量删除mes-库存分析
     *
     * @param ids 需要删除的mes-库存分析主键集合
     * @return 结果
     */
    public int deleteMesHousewareAnalysisByIds(Long[] ids);

    /**
     * 删除mes-库存分析信息
     *
     * @param id mes-库存分析主键
     * @return 结果
     */
    public int deleteMesHousewareAnalysisById(Long id);
}