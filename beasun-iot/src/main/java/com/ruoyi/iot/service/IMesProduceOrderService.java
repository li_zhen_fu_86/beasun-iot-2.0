package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.MesProduceOrder;

/**
 * mes-生产任务单Service接口
 *
 * @author swingli
 * @date 2023-12-23
 */
public interface IMesProduceOrderService
{
    /**
     * 查询mes-生产任务单
     *
     * @param id mes-生产任务单主键
     * @return mes-生产任务单
     */
    public MesProduceOrder selectMesProduceOrderById(Long id);

    /**
     * 查询mes-生产任务单列表
     *
     * @param mesProduceOrder mes-生产任务单
     * @return mes-生产任务单集合
     */
    public List<MesProduceOrder> selectMesProduceOrderList(MesProduceOrder mesProduceOrder);

    /**
     * 新增mes-生产任务单
     *
     * @param mesProduceOrder mes-生产任务单
     * @return 结果
     */
    public int insertMesProduceOrder(MesProduceOrder mesProduceOrder);

    /**
     * 修改mes-生产任务单
     *
     * @param mesProduceOrder mes-生产任务单
     * @return 结果
     */
    public int updateMesProduceOrder(MesProduceOrder mesProduceOrder);

    /**
     * 批量删除mes-生产任务单
     *
     * @param ids 需要删除的mes-生产任务单主键集合
     * @return 结果
     */
    public int deleteMesProduceOrderByIds(Long[] ids);

    /**
     * 删除mes-生产任务单信息
     *
     * @param id mes-生产任务单主键
     * @return 结果
     */
    public int deleteMesProduceOrderById(Long id);
}