package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.VerSoftWare;

/**
 * 版本管理-软件版本管理Service接口
 *
 * @author swingli
 * @date 2023-12-23
 */
public interface IVerSoftWareService
{
    /**
     * 查询版本管理-软件版本管理
     *
     * @param id 版本管理-软件版本管理主键
     * @return 版本管理-软件版本管理
     */
    public VerSoftWare selectVerSoftWareById(Long id);

    /**
     * 查询版本管理-软件版本管理列表
     *
     * @param verSoftWare 版本管理-软件版本管理
     * @return 版本管理-软件版本管理集合
     */
    public List<VerSoftWare> selectVerSoftWareList(VerSoftWare verSoftWare);

    /**
     * 新增版本管理-软件版本管理
     *
     * @param verSoftWare 版本管理-软件版本管理
     * @return 结果
     */
    public int insertVerSoftWare(VerSoftWare verSoftWare);

    /**
     * 修改版本管理-软件版本管理
     *
     * @param verSoftWare 版本管理-软件版本管理
     * @return 结果
     */
    public int updateVerSoftWare(VerSoftWare verSoftWare);

    /**
     * 批量删除版本管理-软件版本管理
     *
     * @param ids 需要删除的版本管理-软件版本管理主键集合
     * @return 结果
     */
    public int deleteVerSoftWareByIds(Long[] ids);

    /**
     * 删除版本管理-软件版本管理信息
     *
     * @param id 版本管理-软件版本管理主键
     * @return 结果
     */
    public int deleteVerSoftWareById(Long id);
}