package com.ruoyi.iot.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.MessageCenterMapper;
import com.ruoyi.iot.domain.MessageCenter;
import com.ruoyi.iot.service.IMessageCenterService;

/**
 * iot-消息中心Service业务层处理
 *
 * @author swingli
 * @date 2023-12-23
 */
@Service
public class MessageCenterServiceImpl implements IMessageCenterService
{
    @Autowired
    private MessageCenterMapper messageCenterMapper;

    /**
     * 查询iot-消息中心
     *
     * @param id iot-消息中心主键
     * @return iot-消息中心
     */
    @Override
    public MessageCenter selectMessageCenterById(Long id)
    {
        return messageCenterMapper.selectMessageCenterById(id);
    }

    /**
     * 查询iot-消息中心列表
     *
     * @param messageCenter iot-消息中心
     * @return iot-消息中心
     */
    @Override
    public List<MessageCenter> selectMessageCenterList(MessageCenter messageCenter)
    {
        return messageCenterMapper.selectMessageCenterList(messageCenter);
    }

    /**
     * 新增iot-消息中心
     *
     * @param messageCenter iot-消息中心
     * @return 结果
     */
    @Override
    public int insertMessageCenter(MessageCenter messageCenter)
    {
        messageCenter.setCreateTime(DateUtils.getNowDate());
        return messageCenterMapper.insertMessageCenter(messageCenter);
    }

    /**
     * 修改iot-消息中心
     *
     * @param messageCenter iot-消息中心
     * @return 结果
     */
    @Override
    public int updateMessageCenter(MessageCenter messageCenter)
    {
        return messageCenterMapper.updateMessageCenter(messageCenter);
    }

    /**
     * 批量删除iot-消息中心
     *
     * @param ids 需要删除的iot-消息中心主键
     * @return 结果
     */
    @Override
    public int deleteMessageCenterByIds(Long[] ids)
    {
        return messageCenterMapper.deleteMessageCenterByIds(ids);
    }

    /**
     * 删除iot-消息中心信息
     *
     * @param id iot-消息中心主键
     * @return 结果
     */
    @Override
    public int deleteMessageCenterById(Long id)
    {
        return messageCenterMapper.deleteMessageCenterById(id);
    }
}