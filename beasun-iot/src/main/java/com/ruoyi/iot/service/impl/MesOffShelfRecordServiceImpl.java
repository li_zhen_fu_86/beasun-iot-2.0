package com.ruoyi.iot.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.MesOffShelfRecordMapper;
import com.ruoyi.iot.domain.MesOffShelfRecord;
import com.ruoyi.iot.service.IMesOffShelfRecordService;

/**
 * mes-下架记录单Service业务层处理
 *
 * @author swingli
 * @date 2023-12-25
 */
@Service
public class MesOffShelfRecordServiceImpl implements IMesOffShelfRecordService
{
    @Autowired
    private MesOffShelfRecordMapper mesOffShelfRecordMapper;

    /**
     * 查询mes-下架记录单
     *
     * @param id mes-下架记录单主键
     * @return mes-下架记录单
     */
    @Override
    public MesOffShelfRecord selectMesOffShelfRecordById(Long id)
    {
        return mesOffShelfRecordMapper.selectMesOffShelfRecordById(id);
    }

    /**
     * 查询mes-下架记录单列表
     *
     * @param mesOffShelfRecord mes-下架记录单
     * @return mes-下架记录单
     */
    @Override
    public List<MesOffShelfRecord> selectMesOffShelfRecordList(MesOffShelfRecord mesOffShelfRecord)
    {
        return mesOffShelfRecordMapper.selectMesOffShelfRecordList(mesOffShelfRecord);
    }

    /**
     * 新增mes-下架记录单
     *
     * @param mesOffShelfRecord mes-下架记录单
     * @return 结果
     */
    @Override
    public int insertMesOffShelfRecord(MesOffShelfRecord mesOffShelfRecord)
    {
        mesOffShelfRecord.setCreateTime(DateUtils.getNowDate());
        return mesOffShelfRecordMapper.insertMesOffShelfRecord(mesOffShelfRecord);
    }

    /**
     * 修改mes-下架记录单
     *
     * @param mesOffShelfRecord mes-下架记录单
     * @return 结果
     */
    @Override
    public int updateMesOffShelfRecord(MesOffShelfRecord mesOffShelfRecord)
    {
        return mesOffShelfRecordMapper.updateMesOffShelfRecord(mesOffShelfRecord);
    }

    /**
     * 批量删除mes-下架记录单
     *
     * @param ids 需要删除的mes-下架记录单主键
     * @return 结果
     */
    @Override
    public int deleteMesOffShelfRecordByIds(Long[] ids)
    {
        return mesOffShelfRecordMapper.deleteMesOffShelfRecordByIds(ids);
    }

    /**
     * 删除mes-下架记录单信息
     *
     * @param id mes-下架记录单主键
     * @return 结果
     */
    @Override
    public int deleteMesOffShelfRecordById(Long id)
    {
        return mesOffShelfRecordMapper.deleteMesOffShelfRecordById(id);
    }
}