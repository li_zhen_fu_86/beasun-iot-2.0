package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.OperModelMapper;
import com.ruoyi.iot.domain.OperModel;
import com.ruoyi.iot.service.IOperModelService;

/**
 * 运维管理-设备模拟Service业务层处理
 *
 * @author swingli
 * @date 2023-12-22
 */
@Service
public class OperModelServiceImpl implements IOperModelService
{
    @Autowired
    private OperModelMapper operModelMapper;

    /**
     * 查询运维管理-设备模拟
     *
     * @param id 运维管理-设备模拟主键
     * @return 运维管理-设备模拟
     */
    @Override
    public OperModel selectOperModelById(Long id)
    {
        return operModelMapper.selectOperModelById(id);
    }

    /**
     * 查询运维管理-设备模拟列表
     *
     * @param operModel 运维管理-设备模拟
     * @return 运维管理-设备模拟
     */
    @Override
    public List<OperModel> selectOperModelList(OperModel operModel)
    {
        return operModelMapper.selectOperModelList(operModel);
    }

    /**
     * 新增运维管理-设备模拟
     *
     * @param operModel 运维管理-设备模拟
     * @return 结果
     */
    @Override
    public int insertOperModel(OperModel operModel)
    {
        return operModelMapper.insertOperModel(operModel);
    }

    /**
     * 修改运维管理-设备模拟
     *
     * @param operModel 运维管理-设备模拟
     * @return 结果
     */
    @Override
    public int updateOperModel(OperModel operModel)
    {
        return operModelMapper.updateOperModel(operModel);
    }

    /**
     * 批量删除运维管理-设备模拟
     *
     * @param ids 需要删除的运维管理-设备模拟主键
     * @return 结果
     */
    @Override
    public int deleteOperModelByIds(Long[] ids)
    {
        return operModelMapper.deleteOperModelByIds(ids);
    }

    /**
     * 删除运维管理-设备模拟信息
     *
     * @param id 运维管理-设备模拟主键
     * @return 结果
     */
    @Override
    public int deleteOperModelById(Long id)
    {
        return operModelMapper.deleteOperModelById(id);
    }
}