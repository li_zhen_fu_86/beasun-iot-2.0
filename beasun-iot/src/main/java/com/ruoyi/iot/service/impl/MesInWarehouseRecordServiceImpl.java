package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.MesInWarehouseRecordMapper;
import com.ruoyi.iot.domain.MesInWarehouseRecord;
import com.ruoyi.iot.service.IMesInWarehouseRecordService;

/**
 * mesh-入库记录单Service业务层处理
 *
 * @author swingli
 * @date 2023-12-25
 */
@Service
public class MesInWarehouseRecordServiceImpl implements IMesInWarehouseRecordService
{
    @Autowired
    private MesInWarehouseRecordMapper mesInWarehouseRecordMapper;

    /**
     * 查询mesh-入库记录单
     *
     * @param id mesh-入库记录单主键
     * @return mesh-入库记录单
     */
    @Override
    public MesInWarehouseRecord selectMesInWarehouseRecordById(Long id)
    {
        return mesInWarehouseRecordMapper.selectMesInWarehouseRecordById(id);
    }

    /**
     * 查询mesh-入库记录单列表
     *
     * @param mesInWarehouseRecord mesh-入库记录单
     * @return mesh-入库记录单
     */
    @Override
    public List<MesInWarehouseRecord> selectMesInWarehouseRecordList(MesInWarehouseRecord mesInWarehouseRecord)
    {
        return mesInWarehouseRecordMapper.selectMesInWarehouseRecordList(mesInWarehouseRecord);
    }

    /**
     * 新增mesh-入库记录单
     *
     * @param mesInWarehouseRecord mesh-入库记录单
     * @return 结果
     */
    @Override
    public int insertMesInWarehouseRecord(MesInWarehouseRecord mesInWarehouseRecord)
    {
        return mesInWarehouseRecordMapper.insertMesInWarehouseRecord(mesInWarehouseRecord);
    }

    /**
     * 修改mesh-入库记录单
     *
     * @param mesInWarehouseRecord mesh-入库记录单
     * @return 结果
     */
    @Override
    public int updateMesInWarehouseRecord(MesInWarehouseRecord mesInWarehouseRecord)
    {
        return mesInWarehouseRecordMapper.updateMesInWarehouseRecord(mesInWarehouseRecord);
    }

    /**
     * 批量删除mesh-入库记录单
     *
     * @param ids 需要删除的mesh-入库记录单主键
     * @return 结果
     */
    @Override
    public int deleteMesInWarehouseRecordByIds(Long[] ids)
    {
        return mesInWarehouseRecordMapper.deleteMesInWarehouseRecordByIds(ids);
    }

    /**
     * 删除mesh-入库记录单信息
     *
     * @param id mesh-入库记录单主键
     * @return 结果
     */
    @Override
    public int deleteMesInWarehouseRecordById(Long id)
    {
        return mesInWarehouseRecordMapper.deleteMesInWarehouseRecordById(id);
    }
}