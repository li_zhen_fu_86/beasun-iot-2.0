package com.ruoyi.iot.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.MesCompleteOrderMapper;
import com.ruoyi.iot.domain.MesCompleteOrder;
import com.ruoyi.iot.service.IMesCompleteOrderService;

/**
 * mes-完工汇报单Service业务层处理
 *
 * @author swingli
 * @date 2023-12-23
 */
@Service
public class MesCompleteOrderServiceImpl implements IMesCompleteOrderService
{
    @Autowired
    private MesCompleteOrderMapper mesCompleteOrderMapper;

    /**
     * 查询mes-完工汇报单
     *
     * @param id mes-完工汇报单主键
     * @return mes-完工汇报单
     */
    @Override
    public MesCompleteOrder selectMesCompleteOrderById(Long id)
    {
        return mesCompleteOrderMapper.selectMesCompleteOrderById(id);
    }

    /**
     * 查询mes-完工汇报单列表
     *
     * @param mesCompleteOrder mes-完工汇报单
     * @return mes-完工汇报单
     */
    @Override
    public List<MesCompleteOrder> selectMesCompleteOrderList(MesCompleteOrder mesCompleteOrder)
    {
        return mesCompleteOrderMapper.selectMesCompleteOrderList(mesCompleteOrder);
    }

    /**
     * 新增mes-完工汇报单
     *
     * @param mesCompleteOrder mes-完工汇报单
     * @return 结果
     */
    @Override
    public int insertMesCompleteOrder(MesCompleteOrder mesCompleteOrder)
    {
        mesCompleteOrder.setCreateTime(DateUtils.getNowDate());
        return mesCompleteOrderMapper.insertMesCompleteOrder(mesCompleteOrder);
    }

    /**
     * 修改mes-完工汇报单
     *
     * @param mesCompleteOrder mes-完工汇报单
     * @return 结果
     */
    @Override
    public int updateMesCompleteOrder(MesCompleteOrder mesCompleteOrder)
    {
        return mesCompleteOrderMapper.updateMesCompleteOrder(mesCompleteOrder);
    }

    /**
     * 批量删除mes-完工汇报单
     *
     * @param ids 需要删除的mes-完工汇报单主键
     * @return 结果
     */
    @Override
    public int deleteMesCompleteOrderByIds(Long[] ids)
    {
        return mesCompleteOrderMapper.deleteMesCompleteOrderByIds(ids);
    }

    /**
     * 删除mes-完工汇报单信息
     *
     * @param id mes-完工汇报单主键
     * @return 结果
     */
    @Override
    public int deleteMesCompleteOrderById(Long id)
    {
        return mesCompleteOrderMapper.deleteMesCompleteOrderById(id);
    }
}