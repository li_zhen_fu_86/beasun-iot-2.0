package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.MesProduceOrderMapper;
import com.ruoyi.iot.domain.MesProduceOrder;
import com.ruoyi.iot.service.IMesProduceOrderService;

/**
 * mes-生产任务单Service业务层处理
 *
 * @author swingli
 * @date 2023-12-23
 */
@Service
public class MesProduceOrderServiceImpl implements IMesProduceOrderService
{
    @Autowired
    private MesProduceOrderMapper mesProduceOrderMapper;

    /**
     * 查询mes-生产任务单
     *
     * @param id mes-生产任务单主键
     * @return mes-生产任务单
     */
    @Override
    public MesProduceOrder selectMesProduceOrderById(Long id)
    {
        return mesProduceOrderMapper.selectMesProduceOrderById(id);
    }

    /**
     * 查询mes-生产任务单列表
     *
     * @param mesProduceOrder mes-生产任务单
     * @return mes-生产任务单
     */
    @Override
    public List<MesProduceOrder> selectMesProduceOrderList(MesProduceOrder mesProduceOrder)
    {
        return mesProduceOrderMapper.selectMesProduceOrderList(mesProduceOrder);
    }

    /**
     * 新增mes-生产任务单
     *
     * @param mesProduceOrder mes-生产任务单
     * @return 结果
     */
    @Override
    public int insertMesProduceOrder(MesProduceOrder mesProduceOrder)
    {
        return mesProduceOrderMapper.insertMesProduceOrder(mesProduceOrder);
    }

    /**
     * 修改mes-生产任务单
     *
     * @param mesProduceOrder mes-生产任务单
     * @return 结果
     */
    @Override
    public int updateMesProduceOrder(MesProduceOrder mesProduceOrder)
    {
        return mesProduceOrderMapper.updateMesProduceOrder(mesProduceOrder);
    }

    /**
     * 批量删除mes-生产任务单
     *
     * @param ids 需要删除的mes-生产任务单主键
     * @return 结果
     */
    @Override
    public int deleteMesProduceOrderByIds(Long[] ids)
    {
        return mesProduceOrderMapper.deleteMesProduceOrderByIds(ids);
    }

    /**
     * 删除mes-生产任务单信息
     *
     * @param id mes-生产任务单主键
     * @return 结果
     */
    @Override
    public int deleteMesProduceOrderById(Long id)
    {
        return mesProduceOrderMapper.deleteMesProduceOrderById(id);
    }
}