package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.Task;

/**
 * 任务中心-任务管理Service接口
 *
 * @author swingli
 * @date 2023-12-23
 */
public interface ITaskService
{
    /**
     * 查询任务中心-任务管理
     *
     * @param id 任务中心-任务管理主键
     * @return 任务中心-任务管理
     */
    public Task selectTaskById(Long id);

    /**
     * 查询任务中心-任务管理列表
     *
     * @param task 任务中心-任务管理
     * @return 任务中心-任务管理集合
     */
    public List<Task> selectTaskList(Task task);

    /**
     * 新增任务中心-任务管理
     *
     * @param task 任务中心-任务管理
     * @return 结果
     */
    public int insertTask(Task task);

    /**
     * 修改任务中心-任务管理
     *
     * @param task 任务中心-任务管理
     * @return 结果
     */
    public int updateTask(Task task);

    /**
     * 批量删除任务中心-任务管理
     *
     * @param ids 需要删除的任务中心-任务管理主键集合
     * @return 结果
     */
    public int deleteTaskByIds(Long[] ids);

    /**
     * 删除任务中心-任务管理信息
     *
     * @param id 任务中心-任务管理主键
     * @return 结果
     */
    public int deleteTaskById(Long id);
}