package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.MesOutOrderMapper;
import com.ruoyi.iot.domain.MesOutOrder;
import com.ruoyi.iot.service.IMesOutOrderService;

/**
 * mes-出库单Service业务层处理
 *
 * @author swingli
 * @date 2023-12-23
 */
@Service
public class MesOutOrderServiceImpl implements IMesOutOrderService
{
    @Autowired
    private MesOutOrderMapper mesOutOrderMapper;

    /**
     * 查询mes-出库单
     *
     * @param id mes-出库单主键
     * @return mes-出库单
     */
    @Override
    public MesOutOrder selectMesOutOrderById(Long id)
    {
        return mesOutOrderMapper.selectMesOutOrderById(id);
    }

    /**
     * 查询mes-出库单列表
     *
     * @param mesOutOrder mes-出库单
     * @return mes-出库单
     */
    @Override
    public List<MesOutOrder> selectMesOutOrderList(MesOutOrder mesOutOrder)
    {
        return mesOutOrderMapper.selectMesOutOrderList(mesOutOrder);
    }

    /**
     * 新增mes-出库单
     *
     * @param mesOutOrder mes-出库单
     * @return 结果
     */
    @Override
    public int insertMesOutOrder(MesOutOrder mesOutOrder)
    {
        return mesOutOrderMapper.insertMesOutOrder(mesOutOrder);
    }

    /**
     * 修改mes-出库单
     *
     * @param mesOutOrder mes-出库单
     * @return 结果
     */
    @Override
    public int updateMesOutOrder(MesOutOrder mesOutOrder)
    {
        return mesOutOrderMapper.updateMesOutOrder(mesOutOrder);
    }

    /**
     * 批量删除mes-出库单
     *
     * @param ids 需要删除的mes-出库单主键
     * @return 结果
     */
    @Override
    public int deleteMesOutOrderByIds(Long[] ids)
    {
        return mesOutOrderMapper.deleteMesOutOrderByIds(ids);
    }

    /**
     * 删除mes-出库单信息
     *
     * @param id mes-出库单主键
     * @return 结果
     */
    @Override
    public int deleteMesOutOrderById(Long id)
    {
        return mesOutOrderMapper.deleteMesOutOrderById(id);
    }
}