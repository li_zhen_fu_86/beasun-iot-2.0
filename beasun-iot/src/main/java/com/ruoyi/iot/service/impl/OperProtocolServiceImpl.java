package com.ruoyi.iot.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.iot.domain.OperProtocol;
import com.ruoyi.iot.mapper.OperProtocolMapper;
import com.ruoyi.iot.service.IOperProtocolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 运维管理-协议管理Service业务层处理
 * 
 * @author swingli
 * @date 2023-12-22
 */
@Service
public class OperProtocolServiceImpl implements IOperProtocolService
{
    @Autowired
    private OperProtocolMapper operProtocolMapper;

    /**
     * 查询运维管理-协议管理
     * 
     * @param id 运维管理-协议管理主键
     * @return 运维管理-协议管理
     */
    @Override
    public OperProtocol selectOperProtocolById(Long id)
    {
        return operProtocolMapper.selectOperProtocolById(id);
    }

    /**
     * 查询运维管理-协议管理列表
     * 
     * @param operProtocol 运维管理-协议管理
     * @return 运维管理-协议管理
     */
    @Override
    public List<OperProtocol> selectOperProtocolList(OperProtocol operProtocol)
    {
        return operProtocolMapper.selectOperProtocolList(operProtocol);
    }

    /**
     * 新增运维管理-协议管理
     * 
     * @param operProtocol 运维管理-协议管理
     * @return 结果
     */
    @Override
    public int insertOperProtocol(OperProtocol operProtocol)
    {
        operProtocol.setCreateTime(DateUtils.getNowDate());
        return operProtocolMapper.insertOperProtocol(operProtocol);
    }

    /**
     * 修改运维管理-协议管理
     * 
     * @param operProtocol 运维管理-协议管理
     * @return 结果
     */
    @Override
    public int updateOperProtocol(OperProtocol operProtocol)
    {
        operProtocol.setUpdateTime(DateUtils.getNowDate());
        return operProtocolMapper.updateOperProtocol(operProtocol);
    }

    /**
     * 批量删除运维管理-协议管理
     * 
     * @param ids 需要删除的运维管理-协议管理主键
     * @return 结果
     */
    @Override
    public int deleteOperProtocolByIds(Long[] ids)
    {
        return operProtocolMapper.deleteOperProtocolByIds(ids);
    }

    /**
     * 删除运维管理-协议管理信息
     * 
     * @param id 运维管理-协议管理主键
     * @return 结果
     */
    @Override
    public int deleteOperProtocolById(Long id)
    {
        return operProtocolMapper.deleteOperProtocolById(id);
    }
}