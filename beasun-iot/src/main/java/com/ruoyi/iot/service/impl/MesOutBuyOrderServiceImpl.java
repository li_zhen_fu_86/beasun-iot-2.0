package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.MesOutBuyOrderMapper;
import com.ruoyi.iot.domain.MesOutBuyOrder;
import com.ruoyi.iot.service.IMesOutBuyOrderService;

/**
 * mes-出库订单Service业务层处理
 *
 * @author swingli
 * @date 2023-12-25
 */
@Service
public class MesOutBuyOrderServiceImpl implements IMesOutBuyOrderService
{
    @Autowired
    private MesOutBuyOrderMapper mesOutBuyOrderMapper;

    /**
     * 查询mes-出库订单
     *
     * @param id mes-出库订单主键
     * @return mes-出库订单
     */
    @Override
    public MesOutBuyOrder selectMesOutBuyOrderById(Long id)
    {
        return mesOutBuyOrderMapper.selectMesOutBuyOrderById(id);
    }

    /**
     * 查询mes-出库订单列表
     *
     * @param mesOutBuyOrder mes-出库订单
     * @return mes-出库订单
     */
    @Override
    public List<MesOutBuyOrder> selectMesOutBuyOrderList(MesOutBuyOrder mesOutBuyOrder)
    {
        return mesOutBuyOrderMapper.selectMesOutBuyOrderList(mesOutBuyOrder);
    }

    /**
     * 新增mes-出库订单
     *
     * @param mesOutBuyOrder mes-出库订单
     * @return 结果
     */
    @Override
    public int insertMesOutBuyOrder(MesOutBuyOrder mesOutBuyOrder)
    {
        return mesOutBuyOrderMapper.insertMesOutBuyOrder(mesOutBuyOrder);
    }

    /**
     * 修改mes-出库订单
     *
     * @param mesOutBuyOrder mes-出库订单
     * @return 结果
     */
    @Override
    public int updateMesOutBuyOrder(MesOutBuyOrder mesOutBuyOrder)
    {
        return mesOutBuyOrderMapper.updateMesOutBuyOrder(mesOutBuyOrder);
    }

    /**
     * 批量删除mes-出库订单
     *
     * @param ids 需要删除的mes-出库订单主键
     * @return 结果
     */
    @Override
    public int deleteMesOutBuyOrderByIds(Long[] ids)
    {
        return mesOutBuyOrderMapper.deleteMesOutBuyOrderByIds(ids);
    }

    /**
     * 删除mes-出库订单信息
     *
     * @param id mes-出库订单主键
     * @return 结果
     */
    @Override
    public int deleteMesOutBuyOrderById(Long id)
    {
        return mesOutBuyOrderMapper.deleteMesOutBuyOrderById(id);
    }
}