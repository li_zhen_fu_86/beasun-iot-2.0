package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.VerOtaUpdate;

/**
 * 版本管理-OTA升级广利Service接口
 *
 * @author swingli
 * @date 2023-12-23
 */
public interface IVerOtaUpdateService
{
    /**
     * 查询版本管理-OTA升级广利
     *
     * @param id 版本管理-OTA升级广利主键
     * @return 版本管理-OTA升级广利
     */
    public VerOtaUpdate selectVerOtaUpdateById(Long id);

    /**
     * 查询版本管理-OTA升级广利列表
     *
     * @param verOtaUpdate 版本管理-OTA升级广利
     * @return 版本管理-OTA升级广利集合
     */
    public List<VerOtaUpdate> selectVerOtaUpdateList(VerOtaUpdate verOtaUpdate);

    /**
     * 新增版本管理-OTA升级广利
     *
     * @param verOtaUpdate 版本管理-OTA升级广利
     * @return 结果
     */
    public int insertVerOtaUpdate(VerOtaUpdate verOtaUpdate);

    /**
     * 修改版本管理-OTA升级广利
     *
     * @param verOtaUpdate 版本管理-OTA升级广利
     * @return 结果
     */
    public int updateVerOtaUpdate(VerOtaUpdate verOtaUpdate);

    /**
     * 批量删除版本管理-OTA升级广利
     *
     * @param ids 需要删除的版本管理-OTA升级广利主键集合
     * @return 结果
     */
    public int deleteVerOtaUpdateByIds(Long[] ids);

    /**
     * 删除版本管理-OTA升级广利信息
     *
     * @param id 版本管理-OTA升级广利主键
     * @return 结果
     */
    public int deleteVerOtaUpdateById(Long id);
}