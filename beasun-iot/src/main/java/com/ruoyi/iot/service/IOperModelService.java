package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.OperModel;

/**
 * 运维管理-设备模拟Service接口
 *
 * @author swingli
 * @date 2023-12-22
 */
public interface IOperModelService
{
    /**
     * 查询运维管理-设备模拟
     *
     * @param id 运维管理-设备模拟主键
     * @return 运维管理-设备模拟
     */
    public OperModel selectOperModelById(Long id);

    /**
     * 查询运维管理-设备模拟列表
     *
     * @param operModel 运维管理-设备模拟
     * @return 运维管理-设备模拟集合
     */
    public List<OperModel> selectOperModelList(OperModel operModel);

    /**
     * 新增运维管理-设备模拟
     *
     * @param operModel 运维管理-设备模拟
     * @return 结果
     */
    public int insertOperModel(OperModel operModel);

    /**
     * 修改运维管理-设备模拟
     *
     * @param operModel 运维管理-设备模拟
     * @return 结果
     */
    public int updateOperModel(OperModel operModel);

    /**
     * 批量删除运维管理-设备模拟
     *
     * @param ids 需要删除的运维管理-设备模拟主键集合
     * @return 结果
     */
    public int deleteOperModelByIds(Long[] ids);

    /**
     * 删除运维管理-设备模拟信息
     *
     * @param id 运维管理-设备模拟主键
     * @return 结果
     */
    public int deleteOperModelById(Long id);
}