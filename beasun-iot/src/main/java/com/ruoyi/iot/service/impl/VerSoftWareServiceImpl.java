package com.ruoyi.iot.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.VerSoftWareMapper;
import com.ruoyi.iot.domain.VerSoftWare;
import com.ruoyi.iot.service.IVerSoftWareService;

/**
 * 版本管理-软件版本管理Service业务层处理
 *
 * @author swingli
 * @date 2023-12-23
 */
@Service
public class VerSoftWareServiceImpl implements IVerSoftWareService
{
    @Autowired
    private VerSoftWareMapper verSoftWareMapper;

    /**
     * 查询版本管理-软件版本管理
     *
     * @param id 版本管理-软件版本管理主键
     * @return 版本管理-软件版本管理
     */
    @Override
    public VerSoftWare selectVerSoftWareById(Long id)
    {
        return verSoftWareMapper.selectVerSoftWareById(id);
    }

    /**
     * 查询版本管理-软件版本管理列表
     *
     * @param verSoftWare 版本管理-软件版本管理
     * @return 版本管理-软件版本管理
     */
    @Override
    public List<VerSoftWare> selectVerSoftWareList(VerSoftWare verSoftWare)
    {
        return verSoftWareMapper.selectVerSoftWareList(verSoftWare);
    }

    /**
     * 新增版本管理-软件版本管理
     *
     * @param verSoftWare 版本管理-软件版本管理
     * @return 结果
     */
    @Override
    public int insertVerSoftWare(VerSoftWare verSoftWare)
    {
        return verSoftWareMapper.insertVerSoftWare(verSoftWare);
    }

    /**
     * 修改版本管理-软件版本管理
     *
     * @param verSoftWare 版本管理-软件版本管理
     * @return 结果
     */
    @Override
    public int updateVerSoftWare(VerSoftWare verSoftWare)
    {
        verSoftWare.setUpdateTime(DateUtils.getNowDate());
        return verSoftWareMapper.updateVerSoftWare(verSoftWare);
    }

    /**
     * 批量删除版本管理-软件版本管理
     *
     * @param ids 需要删除的版本管理-软件版本管理主键
     * @return 结果
     */
    @Override
    public int deleteVerSoftWareByIds(Long[] ids)
    {
        return verSoftWareMapper.deleteVerSoftWareByIds(ids);
    }

    /**
     * 删除版本管理-软件版本管理信息
     *
     * @param id 版本管理-软件版本管理主键
     * @return 结果
     */
    @Override
    public int deleteVerSoftWareById(Long id)
    {
        return verSoftWareMapper.deleteVerSoftWareById(id);
    }
}