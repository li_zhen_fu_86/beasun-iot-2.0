package com.ruoyi.iot.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.VerDeviceMapper;
import com.ruoyi.iot.domain.VerDevice;
import com.ruoyi.iot.service.IVerDeviceService;

/**
 * 版本管理-硬件版本定义Service业务层处理
 *
 * @author swingli
 * @date 2023-12-23
 */
@Service
public class VerDeviceServiceImpl implements IVerDeviceService
{
    @Autowired
    private VerDeviceMapper verDeviceMapper;

    /**
     * 查询版本管理-硬件版本定义
     *
     * @param id 版本管理-硬件版本定义主键
     * @return 版本管理-硬件版本定义
     */
    @Override
    public VerDevice selectVerDeviceById(Long id)
    {
        return verDeviceMapper.selectVerDeviceById(id);
    }

    /**
     * 查询版本管理-硬件版本定义列表
     *
     * @param verDevice 版本管理-硬件版本定义
     * @return 版本管理-硬件版本定义
     */
    @Override
    public List<VerDevice> selectVerDeviceList(VerDevice verDevice)
    {
        return verDeviceMapper.selectVerDeviceList(verDevice);
    }

    /**
     * 新增版本管理-硬件版本定义
     *
     * @param verDevice 版本管理-硬件版本定义
     * @return 结果
     */
    @Override
    public int insertVerDevice(VerDevice verDevice)
    {
        return verDeviceMapper.insertVerDevice(verDevice);
    }

    /**
     * 修改版本管理-硬件版本定义
     *
     * @param verDevice 版本管理-硬件版本定义
     * @return 结果
     */
    @Override
    public int updateVerDevice(VerDevice verDevice)
    {
        verDevice.setUpdateTime(DateUtils.getNowDate());
        return verDeviceMapper.updateVerDevice(verDevice);
    }

    /**
     * 批量删除版本管理-硬件版本定义
     *
     * @param ids 需要删除的版本管理-硬件版本定义主键
     * @return 结果
     */
    @Override
    public int deleteVerDeviceByIds(Long[] ids)
    {
        return verDeviceMapper.deleteVerDeviceByIds(ids);
    }

    /**
     * 删除版本管理-硬件版本定义信息
     *
     * @param id 版本管理-硬件版本定义主键
     * @return 结果
     */
    @Override
    public int deleteVerDeviceById(Long id)
    {
        return verDeviceMapper.deleteVerDeviceById(id);
    }
}