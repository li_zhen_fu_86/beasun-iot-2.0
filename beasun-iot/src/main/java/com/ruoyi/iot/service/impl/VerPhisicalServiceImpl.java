package com.ruoyi.iot.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.VerPhisicalMapper;
import com.ruoyi.iot.domain.VerPhisical;
import com.ruoyi.iot.service.IVerPhisicalService;

/**
 * 版本管理-固件版管理Service业务层处理
 *
 * @author swingli
 * @date 2023-12-23
 */
@Service
public class VerPhisicalServiceImpl implements IVerPhisicalService
{
    @Autowired
    private VerPhisicalMapper verPhisicalMapper;

    /**
     * 查询版本管理-固件版管理
     *
     * @param id 版本管理-固件版管理主键
     * @return 版本管理-固件版管理
     */
    @Override
    public VerPhisical selectVerPhisicalById(Long id)
    {
        return verPhisicalMapper.selectVerPhisicalById(id);
    }

    /**
     * 查询版本管理-固件版管理列表
     *
     * @param verPhisical 版本管理-固件版管理
     * @return 版本管理-固件版管理
     */
    @Override
    public List<VerPhisical> selectVerPhisicalList(VerPhisical verPhisical)
    {
        return verPhisicalMapper.selectVerPhisicalList(verPhisical);
    }

    /**
     * 新增版本管理-固件版管理
     *
     * @param verPhisical 版本管理-固件版管理
     * @return 结果
     */
    @Override
    public int insertVerPhisical(VerPhisical verPhisical)
    {
        return verPhisicalMapper.insertVerPhisical(verPhisical);
    }

    /**
     * 修改版本管理-固件版管理
     *
     * @param verPhisical 版本管理-固件版管理
     * @return 结果
     */
    @Override
    public int updateVerPhisical(VerPhisical verPhisical)
    {
        verPhisical.setUpdateTime(DateUtils.getNowDate());
        return verPhisicalMapper.updateVerPhisical(verPhisical);
    }

    /**
     * 批量删除版本管理-固件版管理
     *
     * @param ids 需要删除的版本管理-固件版管理主键
     * @return 结果
     */
    @Override
    public int deleteVerPhisicalByIds(Long[] ids)
    {
        return verPhisicalMapper.deleteVerPhisicalByIds(ids);
    }

    /**
     * 删除版本管理-固件版管理信息
     *
     * @param id 版本管理-固件版管理主键
     * @return 结果
     */
    @Override
    public int deleteVerPhisicalById(Long id)
    {
        return verPhisicalMapper.deleteVerPhisicalById(id);
    }
}