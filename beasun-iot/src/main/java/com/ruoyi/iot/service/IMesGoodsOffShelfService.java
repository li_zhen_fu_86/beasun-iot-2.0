package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.MesGoodsOffShelf;

/**
 * mes-出库下架作业Service接口
 *
 * @author swingli
 * @date 2023-12-25
 */
public interface IMesGoodsOffShelfService
{
    /**
     * 查询mes-出库下架作业
     *
     * @param id mes-出库下架作业主键
     * @return mes-出库下架作业
     */
    public MesGoodsOffShelf selectMesGoodsOffShelfById(Long id);

    /**
     * 查询mes-出库下架作业列表
     *
     * @param mesGoodsOffShelf mes-出库下架作业
     * @return mes-出库下架作业集合
     */
    public List<MesGoodsOffShelf> selectMesGoodsOffShelfList(MesGoodsOffShelf mesGoodsOffShelf);

    /**
     * 新增mes-出库下架作业
     *
     * @param mesGoodsOffShelf mes-出库下架作业
     * @return 结果
     */
    public int insertMesGoodsOffShelf(MesGoodsOffShelf mesGoodsOffShelf);

    /**
     * 修改mes-出库下架作业
     *
     * @param mesGoodsOffShelf mes-出库下架作业
     * @return 结果
     */
    public int updateMesGoodsOffShelf(MesGoodsOffShelf mesGoodsOffShelf);

    /**
     * 批量删除mes-出库下架作业
     *
     * @param ids 需要删除的mes-出库下架作业主键集合
     * @return 结果
     */
    public int deleteMesGoodsOffShelfByIds(Long[] ids);

    /**
     * 删除mes-出库下架作业信息
     *
     * @param id mes-出库下架作业主键
     * @return 结果
     */
    public int deleteMesGoodsOffShelfById(Long id);
}