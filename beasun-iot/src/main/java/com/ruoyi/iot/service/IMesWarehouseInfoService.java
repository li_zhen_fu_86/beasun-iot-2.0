package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.MesWarehouseInfo;

/**
 * mes-库存信息Service接口
 *
 * @author swingli
 * @date 2023-12-25
 */
public interface IMesWarehouseInfoService
{
    /**
     * 查询mes-库存信息
     *
     * @param id mes-库存信息主键
     * @return mes-库存信息
     */
    public MesWarehouseInfo selectMesWarehouseInfoById(Long id);

    /**
     * 查询mes-库存信息列表
     *
     * @param mesWarehouseInfo mes-库存信息
     * @return mes-库存信息集合
     */
    public List<MesWarehouseInfo> selectMesWarehouseInfoList(MesWarehouseInfo mesWarehouseInfo);

    /**
     * 新增mes-库存信息
     *
     * @param mesWarehouseInfo mes-库存信息
     * @return 结果
     */
    public int insertMesWarehouseInfo(MesWarehouseInfo mesWarehouseInfo);

    /**
     * 修改mes-库存信息
     *
     * @param mesWarehouseInfo mes-库存信息
     * @return 结果
     */
    public int updateMesWarehouseInfo(MesWarehouseInfo mesWarehouseInfo);

    /**
     * 批量删除mes-库存信息
     *
     * @param ids 需要删除的mes-库存信息主键集合
     * @return 结果
     */
    public int deleteMesWarehouseInfoByIds(Long[] ids);

    /**
     * 删除mes-库存信息信息
     *
     * @param id mes-库存信息主键
     * @return 结果
     */
    public int deleteMesWarehouseInfoById(Long id);
}