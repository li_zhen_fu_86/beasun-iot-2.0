package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.MesWarehouseInfoMapper;
import com.ruoyi.iot.domain.MesWarehouseInfo;
import com.ruoyi.iot.service.IMesWarehouseInfoService;

/**
 * mes-库存信息Service业务层处理
 *
 * @author swingli
 * @date 2023-12-25
 */
@Service
public class MesWarehouseInfoServiceImpl implements IMesWarehouseInfoService
{
    @Autowired
    private MesWarehouseInfoMapper mesWarehouseInfoMapper;

    /**
     * 查询mes-库存信息
     *
     * @param id mes-库存信息主键
     * @return mes-库存信息
     */
    @Override
    public MesWarehouseInfo selectMesWarehouseInfoById(Long id)
    {
        return mesWarehouseInfoMapper.selectMesWarehouseInfoById(id);
    }

    /**
     * 查询mes-库存信息列表
     *
     * @param mesWarehouseInfo mes-库存信息
     * @return mes-库存信息
     */
    @Override
    public List<MesWarehouseInfo> selectMesWarehouseInfoList(MesWarehouseInfo mesWarehouseInfo)
    {
        return mesWarehouseInfoMapper.selectMesWarehouseInfoList(mesWarehouseInfo);
    }

    /**
     * 新增mes-库存信息
     *
     * @param mesWarehouseInfo mes-库存信息
     * @return 结果
     */
    @Override
    public int insertMesWarehouseInfo(MesWarehouseInfo mesWarehouseInfo)
    {
        return mesWarehouseInfoMapper.insertMesWarehouseInfo(mesWarehouseInfo);
    }

    /**
     * 修改mes-库存信息
     *
     * @param mesWarehouseInfo mes-库存信息
     * @return 结果
     */
    @Override
    public int updateMesWarehouseInfo(MesWarehouseInfo mesWarehouseInfo)
    {
        return mesWarehouseInfoMapper.updateMesWarehouseInfo(mesWarehouseInfo);
    }

    /**
     * 批量删除mes-库存信息
     *
     * @param ids 需要删除的mes-库存信息主键
     * @return 结果
     */
    @Override
    public int deleteMesWarehouseInfoByIds(Long[] ids)
    {
        return mesWarehouseInfoMapper.deleteMesWarehouseInfoByIds(ids);
    }

    /**
     * 删除mes-库存信息信息
     *
     * @param id mes-库存信息主键
     * @return 结果
     */
    @Override
    public int deleteMesWarehouseInfoById(Long id)
    {
        return mesWarehouseInfoMapper.deleteMesWarehouseInfoById(id);
    }
}