package com.ruoyi.iot.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.MesHousewareAnalysisMapper;
import com.ruoyi.iot.domain.MesHousewareAnalysis;
import com.ruoyi.iot.service.IMesHousewareAnalysisService;

/**
 * mes-库存分析Service业务层处理
 *
 * @author swingli
 * @date 2023-12-23
 */
@Service
public class MesHousewareAnalysisServiceImpl implements IMesHousewareAnalysisService
{
    @Autowired
    private MesHousewareAnalysisMapper mesHousewareAnalysisMapper;

    /**
     * 查询mes-库存分析
     *
     * @param id mes-库存分析主键
     * @return mes-库存分析
     */
    @Override
    public MesHousewareAnalysis selectMesHousewareAnalysisById(Long id)
    {
        return mesHousewareAnalysisMapper.selectMesHousewareAnalysisById(id);
    }

    /**
     * 查询mes-库存分析列表
     *
     * @param mesHousewareAnalysis mes-库存分析
     * @return mes-库存分析
     */
    @Override
    public List<MesHousewareAnalysis> selectMesHousewareAnalysisList(MesHousewareAnalysis mesHousewareAnalysis)
    {
        return mesHousewareAnalysisMapper.selectMesHousewareAnalysisList(mesHousewareAnalysis);
    }

    /**
     * 新增mes-库存分析
     *
     * @param mesHousewareAnalysis mes-库存分析
     * @return 结果
     */
    @Override
    public int insertMesHousewareAnalysis(MesHousewareAnalysis mesHousewareAnalysis)
    {
        mesHousewareAnalysis.setCreateTime(DateUtils.getNowDate());
        return mesHousewareAnalysisMapper.insertMesHousewareAnalysis(mesHousewareAnalysis);
    }

    /**
     * 修改mes-库存分析
     *
     * @param mesHousewareAnalysis mes-库存分析
     * @return 结果
     */
    @Override
    public int updateMesHousewareAnalysis(MesHousewareAnalysis mesHousewareAnalysis)
    {
        return mesHousewareAnalysisMapper.updateMesHousewareAnalysis(mesHousewareAnalysis);
    }

    /**
     * 批量删除mes-库存分析
     *
     * @param ids 需要删除的mes-库存分析主键
     * @return 结果
     */
    @Override
    public int deleteMesHousewareAnalysisByIds(Long[] ids)
    {
        return mesHousewareAnalysisMapper.deleteMesHousewareAnalysisByIds(ids);
    }

    /**
     * 删除mes-库存分析信息
     *
     * @param id mes-库存分析主键
     * @return 结果
     */
    @Override
    public int deleteMesHousewareAnalysisById(Long id)
    {
        return mesHousewareAnalysisMapper.deleteMesHousewareAnalysisById(id);
    }
}