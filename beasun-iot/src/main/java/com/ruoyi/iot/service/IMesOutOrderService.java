package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.MesOutOrder;

/**
 * mes-出库单Service接口
 *
 * @author swingli
 * @date 2023-12-23
 */
public interface IMesOutOrderService
{
    /**
     * 查询mes-出库单
     *
     * @param id mes-出库单主键
     * @return mes-出库单
     */
    public MesOutOrder selectMesOutOrderById(Long id);

    /**
     * 查询mes-出库单列表
     *
     * @param mesOutOrder mes-出库单
     * @return mes-出库单集合
     */
    public List<MesOutOrder> selectMesOutOrderList(MesOutOrder mesOutOrder);

    /**
     * 新增mes-出库单
     *
     * @param mesOutOrder mes-出库单
     * @return 结果
     */
    public int insertMesOutOrder(MesOutOrder mesOutOrder);

    /**
     * 修改mes-出库单
     *
     * @param mesOutOrder mes-出库单
     * @return 结果
     */
    public int updateMesOutOrder(MesOutOrder mesOutOrder);

    /**
     * 批量删除mes-出库单
     *
     * @param ids 需要删除的mes-出库单主键集合
     * @return 结果
     */
    public int deleteMesOutOrderByIds(Long[] ids);

    /**
     * 删除mes-出库单信息
     *
     * @param id mes-出库单主键
     * @return 结果
     */
    public int deleteMesOutOrderById(Long id);
}