package com.ruoyi.iot.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.OperTemplateMapper;
import com.ruoyi.iot.domain.OperTemplate;
import com.ruoyi.iot.service.IOperTemplateService;

/**
 * 运维管理-采集模板Service业务层处理
 *
 * @author swingli
 * @date 2023-12-22
 */
@Service
public class OperTemplateServiceImpl implements IOperTemplateService
{
    @Autowired
    private OperTemplateMapper operTemplateMapper;

    /**
     * 查询运维管理-采集模板
     *
     * @param id 运维管理-采集模板主键
     * @return 运维管理-采集模板
     */
    @Override
    public OperTemplate selectOperTemplateById(Long id)
    {
        return operTemplateMapper.selectOperTemplateById(id);
    }

    /**
     * 查询运维管理-采集模板列表
     *
     * @param operTemplate 运维管理-采集模板
     * @return 运维管理-采集模板
     */
    @Override
    public List<OperTemplate> selectOperTemplateList(OperTemplate operTemplate)
    {
        return operTemplateMapper.selectOperTemplateList(operTemplate);
    }

    /**
     * 新增运维管理-采集模板
     *
     * @param operTemplate 运维管理-采集模板
     * @return 结果
     */
    @Override
    public int insertOperTemplate(OperTemplate operTemplate)
    {
        operTemplate.setCreateTime(DateUtils.getNowDate());
        return operTemplateMapper.insertOperTemplate(operTemplate);
    }

    /**
     * 修改运维管理-采集模板
     *
     * @param operTemplate 运维管理-采集模板
     * @return 结果
     */
    @Override
    public int updateOperTemplate(OperTemplate operTemplate)
    {
        operTemplate.setUpdateTime(DateUtils.getNowDate());
        return operTemplateMapper.updateOperTemplate(operTemplate);
    }

    /**
     * 批量删除运维管理-采集模板
     *
     * @param ids 需要删除的运维管理-采集模板主键
     * @return 结果
     */
    @Override
    public int deleteOperTemplateByIds(Long[] ids)
    {
        return operTemplateMapper.deleteOperTemplateByIds(ids);
    }

    /**
     * 删除运维管理-采集模板信息
     *
     * @param id 运维管理-采集模板主键
     * @return 结果
     */
    @Override
    public int deleteOperTemplateById(Long id)
    {
        return operTemplateMapper.deleteOperTemplateById(id);
    }
}