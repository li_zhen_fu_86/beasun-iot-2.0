package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.MesSerialWarehouseMapper;
import com.ruoyi.iot.domain.MesSerialWarehouse;
import com.ruoyi.iot.service.IMesSerialWarehouseService;

/**
 * mes-序列号库存Service业务层处理
 *
 * @author swingli
 * @date 2023-12-25
 */
@Service
public class MesSerialWarehouseServiceImpl implements IMesSerialWarehouseService
{
    @Autowired
    private MesSerialWarehouseMapper mesSerialWarehouseMapper;

    /**
     * 查询mes-序列号库存
     *
     * @param id mes-序列号库存主键
     * @return mes-序列号库存
     */
    @Override
    public MesSerialWarehouse selectMesSerialWarehouseById(Long id)
    {
        return mesSerialWarehouseMapper.selectMesSerialWarehouseById(id);
    }

    /**
     * 查询mes-序列号库存列表
     *
     * @param mesSerialWarehouse mes-序列号库存
     * @return mes-序列号库存
     */
    @Override
    public List<MesSerialWarehouse> selectMesSerialWarehouseList(MesSerialWarehouse mesSerialWarehouse)
    {
        return mesSerialWarehouseMapper.selectMesSerialWarehouseList(mesSerialWarehouse);
    }

    /**
     * 新增mes-序列号库存
     *
     * @param mesSerialWarehouse mes-序列号库存
     * @return 结果
     */
    @Override
    public int insertMesSerialWarehouse(MesSerialWarehouse mesSerialWarehouse)
    {
        return mesSerialWarehouseMapper.insertMesSerialWarehouse(mesSerialWarehouse);
    }

    /**
     * 修改mes-序列号库存
     *
     * @param mesSerialWarehouse mes-序列号库存
     * @return 结果
     */
    @Override
    public int updateMesSerialWarehouse(MesSerialWarehouse mesSerialWarehouse)
    {
        return mesSerialWarehouseMapper.updateMesSerialWarehouse(mesSerialWarehouse);
    }

    /**
     * 批量删除mes-序列号库存
     *
     * @param ids 需要删除的mes-序列号库存主键
     * @return 结果
     */
    @Override
    public int deleteMesSerialWarehouseByIds(Long[] ids)
    {
        return mesSerialWarehouseMapper.deleteMesSerialWarehouseByIds(ids);
    }

    /**
     * 删除mes-序列号库存信息
     *
     * @param id mes-序列号库存主键
     * @return 结果
     */
    @Override
    public int deleteMesSerialWarehouseById(Long id)
    {
        return mesSerialWarehouseMapper.deleteMesSerialWarehouseById(id);
    }
}