package com.ruoyi.iot.util;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;

/**
 * Created by Andy.Yang on 2017/3/27.
 */
public class GsonUtils {
    public static final String DATE_FORMATE = "yyyy-MM-dd HH:mm:ss";

    private static final Logger log = LoggerFactory.getLogger(GsonUtils.class);

    /**
     * 获取gson对象，带时间格式
     * @param prettyPrinting  是否输出格式化的json
     * @param dateFormat  时间格式
     * @return
     */
    public static Gson getGsonWithFormat(boolean prettyPrinting, String dateFormat ) {
        final GsonBuilder gsonBuilder = new GsonBuilder();
        if ( prettyPrinting ){
            gsonBuilder.setPrettyPrinting();
        }

        if ( null == dateFormat ){
            dateFormat = DATE_FORMATE;
        }

        gsonBuilder.disableHtmlEscaping();
        gsonBuilder.setDateFormat( dateFormat );
        gsonBuilder.registerTypeAdapter( Instant.class, new JsonInstantSerializer());
        return gsonBuilder.create();
    }
}
