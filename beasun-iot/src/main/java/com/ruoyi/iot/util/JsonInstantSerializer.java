package com.ruoyi.iot.util;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.ruoyi.iot.config.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.time.Instant;

/**
 * Created by Andy.Yang on 2017/5/4.
 */
public class JsonInstantSerializer implements JsonDeserializer<Instant> {
    private final Logger log = LoggerFactory.getLogger(getClass());

    // json转为对象时调用,实现JsonDeserializer<PluginParam>接口
    @Override
    public Instant deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Instant instant = null;
        try {
            instant = DateUtils.getDateFromString( json.getAsString(), Constants.DATE_FORMATE ).toInstant();
        } catch (ParseException e) {
            log.error( e.getMessage() );
            e.printStackTrace();
        }
        return instant;
    }
}
